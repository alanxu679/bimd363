﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// Action
struct Action_t3370723196;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// AutoMove
struct AutoMove_t3127166102;
// Physics2DObject
struct Physics2DObject_t3026219368;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Transform
struct Transform_t3600365921;
// AutoRotate
struct AutoRotate_t913508323;
// BalloonScript
struct BalloonScript_t4017429074;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// System.String
struct String_t;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// BulletAttribute
struct BulletAttribute_t588811475;
// CameraFollow
struct CameraFollow_t129522575;
// CollectableAttribute
struct CollectableAttribute_t113388083;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// ConditionArea
struct ConditionArea_t3498028374;
// ConditionBase
struct ConditionBase_t3399778919;
// System.Collections.Generic.List`1<Action>
struct List_1_t547830642;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// ConditionCollision
struct ConditionCollision_t1635305638;
// UnityEngine.Collision2D
struct Collision2D_t2842956331;
// ConditionKeyPress
struct ConditionKeyPress_t1643004734;
// ConditionRepeat
struct ConditionRepeat_t1215900890;
// ConsumeResourceAction
struct ConsumeResourceAction_t2832324500;
// UIScript
struct UIScript_t1195624422;
// CreateObjectAction
struct CreateObjectAction_t2026806364;
// DestroyAction
struct DestroyAction_t1816662878;
// DestroyForPointsAttribute
struct DestroyForPointsAttribute_t2602039747;
// DialogueBalloonAction
struct DialogueBalloonAction_t3348434864;
// DialogueSystem
struct DialogueSystem_t292220827;
// System.Delegate
struct Delegate_t1188392813;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// DialogueBalloonAction/<WaitForBallonDestroyed>c__Iterator0
struct U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479;
// System.Func`1<System.Boolean>
struct Func_1_t3822001908;
// UnityEngine.WaitUntil
struct WaitUntil_t3373419216;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// Enums
struct Enums_t1053268879;
// FollowTarget
struct FollowTarget_t1456261924;
// HealthSystemAttribute
struct HealthSystemAttribute_t2647166000;
// InventoryResources
struct InventoryResources_t222662465;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// Jump
struct Jump_t2935391209;
// LoadLevelAction
struct LoadLevelAction_t3647347530;
// ModifyHealthAttribute
struct ModifyHealthAttribute_t3455596632;
// Move
struct Move_t3440333737;
// ObjectCreatorArea
struct ObjectCreatorArea_t9936620;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t3581341831;
// ObjectCreatorArea/<SpawnObject>c__Iterator0
struct U3CSpawnObjectU3Ec__Iterator0_t3452356763;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// ObjectShooter
struct ObjectShooter_t734116591;
// OnOffAction
struct OnOffAction_t1233588362;
// Patrol
struct Patrol_t2573740331;
// PickUpAndHold
struct PickUpAndHold_t4109171223;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// Push
struct Push_t4181980645;
// ResourceAttribute
struct ResourceAttribute_t892502608;
// UnityEngine.Sprite
struct Sprite_t280657092;
// ResourceStruct
struct ResourceStruct_t862114426;
// UIItemScript
struct UIItemScript_t1692532892;
// Rotate
struct Rotate_t1850091912;
// Shoot
struct Shoot_t2202938192;
// Stopper
struct Stopper_t1976456672;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t662546754;
// TeleportAction
struct TeleportAction_t1205856175;
// TimedSelfDestruct
struct TimedSelfDestruct_t3957973201;
// System.Collections.Generic.Dictionary`2<System.Int32,ResourceStruct>
struct Dictionary_2_t4045795053;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1968819495;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// Wander
struct Wander_t651605530;
// Wander/<ChangeDirection>c__Iterator0
struct U3CChangeDirectionU3Ec__Iterator0_t1842818970;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// Action[]
struct ActionU5BU5D_t2307473173;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// ResourceStruct[]
struct ResourceStructU5BU5D_t1642021471;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t763310475;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,ResourceStruct,System.Collections.DictionaryEntry>
struct Transform_1_t1246360315;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t96683501;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t422084607;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct Action_1_t819399007;

extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern const uint32_t AutoMove_FixedUpdate_m1334605003_MetadataUsageId;
extern RuntimeClass* Utils_t1444179947_il2cpp_TypeInfo_var;
extern const uint32_t AutoMove_OnDrawGizmosSelected_m2054477897_MetadataUsageId;
extern const uint32_t AutoRotate_OnDrawGizmosSelected_m911995967_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var;
extern const uint32_t BalloonScript_Awake_m536274945_MetadataUsageId;
extern RuntimeClass* KeyCode_t2599294277_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern RuntimeClass* RectTransformUtility_t1743242446_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var;
extern String_t* _stringLiteral2700639336;
extern const uint32_t BalloonScript_Setup_m3797033188_MetadataUsageId;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern const uint32_t BalloonScript_Update_m1258479472_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502_RuntimeMethod_var;
extern const uint32_t BalloonScript_FollowTarget_m625056729_MetadataUsageId;
extern const uint32_t BulletAttribute_Reset_m495814584_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var;
extern const uint32_t CameraFollow_Awake_m1034099068_MetadataUsageId;
extern const uint32_t CameraFollow_FixedUpdate_m1628278094_MetadataUsageId;
extern const uint32_t CameraFollow_LateUpdate_m2490584337_MetadataUsageId;
extern const uint32_t CollectableAttribute_Reset_m1687984457_MetadataUsageId;
extern String_t* _stringLiteral2261822918;
extern String_t* _stringLiteral259609454;
extern const uint32_t CollectableAttribute_OnTriggerEnter2D_m2028075393_MetadataUsageId;
extern const uint32_t ConditionArea_Reset_m4059968981_MetadataUsageId;
extern RuntimeClass* List_1_t547830642_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m3594099166_RuntimeMethod_var;
extern const uint32_t ConditionBase__ctor_m238416164_MetadataUsageId;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_GetEnumerator_m2023103186_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m263105171_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m4019089914_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1654570958_RuntimeMethod_var;
extern String_t* _stringLiteral1632036919;
extern const uint32_t ConditionBase_ExecuteAllActions_m3297109267_MetadataUsageId;
extern const uint32_t ConditionCollision_Reset_m4122520466_MetadataUsageId;
extern const uint32_t ConditionKeyPress_Update_m856179128_MetadataUsageId;
extern const RuntimeMethod* Object_FindObjectOfType_TisUIScript_t1195624422_m1742595665_RuntimeMethod_var;
extern const uint32_t ConsumeResourceAction_Start_m1402206288_MetadataUsageId;
extern String_t* _stringLiteral920216033;
extern const uint32_t ConsumeResourceAction_ExecuteAction_m3605170454_MetadataUsageId;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var;
extern String_t* _stringLiteral2979139915;
extern const uint32_t CreateObjectAction_ExecuteAction_m3938939490_MetadataUsageId;
extern const uint32_t DestroyAction_ExecuteAction_m2632851313_MetadataUsageId;
extern const uint32_t DestroyForPointsAttribute_Start_m2308896846_MetadataUsageId;
extern const uint32_t DestroyForPointsAttribute_Reset_m2910058454_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisBulletAttribute_t588811475_m587253523_RuntimeMethod_var;
extern String_t* _stringLiteral37315637;
extern String_t* _stringLiteral3499710428;
extern String_t* _stringLiteral2632304242;
extern const uint32_t DestroyForPointsAttribute_OnTriggerEnter2D_m3805996196_MetadataUsageId;
extern String_t* _stringLiteral2734162053;
extern const uint32_t DialogueBalloonAction__ctor_m398991252_MetadataUsageId;
extern RuntimeClass* UnityAction_t3245792599_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisDialogueSystem_t292220827_m1864039531_RuntimeMethod_var;
extern const RuntimeMethod* DialogueBalloonAction_OnBalloonDestroyed_m1884765915_RuntimeMethod_var;
extern String_t* _stringLiteral2002083477;
extern const uint32_t DialogueBalloonAction_ExecuteAction_m1502107245_MetadataUsageId;
extern RuntimeClass* U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479_il2cpp_TypeInfo_var;
extern const uint32_t DialogueBalloonAction_WaitForBallonDestroyed_m1668273814_MetadataUsageId;
extern const uint32_t DialogueBalloonAction_OnBalloonDestroyed_m1884765915_MetadataUsageId;
extern RuntimeClass* Func_1_t3822001908_il2cpp_TypeInfo_var;
extern RuntimeClass* WaitUntil_t3373419216_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CWaitForBallonDestroyedU3Ec__Iterator0_U3CU3Em__0_m2819031299_RuntimeMethod_var;
extern const RuntimeMethod* Func_1__ctor_m1399073142_RuntimeMethod_var;
extern const uint32_t U3CWaitForBallonDestroyedU3Ec__Iterator0_MoveNext_m376558171_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CWaitForBallonDestroyedU3Ec__Iterator0_Reset_m2388530936_RuntimeMethod_var;
extern const uint32_t U3CWaitForBallonDestroyedU3Ec__Iterator0_Reset_m2388530936_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisBalloonScript_t4017429074_m2020822283_RuntimeMethod_var;
extern const uint32_t DialogueSystem_CreateBalloon_m1415492347_MetadataUsageId;
extern const uint32_t FollowTarget_FixedUpdate_m3007629599_MetadataUsageId;
extern const uint32_t HealthSystemAttribute_Start_m151931609_MetadataUsageId;
extern const uint32_t HealthSystemAttribute_ModifyHealth_m2785567747_MetadataUsageId;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Count_m2276455407_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3346958548_RuntimeMethod_var;
extern const uint32_t InventoryResources_GetResourceTypes_m27341909_MetadataUsageId;
extern String_t* _stringLiteral3128803744;
extern const uint32_t Jump__ctor_m2243614027_MetadataUsageId;
extern const uint32_t Jump_Update_m2663392189_MetadataUsageId;
extern String_t* _stringLiteral3452614544;
extern const uint32_t LoadLevelAction__ctor_m1384665989_MetadataUsageId;
extern const uint32_t LoadLevelAction_ExecuteAction_m1200367372_MetadataUsageId;
extern const uint32_t ModifyHealthAttribute_Reset_m296152965_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisHealthSystemAttribute_t2647166000_m3641020519_RuntimeMethod_var;
extern const uint32_t ModifyHealthAttribute_OnTriggerEnter2D_m197713471_MetadataUsageId;
extern String_t* _stringLiteral1828639942;
extern String_t* _stringLiteral2984908384;
extern String_t* _stringLiteral4105162317;
extern String_t* _stringLiteral2167972859;
extern String_t* _stringLiteral1613539661;
extern const uint32_t Move_Update_m2605015955_MetadataUsageId;
extern const uint32_t Move_FixedUpdate_m2825193255_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisBoxCollider2D_t3581341831_m414724273_RuntimeMethod_var;
extern const uint32_t ObjectCreatorArea_Start_m1518290345_MetadataUsageId;
extern RuntimeClass* U3CSpawnObjectU3Ec__Iterator0_t3452356763_il2cpp_TypeInfo_var;
extern const uint32_t ObjectCreatorArea_SpawnObject_m1647072653_MetadataUsageId;
extern RuntimeClass* WaitForSeconds_t1699091251_il2cpp_TypeInfo_var;
extern const uint32_t U3CSpawnObjectU3Ec__Iterator0_MoveNext_m1266700745_MetadataUsageId;
extern const RuntimeMethod* U3CSpawnObjectU3Ec__Iterator0_Reset_m1582788942_RuntimeMethod_var;
extern const uint32_t U3CSpawnObjectU3Ec__Iterator0_Reset_m1582788942_MetadataUsageId;
extern const uint32_t ObjectShooter_Start_m1222625711_MetadataUsageId;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisRigidbody2D_t939494601_m2767154013_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisBulletAttribute_t588811475_m718945470_RuntimeMethod_var;
extern const uint32_t ObjectShooter_Update_m4161465741_MetadataUsageId;
extern const uint32_t ObjectShooter_OnDrawGizmosSelected_m2214603868_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var;
extern const uint32_t OnOffAction_ExecuteAction_m1936074012_MetadataUsageId;
extern RuntimeClass* Vector2U5BU5D_t1457185986_il2cpp_TypeInfo_var;
extern const uint32_t Patrol_Start_m2892979382_MetadataUsageId;
extern const uint32_t Patrol_FixedUpdate_m2597641244_MetadataUsageId;
extern const uint32_t Patrol_Reset_m2317667825_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var;
extern const uint32_t Physics2DObject_Awake_m2862751549_MetadataUsageId;
extern const uint32_t PickUpAndHold_Update_m2356086211_MetadataUsageId;
extern const uint32_t PickUpAndHold_Drop_m1762827602_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisPickUpAndHold_t4109171223_m2673350194_RuntimeMethod_var;
extern String_t* _stringLiteral4289369868;
extern const uint32_t PickUpAndHold_PickUp_m1915750871_MetadataUsageId;
extern const uint32_t Push_Update_m2660533297_MetadataUsageId;
extern const uint32_t Push_FixedUpdate_m920108308_MetadataUsageId;
extern const uint32_t Push_OnDrawGizmosSelected_m1355904309_MetadataUsageId;
extern const uint32_t ResourceAttribute_Start_m4290658858_MetadataUsageId;
extern const uint32_t ResourceAttribute_Reset_m2199242252_MetadataUsageId;
extern String_t* _stringLiteral975027493;
extern const uint32_t ResourceAttribute_OnTriggerEnter2D_m3680524752_MetadataUsageId;
extern const uint32_t Rotate_Update_m2566458797_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisCircleCollider2D_t662546754_m1597288992_RuntimeMethod_var;
extern const uint32_t Stopper_Start_m3905942180_MetadataUsageId;
extern const uint32_t Stopper_Update_m235651721_MetadataUsageId;
extern const uint32_t Stopper_throwBall_m3126347534_MetadataUsageId;
extern const uint32_t Stopper_OnCollisionEnter2D_m1669770713_MetadataUsageId;
extern String_t* _stringLiteral2640249375;
extern const uint32_t Stopper_onHolder_m969266357_MetadataUsageId;
extern const uint32_t TeleportAction_ExecuteAction_m2630593480_MetadataUsageId;
extern String_t* _stringLiteral2460463356;
extern const uint32_t TimedSelfDestruct_Start_m4128724077_MetadataUsageId;
extern const uint32_t TimedSelfDestruct_DestroyMe_m477135811_MetadataUsageId;
extern RuntimeClass* TextU5BU5D_t422084607_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32U5BU5D_t385246372_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t4045795053_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m790289403_RuntimeMethod_var;
extern const uint32_t UIScript__ctor_m1214042822_MetadataUsageId;
extern String_t* _stringLiteral1512031223;
extern String_t* _stringLiteral3148363280;
extern const uint32_t UIScript_Start_m923733378_MetadataUsageId;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral709948148;
extern String_t* _stringLiteral48364148;
extern const uint32_t UIScript_GameWon_m3372413357_MetadataUsageId;
extern RuntimeClass* ResourceStruct_t862114426_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m537408582_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m3023042062_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisUIItemScript_t1692532892_m3238252138_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m1464489386_RuntimeMethod_var;
extern const uint32_t UIScript_AddResource_m2508601531_MetadataUsageId;
extern const uint32_t UIScript_CheckIfHasResources_m685094672_MetadataUsageId;
extern const uint32_t UIScript_ConsumeResource_m3280961331_MetadataUsageId;
extern const RuntimeMethod* Resources_Load_TisMesh_t3648964284_m3514705491_RuntimeMethod_var;
extern String_t* _stringLiteral1095709682;
extern String_t* _stringLiteral724398280;
extern String_t* _stringLiteral1284005273;
extern const uint32_t Utils__cctor_m1792585047_MetadataUsageId;
extern const uint32_t Utils_SetAxisTowards_m1999213583_MetadataUsageId;
extern const uint32_t Utils_GetVectorFromAxis_m3396922722_MetadataUsageId;
extern const uint32_t Utils_DrawMoveArrowGizmo_m4283051242_MetadataUsageId;
extern const uint32_t Utils_DrawShootArrowGizmo_m2541881908_MetadataUsageId;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern const uint32_t Utils_DrawRotateArrowGizmo_m3937198024_MetadataUsageId;
extern const uint32_t Utils_DrawGizmo_m3328925645_MetadataUsageId;
extern const uint32_t Utils_Angle_m3844311520_MetadataUsageId;
extern RuntimeClass* U3CChangeDirectionU3Ec__Iterator0_t1842818970_il2cpp_TypeInfo_var;
extern const uint32_t Wander_ChangeDirection_m1837724728_MetadataUsageId;
extern const uint32_t Wander_FixedUpdate_m2471657602_MetadataUsageId;
extern const uint32_t U3CChangeDirectionU3Ec__Iterator0_MoveNext_m2145628392_MetadataUsageId;
extern const RuntimeMethod* U3CChangeDirectionU3Ec__Iterator0_Reset_m401157713_RuntimeMethod_var;
extern const uint32_t U3CChangeDirectionU3Ec__Iterator0_Reset_m401157713_MetadataUsageId;
struct ContactPoint2D_t3390240644 ;

struct StringU5BU5D_t1281789340;
struct Vector2U5BU5D_t1457185986;
struct GameObjectU5BU5D_t3328599146;
struct TextU5BU5D_t422084607;
struct Int32U5BU5D_t385246372;


#ifndef U3CMODULEU3E_T692745542_H
#define U3CMODULEU3E_T692745542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745542 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745542_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T3319525431_H
#define LIST_1_T3319525431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.String>
struct  List_1_t3319525431  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t1281789340* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____items_1)); }
	inline StringU5BU5D_t1281789340* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t1281789340** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t1281789340* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3319525431_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	StringU5BU5D_t1281789340* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3319525431_StaticFields, ___EmptyArray_4)); }
	inline StringU5BU5D_t1281789340* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline StringU5BU5D_t1281789340** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(StringU5BU5D_t1281789340* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3319525431_H
#ifndef ENUMS_T1053268879_H
#define ENUMS_T1053268879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enums
struct  Enums_t1053268879  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMS_T1053268879_H
#ifndef U3CWAITFORBALLONDESTROYEDU3EC__ITERATOR0_T3522846479_H
#define U3CWAITFORBALLONDESTROYEDU3EC__ITERATOR0_T3522846479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DialogueBalloonAction/<WaitForBallonDestroyed>c__Iterator0
struct  U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479  : public RuntimeObject
{
public:
	// DialogueBalloonAction DialogueBalloonAction/<WaitForBallonDestroyed>c__Iterator0::$this
	DialogueBalloonAction_t3348434864 * ___U24this_0;
	// System.Object DialogueBalloonAction/<WaitForBallonDestroyed>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean DialogueBalloonAction/<WaitForBallonDestroyed>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 DialogueBalloonAction/<WaitForBallonDestroyed>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479, ___U24this_0)); }
	inline DialogueBalloonAction_t3348434864 * get_U24this_0() const { return ___U24this_0; }
	inline DialogueBalloonAction_t3348434864 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(DialogueBalloonAction_t3348434864 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORBALLONDESTROYEDU3EC__ITERATOR0_T3522846479_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef LIST_1_T547830642_H
#define LIST_1_T547830642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Action>
struct  List_1_t547830642  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ActionU5BU5D_t2307473173* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t547830642, ____items_1)); }
	inline ActionU5BU5D_t2307473173* get__items_1() const { return ____items_1; }
	inline ActionU5BU5D_t2307473173** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ActionU5BU5D_t2307473173* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t547830642, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t547830642, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t547830642_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ActionU5BU5D_t2307473173* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t547830642_StaticFields, ___EmptyArray_4)); }
	inline ActionU5BU5D_t2307473173* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ActionU5BU5D_t2307473173** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ActionU5BU5D_t2307473173* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T547830642_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef U3CSPAWNOBJECTU3EC__ITERATOR0_T3452356763_H
#define U3CSPAWNOBJECTU3EC__ITERATOR0_T3452356763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectCreatorArea/<SpawnObject>c__Iterator0
struct  U3CSpawnObjectU3Ec__Iterator0_t3452356763  : public RuntimeObject
{
public:
	// System.Single ObjectCreatorArea/<SpawnObject>c__Iterator0::<randomX>__1
	float ___U3CrandomXU3E__1_0;
	// System.Single ObjectCreatorArea/<SpawnObject>c__Iterator0::<randomY>__1
	float ___U3CrandomYU3E__1_1;
	// UnityEngine.GameObject ObjectCreatorArea/<SpawnObject>c__Iterator0::<newObject>__1
	GameObject_t1113636619 * ___U3CnewObjectU3E__1_2;
	// ObjectCreatorArea ObjectCreatorArea/<SpawnObject>c__Iterator0::$this
	ObjectCreatorArea_t9936620 * ___U24this_3;
	// System.Object ObjectCreatorArea/<SpawnObject>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean ObjectCreatorArea/<SpawnObject>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 ObjectCreatorArea/<SpawnObject>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CrandomXU3E__1_0() { return static_cast<int32_t>(offsetof(U3CSpawnObjectU3Ec__Iterator0_t3452356763, ___U3CrandomXU3E__1_0)); }
	inline float get_U3CrandomXU3E__1_0() const { return ___U3CrandomXU3E__1_0; }
	inline float* get_address_of_U3CrandomXU3E__1_0() { return &___U3CrandomXU3E__1_0; }
	inline void set_U3CrandomXU3E__1_0(float value)
	{
		___U3CrandomXU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CrandomYU3E__1_1() { return static_cast<int32_t>(offsetof(U3CSpawnObjectU3Ec__Iterator0_t3452356763, ___U3CrandomYU3E__1_1)); }
	inline float get_U3CrandomYU3E__1_1() const { return ___U3CrandomYU3E__1_1; }
	inline float* get_address_of_U3CrandomYU3E__1_1() { return &___U3CrandomYU3E__1_1; }
	inline void set_U3CrandomYU3E__1_1(float value)
	{
		___U3CrandomYU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CnewObjectU3E__1_2() { return static_cast<int32_t>(offsetof(U3CSpawnObjectU3Ec__Iterator0_t3452356763, ___U3CnewObjectU3E__1_2)); }
	inline GameObject_t1113636619 * get_U3CnewObjectU3E__1_2() const { return ___U3CnewObjectU3E__1_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CnewObjectU3E__1_2() { return &___U3CnewObjectU3E__1_2; }
	inline void set_U3CnewObjectU3E__1_2(GameObject_t1113636619 * value)
	{
		___U3CnewObjectU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnewObjectU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CSpawnObjectU3Ec__Iterator0_t3452356763, ___U24this_3)); }
	inline ObjectCreatorArea_t9936620 * get_U24this_3() const { return ___U24this_3; }
	inline ObjectCreatorArea_t9936620 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ObjectCreatorArea_t9936620 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CSpawnObjectU3Ec__Iterator0_t3452356763, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CSpawnObjectU3Ec__Iterator0_t3452356763, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSpawnObjectU3Ec__Iterator0_t3452356763, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNOBJECTU3EC__ITERATOR0_T3452356763_H
#ifndef CUSTOMYIELDINSTRUCTION_T1895667560_H
#define CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1895667560  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CCHANGEDIRECTIONU3EC__ITERATOR0_T1842818970_H
#define U3CCHANGEDIRECTIONU3EC__ITERATOR0_T1842818970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wander/<ChangeDirection>c__Iterator0
struct  U3CChangeDirectionU3Ec__Iterator0_t1842818970  : public RuntimeObject
{
public:
	// Wander Wander/<ChangeDirection>c__Iterator0::$this
	Wander_t651605530 * ___U24this_0;
	// System.Object Wander/<ChangeDirection>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Wander/<ChangeDirection>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Wander/<ChangeDirection>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CChangeDirectionU3Ec__Iterator0_t1842818970, ___U24this_0)); }
	inline Wander_t651605530 * get_U24this_0() const { return ___U24this_0; }
	inline Wander_t651605530 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Wander_t651605530 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CChangeDirectionU3Ec__Iterator0_t1842818970, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CChangeDirectionU3Ec__Iterator0_t1842818970, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CChangeDirectionU3Ec__Iterator0_t1842818970, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGEDIRECTIONU3EC__ITERATOR0_T1842818970_H
#ifndef UTILS_T1444179947_H
#define UTILS_T1444179947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils
struct  Utils_t1444179947  : public RuntimeObject
{
public:

public:
};

struct Utils_t1444179947_StaticFields
{
public:
	// UnityEngine.Mesh Utils::moveArrowMesh
	Mesh_t3648964284 * ___moveArrowMesh_0;
	// UnityEngine.Mesh Utils::shootArrowMesh
	Mesh_t3648964284 * ___shootArrowMesh_1;
	// UnityEngine.Mesh Utils::rotateArrowMesh
	Mesh_t3648964284 * ___rotateArrowMesh_2;

public:
	inline static int32_t get_offset_of_moveArrowMesh_0() { return static_cast<int32_t>(offsetof(Utils_t1444179947_StaticFields, ___moveArrowMesh_0)); }
	inline Mesh_t3648964284 * get_moveArrowMesh_0() const { return ___moveArrowMesh_0; }
	inline Mesh_t3648964284 ** get_address_of_moveArrowMesh_0() { return &___moveArrowMesh_0; }
	inline void set_moveArrowMesh_0(Mesh_t3648964284 * value)
	{
		___moveArrowMesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___moveArrowMesh_0), value);
	}

	inline static int32_t get_offset_of_shootArrowMesh_1() { return static_cast<int32_t>(offsetof(Utils_t1444179947_StaticFields, ___shootArrowMesh_1)); }
	inline Mesh_t3648964284 * get_shootArrowMesh_1() const { return ___shootArrowMesh_1; }
	inline Mesh_t3648964284 ** get_address_of_shootArrowMesh_1() { return &___shootArrowMesh_1; }
	inline void set_shootArrowMesh_1(Mesh_t3648964284 * value)
	{
		___shootArrowMesh_1 = value;
		Il2CppCodeGenWriteBarrier((&___shootArrowMesh_1), value);
	}

	inline static int32_t get_offset_of_rotateArrowMesh_2() { return static_cast<int32_t>(offsetof(Utils_t1444179947_StaticFields, ___rotateArrowMesh_2)); }
	inline Mesh_t3648964284 * get_rotateArrowMesh_2() const { return ___rotateArrowMesh_2; }
	inline Mesh_t3648964284 ** get_address_of_rotateArrowMesh_2() { return &___rotateArrowMesh_2; }
	inline void set_rotateArrowMesh_2(Mesh_t3648964284 * value)
	{
		___rotateArrowMesh_2 = value;
		Il2CppCodeGenWriteBarrier((&___rotateArrowMesh_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T1444179947_H
#ifndef DICTIONARY_2_T4045795053_H
#define DICTIONARY_2_T4045795053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,ResourceStruct>
struct  Dictionary_2_t4045795053  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	Int32U5BU5D_t385246372* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ResourceStructU5BU5D_t1642021471* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t4045795053, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t4045795053, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t4045795053, ___keySlots_6)); }
	inline Int32U5BU5D_t385246372* get_keySlots_6() const { return ___keySlots_6; }
	inline Int32U5BU5D_t385246372** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(Int32U5BU5D_t385246372* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t4045795053, ___valueSlots_7)); }
	inline ResourceStructU5BU5D_t1642021471* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ResourceStructU5BU5D_t1642021471** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ResourceStructU5BU5D_t1642021471* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t4045795053, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t4045795053, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t4045795053, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t4045795053, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t4045795053, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t4045795053, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t4045795053, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t4045795053_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1246360315 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t4045795053_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1246360315 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1246360315 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1246360315 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T4045795053_H
#ifndef RESOURCESTRUCT_T862114426_H
#define RESOURCESTRUCT_T862114426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceStruct
struct  ResourceStruct_t862114426  : public RuntimeObject
{
public:
	// System.Int32 ResourceStruct::amount
	int32_t ___amount_0;
	// UIItemScript ResourceStruct::UIItem
	UIItemScript_t1692532892 * ___UIItem_1;

public:
	inline static int32_t get_offset_of_amount_0() { return static_cast<int32_t>(offsetof(ResourceStruct_t862114426, ___amount_0)); }
	inline int32_t get_amount_0() const { return ___amount_0; }
	inline int32_t* get_address_of_amount_0() { return &___amount_0; }
	inline void set_amount_0(int32_t value)
	{
		___amount_0 = value;
	}

	inline static int32_t get_offset_of_UIItem_1() { return static_cast<int32_t>(offsetof(ResourceStruct_t862114426, ___UIItem_1)); }
	inline UIItemScript_t1692532892 * get_UIItem_1() const { return ___UIItem_1; }
	inline UIItemScript_t1692532892 ** get_address_of_UIItem_1() { return &___UIItem_1; }
	inline void set_UIItem_1(UIItemScript_t1692532892 * value)
	{
		___UIItem_1 = value;
		Il2CppCodeGenWriteBarrier((&___UIItem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCESTRUCT_T862114426_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef UNITYEVENT_T2581268647_H
#define UNITYEVENT_T2581268647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2581268647  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2581268647, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2581268647_H
#ifndef WAITUNTIL_T3373419216_H
#define WAITUNTIL_T3373419216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitUntil
struct  WaitUntil_t3373419216  : public CustomYieldInstruction_t1895667560
{
public:
	// System.Func`1<System.Boolean> UnityEngine.WaitUntil::m_Predicate
	Func_1_t3822001908 * ___m_Predicate_0;

public:
	inline static int32_t get_offset_of_m_Predicate_0() { return static_cast<int32_t>(offsetof(WaitUntil_t3373419216, ___m_Predicate_0)); }
	inline Func_1_t3822001908 * get_m_Predicate_0() const { return ___m_Predicate_0; }
	inline Func_1_t3822001908 ** get_address_of_m_Predicate_0() { return &___m_Predicate_0; }
	inline void set_m_Predicate_0(Func_1_t3822001908 * value)
	{
		___m_Predicate_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Predicate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITUNTIL_T3373419216_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t257213610 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___l_0)); }
	inline List_1_t257213610 * get_l_0() const { return ___l_0; }
	inline List_1_t257213610 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t257213610 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef ENUMERATOR_T2437074519_H
#define ENUMERATOR_T2437074519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Action>
struct  Enumerator_t2437074519 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t547830642 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Action_t3370723196 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2437074519, ___l_0)); }
	inline List_1_t547830642 * get_l_0() const { return ___l_0; }
	inline List_1_t547830642 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t547830642 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2437074519, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2437074519, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2437074519, ___current_3)); }
	inline Action_t3370723196 * get_current_3() const { return ___current_3; }
	inline Action_t3370723196 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Action_t3370723196 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2437074519_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef WAITFORSECONDS_T1699091251_H
#define WAITFORSECONDS_T1699091251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t1699091251  : public YieldInstruction_t403091072
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t1699091251, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T1699091251_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef SCENE_T2348375561_H
#define SCENE_T2348375561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t2348375561 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t2348375561, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T2348375561_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef LOADSCENEMODE_T3251202195_H
#define LOADSCENEMODE_T3251202195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_t3251202195 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoadSceneMode_t3251202195, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENEMODE_T3251202195_H
#ifndef TARGETS_T3221151245_H
#define TARGETS_T3221151245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enums/Targets
struct  Targets_t3221151245 
{
public:
	// System.Int32 Enums/Targets::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Targets_t3221151245, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETS_T3221151245_H
#ifndef RIGIDBODYTYPE2D_T1648102732_H
#define RIGIDBODYTYPE2D_T1648102732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RigidbodyType2D
struct  RigidbodyType2D_t1648102732 
{
public:
	// System.Int32 UnityEngine.RigidbodyType2D::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RigidbodyType2D_t1648102732, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYTYPE2D_T1648102732_H
#ifndef DIRECTIONS_T3759223838_H
#define DIRECTIONS_T3759223838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enums/Directions
struct  Directions_t3759223838 
{
public:
	// System.Int32 Enums/Directions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Directions_t3759223838, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONS_T3759223838_H
#ifndef PLAYERS_T434611418_H
#define PLAYERS_T434611418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enums/Players
struct  Players_t434611418 
{
public:
	// System.Int32 Enums/Players::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Players_t434611418, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERS_T434611418_H
#ifndef MOVEMENTTYPE_T2977684614_H
#define MOVEMENTTYPE_T2977684614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enums/MovementType
struct  MovementType_t2977684614 
{
public:
	// System.Int32 Enums/MovementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MovementType_t2977684614, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTTYPE_T2977684614_H
#ifndef KEYGROUPS_T2888070947_H
#define KEYGROUPS_T2888070947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enums/KeyGroups
struct  KeyGroups_t2888070947 
{
public:
	// System.Int32 Enums/KeyGroups::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyGroups_t2888070947, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYGROUPS_T2888070947_H
#ifndef AXES_T766252756_H
#define AXES_T766252756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enums/Axes
struct  Axes_t766252756 
{
public:
	// System.Int32 Enums/Axes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axes_t766252756, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXES_T766252756_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef DISAPPEARMODE_T990233539_H
#define DISAPPEARMODE_T990233539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DialogueBalloonAction/DisappearMode
struct  DisappearMode_t990233539 
{
public:
	// System.Int32 DialogueBalloonAction/DisappearMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DisappearMode_t990233539, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISAPPEARMODE_T990233539_H
#ifndef PLAYERS_T356725142_H
#define PLAYERS_T356725142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScript/Players
struct  Players_t356725142 
{
public:
	// System.Int32 UIScript/Players::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Players_t356725142, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERS_T356725142_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef FORCEMODE2D_T255358695_H
#define FORCEMODE2D_T255358695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ForceMode2D
struct  ForceMode2D_t255358695 
{
public:
	// System.Int32 UnityEngine.ForceMode2D::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ForceMode2D_t255358695, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEMODE2D_T255358695_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef COLLIDEREVENTTYPES_T1979526571_H
#define COLLIDEREVENTTYPES_T1979526571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConditionArea/ColliderEventTypes
struct  ColliderEventTypes_t1979526571 
{
public:
	// System.Int32 ConditionArea/ColliderEventTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColliderEventTypes_t1979526571, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDEREVENTTYPES_T1979526571_H
#ifndef KEYEVENTTYPES_T1139667724_H
#define KEYEVENTTYPES_T1139667724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConditionKeyPress/KeyEventTypes
struct  KeyEventTypes_t1139667724 
{
public:
	// System.Int32 ConditionKeyPress/KeyEventTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyEventTypes_t1139667724, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEVENTTYPES_T1139667724_H
#ifndef COLLISION2D_T2842956331_H
#define COLLISION2D_T2842956331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collision2D
struct  Collision2D_t2842956331  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Collision2D::m_Collider
	int32_t ___m_Collider_0;
	// System.Int32 UnityEngine.Collision2D::m_OtherCollider
	int32_t ___m_OtherCollider_1;
	// System.Int32 UnityEngine.Collision2D::m_Rigidbody
	int32_t ___m_Rigidbody_2;
	// System.Int32 UnityEngine.Collision2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_3;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_Contacts
	ContactPoint2DU5BU5D_t96683501* ___m_Contacts_4;
	// UnityEngine.Vector2 UnityEngine.Collision2D::m_RelativeVelocity
	Vector2_t2156229523  ___m_RelativeVelocity_5;
	// System.Int32 UnityEngine.Collision2D::m_Enabled
	int32_t ___m_Enabled_6;

public:
	inline static int32_t get_offset_of_m_Collider_0() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_Collider_0)); }
	inline int32_t get_m_Collider_0() const { return ___m_Collider_0; }
	inline int32_t* get_address_of_m_Collider_0() { return &___m_Collider_0; }
	inline void set_m_Collider_0(int32_t value)
	{
		___m_Collider_0 = value;
	}

	inline static int32_t get_offset_of_m_OtherCollider_1() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_OtherCollider_1)); }
	inline int32_t get_m_OtherCollider_1() const { return ___m_OtherCollider_1; }
	inline int32_t* get_address_of_m_OtherCollider_1() { return &___m_OtherCollider_1; }
	inline void set_m_OtherCollider_1(int32_t value)
	{
		___m_OtherCollider_1 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_2() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_Rigidbody_2)); }
	inline int32_t get_m_Rigidbody_2() const { return ___m_Rigidbody_2; }
	inline int32_t* get_address_of_m_Rigidbody_2() { return &___m_Rigidbody_2; }
	inline void set_m_Rigidbody_2(int32_t value)
	{
		___m_Rigidbody_2 = value;
	}

	inline static int32_t get_offset_of_m_OtherRigidbody_3() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_OtherRigidbody_3)); }
	inline int32_t get_m_OtherRigidbody_3() const { return ___m_OtherRigidbody_3; }
	inline int32_t* get_address_of_m_OtherRigidbody_3() { return &___m_OtherRigidbody_3; }
	inline void set_m_OtherRigidbody_3(int32_t value)
	{
		___m_OtherRigidbody_3 = value;
	}

	inline static int32_t get_offset_of_m_Contacts_4() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_Contacts_4)); }
	inline ContactPoint2DU5BU5D_t96683501* get_m_Contacts_4() const { return ___m_Contacts_4; }
	inline ContactPoint2DU5BU5D_t96683501** get_address_of_m_Contacts_4() { return &___m_Contacts_4; }
	inline void set_m_Contacts_4(ContactPoint2DU5BU5D_t96683501* value)
	{
		___m_Contacts_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Contacts_4), value);
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_5() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_RelativeVelocity_5)); }
	inline Vector2_t2156229523  get_m_RelativeVelocity_5() const { return ___m_RelativeVelocity_5; }
	inline Vector2_t2156229523 * get_address_of_m_RelativeVelocity_5() { return &___m_RelativeVelocity_5; }
	inline void set_m_RelativeVelocity_5(Vector2_t2156229523  value)
	{
		___m_RelativeVelocity_5 = value;
	}

	inline static int32_t get_offset_of_m_Enabled_6() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_Enabled_6)); }
	inline int32_t get_m_Enabled_6() const { return ___m_Enabled_6; }
	inline int32_t* get_address_of_m_Enabled_6() { return &___m_Enabled_6; }
	inline void set_m_Enabled_6(int32_t value)
	{
		___m_Enabled_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Collision2D
struct Collision2D_t2842956331_marshaled_pinvoke
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	ContactPoint2D_t3390240644 * ___m_Contacts_4;
	Vector2_t2156229523  ___m_RelativeVelocity_5;
	int32_t ___m_Enabled_6;
};
// Native definition for COM marshalling of UnityEngine.Collision2D
struct Collision2D_t2842956331_marshaled_com
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	ContactPoint2D_t3390240644 * ___m_Contacts_4;
	Vector2_t2156229523  ___m_RelativeVelocity_5;
	int32_t ___m_Enabled_6;
};
#endif // COLLISION2D_T2842956331_H
#ifndef GAMETYPE_T1121171700_H
#define GAMETYPE_T1121171700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScript/GameType
struct  GameType_t1121171700 
{
public:
	// System.Int32 UIScript/GameType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GameType_t1121171700, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMETYPE_T1121171700_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef MESH_T3648964284_H
#define MESH_T3648964284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t3648964284  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T3648964284_H
#ifndef INVENTORYRESOURCES_T222662465_H
#define INVENTORYRESOURCES_T222662465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryResources
struct  InventoryResources_t222662465  : public ScriptableObject_t2528358522
{
public:
	// System.Collections.Generic.List`1<System.String> InventoryResources::resourcesTypes
	List_1_t3319525431 * ___resourcesTypes_2;

public:
	inline static int32_t get_offset_of_resourcesTypes_2() { return static_cast<int32_t>(offsetof(InventoryResources_t222662465, ___resourcesTypes_2)); }
	inline List_1_t3319525431 * get_resourcesTypes_2() const { return ___resourcesTypes_2; }
	inline List_1_t3319525431 ** get_address_of_resourcesTypes_2() { return &___resourcesTypes_2; }
	inline void set_resourcesTypes_2(List_1_t3319525431 * value)
	{
		___resourcesTypes_2 = value;
		Il2CppCodeGenWriteBarrier((&___resourcesTypes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYRESOURCES_T222662465_H
#ifndef RIGIDBODY2D_T939494601_H
#define RIGIDBODY2D_T939494601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_t939494601  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_T939494601_H
#ifndef UNITYACTION_T3245792599_H
#define UNITYACTION_T3245792599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction
struct  UnityAction_t3245792599  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_T3245792599_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef FUNC_1_T3822001908_H
#define FUNC_1_T3822001908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`1<System.Boolean>
struct  Func_1_t3822001908  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_1_T3822001908_H
#ifndef SPRITERENDERER_T3235626157_H
#define SPRITERENDERER_T3235626157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3235626157  : public Renderer_t2627027031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T3235626157_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t190067161 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t190067161 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t190067161 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t190067161 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t190067161 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t190067161 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef RECTTRANSFORM_T3704657025_H
#define RECTTRANSFORM_T3704657025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3704657025  : public Transform_t3600365921
{
public:

public:
};

struct RectTransform_t3704657025_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1258266594 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t1258266594 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t1258266594 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3704657025_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef COLLIDER2D_T2806799626_H
#define COLLIDER2D_T2806799626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t2806799626  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T2806799626_H
#ifndef PICKUPANDHOLD_T4109171223_H
#define PICKUPANDHOLD_T4109171223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickUpAndHold
struct  PickUpAndHold_t4109171223  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.KeyCode PickUpAndHold::pickupKey
	int32_t ___pickupKey_2;
	// UnityEngine.KeyCode PickUpAndHold::dropKey
	int32_t ___dropKey_3;
	// System.Single PickUpAndHold::pickUpDistance
	float ___pickUpDistance_4;
	// UnityEngine.Transform PickUpAndHold::carriedObject
	Transform_t3600365921 * ___carriedObject_5;

public:
	inline static int32_t get_offset_of_pickupKey_2() { return static_cast<int32_t>(offsetof(PickUpAndHold_t4109171223, ___pickupKey_2)); }
	inline int32_t get_pickupKey_2() const { return ___pickupKey_2; }
	inline int32_t* get_address_of_pickupKey_2() { return &___pickupKey_2; }
	inline void set_pickupKey_2(int32_t value)
	{
		___pickupKey_2 = value;
	}

	inline static int32_t get_offset_of_dropKey_3() { return static_cast<int32_t>(offsetof(PickUpAndHold_t4109171223, ___dropKey_3)); }
	inline int32_t get_dropKey_3() const { return ___dropKey_3; }
	inline int32_t* get_address_of_dropKey_3() { return &___dropKey_3; }
	inline void set_dropKey_3(int32_t value)
	{
		___dropKey_3 = value;
	}

	inline static int32_t get_offset_of_pickUpDistance_4() { return static_cast<int32_t>(offsetof(PickUpAndHold_t4109171223, ___pickUpDistance_4)); }
	inline float get_pickUpDistance_4() const { return ___pickUpDistance_4; }
	inline float* get_address_of_pickUpDistance_4() { return &___pickUpDistance_4; }
	inline void set_pickUpDistance_4(float value)
	{
		___pickUpDistance_4 = value;
	}

	inline static int32_t get_offset_of_carriedObject_5() { return static_cast<int32_t>(offsetof(PickUpAndHold_t4109171223, ___carriedObject_5)); }
	inline Transform_t3600365921 * get_carriedObject_5() const { return ___carriedObject_5; }
	inline Transform_t3600365921 ** get_address_of_carriedObject_5() { return &___carriedObject_5; }
	inline void set_carriedObject_5(Transform_t3600365921 * value)
	{
		___carriedObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___carriedObject_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKUPANDHOLD_T4109171223_H
#ifndef OBJECTSHOOTER_T734116591_H
#define OBJECTSHOOTER_T734116591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectShooter
struct  ObjectShooter_t734116591  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ObjectShooter::prefabToSpawn
	GameObject_t1113636619 * ___prefabToSpawn_2;
	// UnityEngine.KeyCode ObjectShooter::keyToPress
	int32_t ___keyToPress_3;
	// System.Single ObjectShooter::creationRate
	float ___creationRate_4;
	// System.Single ObjectShooter::shootSpeed
	float ___shootSpeed_5;
	// UnityEngine.Vector2 ObjectShooter::shootDirection
	Vector2_t2156229523  ___shootDirection_6;
	// System.Boolean ObjectShooter::relativeToRotation
	bool ___relativeToRotation_7;
	// System.Single ObjectShooter::timeOfLastSpawn
	float ___timeOfLastSpawn_8;
	// System.Int32 ObjectShooter::playerNumber
	int32_t ___playerNumber_9;

public:
	inline static int32_t get_offset_of_prefabToSpawn_2() { return static_cast<int32_t>(offsetof(ObjectShooter_t734116591, ___prefabToSpawn_2)); }
	inline GameObject_t1113636619 * get_prefabToSpawn_2() const { return ___prefabToSpawn_2; }
	inline GameObject_t1113636619 ** get_address_of_prefabToSpawn_2() { return &___prefabToSpawn_2; }
	inline void set_prefabToSpawn_2(GameObject_t1113636619 * value)
	{
		___prefabToSpawn_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefabToSpawn_2), value);
	}

	inline static int32_t get_offset_of_keyToPress_3() { return static_cast<int32_t>(offsetof(ObjectShooter_t734116591, ___keyToPress_3)); }
	inline int32_t get_keyToPress_3() const { return ___keyToPress_3; }
	inline int32_t* get_address_of_keyToPress_3() { return &___keyToPress_3; }
	inline void set_keyToPress_3(int32_t value)
	{
		___keyToPress_3 = value;
	}

	inline static int32_t get_offset_of_creationRate_4() { return static_cast<int32_t>(offsetof(ObjectShooter_t734116591, ___creationRate_4)); }
	inline float get_creationRate_4() const { return ___creationRate_4; }
	inline float* get_address_of_creationRate_4() { return &___creationRate_4; }
	inline void set_creationRate_4(float value)
	{
		___creationRate_4 = value;
	}

	inline static int32_t get_offset_of_shootSpeed_5() { return static_cast<int32_t>(offsetof(ObjectShooter_t734116591, ___shootSpeed_5)); }
	inline float get_shootSpeed_5() const { return ___shootSpeed_5; }
	inline float* get_address_of_shootSpeed_5() { return &___shootSpeed_5; }
	inline void set_shootSpeed_5(float value)
	{
		___shootSpeed_5 = value;
	}

	inline static int32_t get_offset_of_shootDirection_6() { return static_cast<int32_t>(offsetof(ObjectShooter_t734116591, ___shootDirection_6)); }
	inline Vector2_t2156229523  get_shootDirection_6() const { return ___shootDirection_6; }
	inline Vector2_t2156229523 * get_address_of_shootDirection_6() { return &___shootDirection_6; }
	inline void set_shootDirection_6(Vector2_t2156229523  value)
	{
		___shootDirection_6 = value;
	}

	inline static int32_t get_offset_of_relativeToRotation_7() { return static_cast<int32_t>(offsetof(ObjectShooter_t734116591, ___relativeToRotation_7)); }
	inline bool get_relativeToRotation_7() const { return ___relativeToRotation_7; }
	inline bool* get_address_of_relativeToRotation_7() { return &___relativeToRotation_7; }
	inline void set_relativeToRotation_7(bool value)
	{
		___relativeToRotation_7 = value;
	}

	inline static int32_t get_offset_of_timeOfLastSpawn_8() { return static_cast<int32_t>(offsetof(ObjectShooter_t734116591, ___timeOfLastSpawn_8)); }
	inline float get_timeOfLastSpawn_8() const { return ___timeOfLastSpawn_8; }
	inline float* get_address_of_timeOfLastSpawn_8() { return &___timeOfLastSpawn_8; }
	inline void set_timeOfLastSpawn_8(float value)
	{
		___timeOfLastSpawn_8 = value;
	}

	inline static int32_t get_offset_of_playerNumber_9() { return static_cast<int32_t>(offsetof(ObjectShooter_t734116591, ___playerNumber_9)); }
	inline int32_t get_playerNumber_9() const { return ___playerNumber_9; }
	inline int32_t* get_address_of_playerNumber_9() { return &___playerNumber_9; }
	inline void set_playerNumber_9(int32_t value)
	{
		___playerNumber_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSHOOTER_T734116591_H
#ifndef RESOURCEATTRIBUTE_T892502608_H
#define RESOURCEATTRIBUTE_T892502608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceAttribute
struct  ResourceAttribute_t892502608  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 ResourceAttribute::resourceIndex
	int32_t ___resourceIndex_2;
	// System.Int32 ResourceAttribute::amount
	int32_t ___amount_3;
	// UIScript ResourceAttribute::userInterface
	UIScript_t1195624422 * ___userInterface_4;

public:
	inline static int32_t get_offset_of_resourceIndex_2() { return static_cast<int32_t>(offsetof(ResourceAttribute_t892502608, ___resourceIndex_2)); }
	inline int32_t get_resourceIndex_2() const { return ___resourceIndex_2; }
	inline int32_t* get_address_of_resourceIndex_2() { return &___resourceIndex_2; }
	inline void set_resourceIndex_2(int32_t value)
	{
		___resourceIndex_2 = value;
	}

	inline static int32_t get_offset_of_amount_3() { return static_cast<int32_t>(offsetof(ResourceAttribute_t892502608, ___amount_3)); }
	inline int32_t get_amount_3() const { return ___amount_3; }
	inline int32_t* get_address_of_amount_3() { return &___amount_3; }
	inline void set_amount_3(int32_t value)
	{
		___amount_3 = value;
	}

	inline static int32_t get_offset_of_userInterface_4() { return static_cast<int32_t>(offsetof(ResourceAttribute_t892502608, ___userInterface_4)); }
	inline UIScript_t1195624422 * get_userInterface_4() const { return ___userInterface_4; }
	inline UIScript_t1195624422 ** get_address_of_userInterface_4() { return &___userInterface_4; }
	inline void set_userInterface_4(UIScript_t1195624422 * value)
	{
		___userInterface_4 = value;
		Il2CppCodeGenWriteBarrier((&___userInterface_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCEATTRIBUTE_T892502608_H
#ifndef ACTION_T3370723196_H
#define ACTION_T3370723196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Action
struct  Action_t3370723196  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T3370723196_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef TIMEDSELFDESTRUCT_T3957973201_H
#define TIMEDSELFDESTRUCT_T3957973201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimedSelfDestruct
struct  TimedSelfDestruct_t3957973201  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TimedSelfDestruct::timeToDestruction
	float ___timeToDestruction_2;

public:
	inline static int32_t get_offset_of_timeToDestruction_2() { return static_cast<int32_t>(offsetof(TimedSelfDestruct_t3957973201, ___timeToDestruction_2)); }
	inline float get_timeToDestruction_2() const { return ___timeToDestruction_2; }
	inline float* get_address_of_timeToDestruction_2() { return &___timeToDestruction_2; }
	inline void set_timeToDestruction_2(float value)
	{
		___timeToDestruction_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDSELFDESTRUCT_T3957973201_H
#ifndef CIRCLECOLLIDER2D_T662546754_H
#define CIRCLECOLLIDER2D_T662546754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CircleCollider2D
struct  CircleCollider2D_t662546754  : public Collider2D_t2806799626
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIRCLECOLLIDER2D_T662546754_H
#ifndef STOPPER_T1976456672_H
#define STOPPER_T1976456672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Stopper
struct  Stopper_t1976456672  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody2D Stopper::rb2D
	Rigidbody2D_t939494601 * ___rb2D_2;
	// UnityEngine.SpriteRenderer Stopper::renderer
	SpriteRenderer_t3235626157 * ___renderer_3;
	// UnityEngine.CircleCollider2D Stopper::collider
	CircleCollider2D_t662546754 * ___collider_4;
	// System.Single Stopper::mass
	float ___mass_5;
	// UnityEngine.Transform Stopper::holder
	Transform_t3600365921 * ___holder_6;
	// UnityEngine.Transform Stopper::player
	Transform_t3600365921 * ___player_7;
	// System.Boolean Stopper::live
	bool ___live_9;

public:
	inline static int32_t get_offset_of_rb2D_2() { return static_cast<int32_t>(offsetof(Stopper_t1976456672, ___rb2D_2)); }
	inline Rigidbody2D_t939494601 * get_rb2D_2() const { return ___rb2D_2; }
	inline Rigidbody2D_t939494601 ** get_address_of_rb2D_2() { return &___rb2D_2; }
	inline void set_rb2D_2(Rigidbody2D_t939494601 * value)
	{
		___rb2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___rb2D_2), value);
	}

	inline static int32_t get_offset_of_renderer_3() { return static_cast<int32_t>(offsetof(Stopper_t1976456672, ___renderer_3)); }
	inline SpriteRenderer_t3235626157 * get_renderer_3() const { return ___renderer_3; }
	inline SpriteRenderer_t3235626157 ** get_address_of_renderer_3() { return &___renderer_3; }
	inline void set_renderer_3(SpriteRenderer_t3235626157 * value)
	{
		___renderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___renderer_3), value);
	}

	inline static int32_t get_offset_of_collider_4() { return static_cast<int32_t>(offsetof(Stopper_t1976456672, ___collider_4)); }
	inline CircleCollider2D_t662546754 * get_collider_4() const { return ___collider_4; }
	inline CircleCollider2D_t662546754 ** get_address_of_collider_4() { return &___collider_4; }
	inline void set_collider_4(CircleCollider2D_t662546754 * value)
	{
		___collider_4 = value;
		Il2CppCodeGenWriteBarrier((&___collider_4), value);
	}

	inline static int32_t get_offset_of_mass_5() { return static_cast<int32_t>(offsetof(Stopper_t1976456672, ___mass_5)); }
	inline float get_mass_5() const { return ___mass_5; }
	inline float* get_address_of_mass_5() { return &___mass_5; }
	inline void set_mass_5(float value)
	{
		___mass_5 = value;
	}

	inline static int32_t get_offset_of_holder_6() { return static_cast<int32_t>(offsetof(Stopper_t1976456672, ___holder_6)); }
	inline Transform_t3600365921 * get_holder_6() const { return ___holder_6; }
	inline Transform_t3600365921 ** get_address_of_holder_6() { return &___holder_6; }
	inline void set_holder_6(Transform_t3600365921 * value)
	{
		___holder_6 = value;
		Il2CppCodeGenWriteBarrier((&___holder_6), value);
	}

	inline static int32_t get_offset_of_player_7() { return static_cast<int32_t>(offsetof(Stopper_t1976456672, ___player_7)); }
	inline Transform_t3600365921 * get_player_7() const { return ___player_7; }
	inline Transform_t3600365921 ** get_address_of_player_7() { return &___player_7; }
	inline void set_player_7(Transform_t3600365921 * value)
	{
		___player_7 = value;
		Il2CppCodeGenWriteBarrier((&___player_7), value);
	}

	inline static int32_t get_offset_of_live_9() { return static_cast<int32_t>(offsetof(Stopper_t1976456672, ___live_9)); }
	inline bool get_live_9() const { return ___live_9; }
	inline bool* get_address_of_live_9() { return &___live_9; }
	inline void set_live_9(bool value)
	{
		___live_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOPPER_T1976456672_H
#ifndef SHOOT_T2202938192_H
#define SHOOT_T2202938192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shoot
struct  Shoot_t2202938192  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOOT_T2202938192_H
#ifndef UIITEMSCRIPT_T1692532892_H
#define UIITEMSCRIPT_T1692532892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIItemScript
struct  UIItemScript_t1692532892  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image UIItemScript::resourceIcon
	Image_t2670269651 * ___resourceIcon_2;
	// UnityEngine.UI.Text UIItemScript::resourceAmount
	Text_t1901882714 * ___resourceAmount_3;

public:
	inline static int32_t get_offset_of_resourceIcon_2() { return static_cast<int32_t>(offsetof(UIItemScript_t1692532892, ___resourceIcon_2)); }
	inline Image_t2670269651 * get_resourceIcon_2() const { return ___resourceIcon_2; }
	inline Image_t2670269651 ** get_address_of_resourceIcon_2() { return &___resourceIcon_2; }
	inline void set_resourceIcon_2(Image_t2670269651 * value)
	{
		___resourceIcon_2 = value;
		Il2CppCodeGenWriteBarrier((&___resourceIcon_2), value);
	}

	inline static int32_t get_offset_of_resourceAmount_3() { return static_cast<int32_t>(offsetof(UIItemScript_t1692532892, ___resourceAmount_3)); }
	inline Text_t1901882714 * get_resourceAmount_3() const { return ___resourceAmount_3; }
	inline Text_t1901882714 ** get_address_of_resourceAmount_3() { return &___resourceAmount_3; }
	inline void set_resourceAmount_3(Text_t1901882714 * value)
	{
		___resourceAmount_3 = value;
		Il2CppCodeGenWriteBarrier((&___resourceAmount_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIITEMSCRIPT_T1692532892_H
#ifndef PHYSICS2DOBJECT_T3026219368_H
#define PHYSICS2DOBJECT_T3026219368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Physics2DObject
struct  Physics2DObject_t3026219368  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody2D Physics2DObject::rigidbody2D
	Rigidbody2D_t939494601 * ___rigidbody2D_2;

public:
	inline static int32_t get_offset_of_rigidbody2D_2() { return static_cast<int32_t>(offsetof(Physics2DObject_t3026219368, ___rigidbody2D_2)); }
	inline Rigidbody2D_t939494601 * get_rigidbody2D_2() const { return ___rigidbody2D_2; }
	inline Rigidbody2D_t939494601 ** get_address_of_rigidbody2D_2() { return &___rigidbody2D_2; }
	inline void set_rigidbody2D_2(Rigidbody2D_t939494601 * value)
	{
		___rigidbody2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___rigidbody2D_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICS2DOBJECT_T3026219368_H
#ifndef BALLOONSCRIPT_T4017429074_H
#define BALLOONSCRIPT_T4017429074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BalloonScript
struct  BalloonScript_t4017429074  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text BalloonScript::dialogueText
	Text_t1901882714 * ___dialogueText_2;
	// UnityEngine.UI.Text BalloonScript::buttonText
	Text_t1901882714 * ___buttonText_3;
	// UnityEngine.Events.UnityAction BalloonScript::BalloonDestroyed
	UnityAction_t3245792599 * ___BalloonDestroyed_4;
	// UnityEngine.RectTransform BalloonScript::rectTransform
	RectTransform_t3704657025 * ___rectTransform_5;
	// System.Boolean BalloonScript::isUsingButton
	bool ___isUsingButton_6;
	// UnityEngine.KeyCode BalloonScript::buttonUsed
	int32_t ___buttonUsed_7;
	// UnityEngine.Transform BalloonScript::targetObj
	Transform_t3600365921 * ___targetObj_8;
	// System.Single BalloonScript::startTime
	float ___startTime_9;
	// System.Single BalloonScript::duration
	float ___duration_10;

public:
	inline static int32_t get_offset_of_dialogueText_2() { return static_cast<int32_t>(offsetof(BalloonScript_t4017429074, ___dialogueText_2)); }
	inline Text_t1901882714 * get_dialogueText_2() const { return ___dialogueText_2; }
	inline Text_t1901882714 ** get_address_of_dialogueText_2() { return &___dialogueText_2; }
	inline void set_dialogueText_2(Text_t1901882714 * value)
	{
		___dialogueText_2 = value;
		Il2CppCodeGenWriteBarrier((&___dialogueText_2), value);
	}

	inline static int32_t get_offset_of_buttonText_3() { return static_cast<int32_t>(offsetof(BalloonScript_t4017429074, ___buttonText_3)); }
	inline Text_t1901882714 * get_buttonText_3() const { return ___buttonText_3; }
	inline Text_t1901882714 ** get_address_of_buttonText_3() { return &___buttonText_3; }
	inline void set_buttonText_3(Text_t1901882714 * value)
	{
		___buttonText_3 = value;
		Il2CppCodeGenWriteBarrier((&___buttonText_3), value);
	}

	inline static int32_t get_offset_of_BalloonDestroyed_4() { return static_cast<int32_t>(offsetof(BalloonScript_t4017429074, ___BalloonDestroyed_4)); }
	inline UnityAction_t3245792599 * get_BalloonDestroyed_4() const { return ___BalloonDestroyed_4; }
	inline UnityAction_t3245792599 ** get_address_of_BalloonDestroyed_4() { return &___BalloonDestroyed_4; }
	inline void set_BalloonDestroyed_4(UnityAction_t3245792599 * value)
	{
		___BalloonDestroyed_4 = value;
		Il2CppCodeGenWriteBarrier((&___BalloonDestroyed_4), value);
	}

	inline static int32_t get_offset_of_rectTransform_5() { return static_cast<int32_t>(offsetof(BalloonScript_t4017429074, ___rectTransform_5)); }
	inline RectTransform_t3704657025 * get_rectTransform_5() const { return ___rectTransform_5; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_5() { return &___rectTransform_5; }
	inline void set_rectTransform_5(RectTransform_t3704657025 * value)
	{
		___rectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_5), value);
	}

	inline static int32_t get_offset_of_isUsingButton_6() { return static_cast<int32_t>(offsetof(BalloonScript_t4017429074, ___isUsingButton_6)); }
	inline bool get_isUsingButton_6() const { return ___isUsingButton_6; }
	inline bool* get_address_of_isUsingButton_6() { return &___isUsingButton_6; }
	inline void set_isUsingButton_6(bool value)
	{
		___isUsingButton_6 = value;
	}

	inline static int32_t get_offset_of_buttonUsed_7() { return static_cast<int32_t>(offsetof(BalloonScript_t4017429074, ___buttonUsed_7)); }
	inline int32_t get_buttonUsed_7() const { return ___buttonUsed_7; }
	inline int32_t* get_address_of_buttonUsed_7() { return &___buttonUsed_7; }
	inline void set_buttonUsed_7(int32_t value)
	{
		___buttonUsed_7 = value;
	}

	inline static int32_t get_offset_of_targetObj_8() { return static_cast<int32_t>(offsetof(BalloonScript_t4017429074, ___targetObj_8)); }
	inline Transform_t3600365921 * get_targetObj_8() const { return ___targetObj_8; }
	inline Transform_t3600365921 ** get_address_of_targetObj_8() { return &___targetObj_8; }
	inline void set_targetObj_8(Transform_t3600365921 * value)
	{
		___targetObj_8 = value;
		Il2CppCodeGenWriteBarrier((&___targetObj_8), value);
	}

	inline static int32_t get_offset_of_startTime_9() { return static_cast<int32_t>(offsetof(BalloonScript_t4017429074, ___startTime_9)); }
	inline float get_startTime_9() const { return ___startTime_9; }
	inline float* get_address_of_startTime_9() { return &___startTime_9; }
	inline void set_startTime_9(float value)
	{
		___startTime_9 = value;
	}

	inline static int32_t get_offset_of_duration_10() { return static_cast<int32_t>(offsetof(BalloonScript_t4017429074, ___duration_10)); }
	inline float get_duration_10() const { return ___duration_10; }
	inline float* get_address_of_duration_10() { return &___duration_10; }
	inline void set_duration_10(float value)
	{
		___duration_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLOONSCRIPT_T4017429074_H
#ifndef DESTROYFORPOINTSATTRIBUTE_T2602039747_H
#define DESTROYFORPOINTSATTRIBUTE_T2602039747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyForPointsAttribute
struct  DestroyForPointsAttribute_t2602039747  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 DestroyForPointsAttribute::pointsWorth
	int32_t ___pointsWorth_2;
	// UIScript DestroyForPointsAttribute::userInterface
	UIScript_t1195624422 * ___userInterface_3;

public:
	inline static int32_t get_offset_of_pointsWorth_2() { return static_cast<int32_t>(offsetof(DestroyForPointsAttribute_t2602039747, ___pointsWorth_2)); }
	inline int32_t get_pointsWorth_2() const { return ___pointsWorth_2; }
	inline int32_t* get_address_of_pointsWorth_2() { return &___pointsWorth_2; }
	inline void set_pointsWorth_2(int32_t value)
	{
		___pointsWorth_2 = value;
	}

	inline static int32_t get_offset_of_userInterface_3() { return static_cast<int32_t>(offsetof(DestroyForPointsAttribute_t2602039747, ___userInterface_3)); }
	inline UIScript_t1195624422 * get_userInterface_3() const { return ___userInterface_3; }
	inline UIScript_t1195624422 ** get_address_of_userInterface_3() { return &___userInterface_3; }
	inline void set_userInterface_3(UIScript_t1195624422 * value)
	{
		___userInterface_3 = value;
		Il2CppCodeGenWriteBarrier((&___userInterface_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYFORPOINTSATTRIBUTE_T2602039747_H
#ifndef DIALOGUESYSTEM_T292220827_H
#define DIALOGUESYSTEM_T292220827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DialogueSystem
struct  DialogueSystem_t292220827  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DialogueSystem::balloonPrefab
	GameObject_t1113636619 * ___balloonPrefab_2;

public:
	inline static int32_t get_offset_of_balloonPrefab_2() { return static_cast<int32_t>(offsetof(DialogueSystem_t292220827, ___balloonPrefab_2)); }
	inline GameObject_t1113636619 * get_balloonPrefab_2() const { return ___balloonPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_balloonPrefab_2() { return &___balloonPrefab_2; }
	inline void set_balloonPrefab_2(GameObject_t1113636619 * value)
	{
		___balloonPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___balloonPrefab_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGUESYSTEM_T292220827_H
#ifndef CONDITIONBASE_T3399778919_H
#define CONDITIONBASE_T3399778919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConditionBase
struct  ConditionBase_t3399778919  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<Action> ConditionBase::actions
	List_1_t547830642 * ___actions_2;
	// System.Boolean ConditionBase::useCustomActions
	bool ___useCustomActions_3;
	// UnityEngine.Events.UnityEvent ConditionBase::customActions
	UnityEvent_t2581268647 * ___customActions_4;
	// System.Boolean ConditionBase::happenOnlyOnce
	bool ___happenOnlyOnce_5;
	// System.Boolean ConditionBase::alreadyHappened
	bool ___alreadyHappened_6;
	// System.Boolean ConditionBase::filterByTag
	bool ___filterByTag_7;
	// System.String ConditionBase::filterTag
	String_t* ___filterTag_8;

public:
	inline static int32_t get_offset_of_actions_2() { return static_cast<int32_t>(offsetof(ConditionBase_t3399778919, ___actions_2)); }
	inline List_1_t547830642 * get_actions_2() const { return ___actions_2; }
	inline List_1_t547830642 ** get_address_of_actions_2() { return &___actions_2; }
	inline void set_actions_2(List_1_t547830642 * value)
	{
		___actions_2 = value;
		Il2CppCodeGenWriteBarrier((&___actions_2), value);
	}

	inline static int32_t get_offset_of_useCustomActions_3() { return static_cast<int32_t>(offsetof(ConditionBase_t3399778919, ___useCustomActions_3)); }
	inline bool get_useCustomActions_3() const { return ___useCustomActions_3; }
	inline bool* get_address_of_useCustomActions_3() { return &___useCustomActions_3; }
	inline void set_useCustomActions_3(bool value)
	{
		___useCustomActions_3 = value;
	}

	inline static int32_t get_offset_of_customActions_4() { return static_cast<int32_t>(offsetof(ConditionBase_t3399778919, ___customActions_4)); }
	inline UnityEvent_t2581268647 * get_customActions_4() const { return ___customActions_4; }
	inline UnityEvent_t2581268647 ** get_address_of_customActions_4() { return &___customActions_4; }
	inline void set_customActions_4(UnityEvent_t2581268647 * value)
	{
		___customActions_4 = value;
		Il2CppCodeGenWriteBarrier((&___customActions_4), value);
	}

	inline static int32_t get_offset_of_happenOnlyOnce_5() { return static_cast<int32_t>(offsetof(ConditionBase_t3399778919, ___happenOnlyOnce_5)); }
	inline bool get_happenOnlyOnce_5() const { return ___happenOnlyOnce_5; }
	inline bool* get_address_of_happenOnlyOnce_5() { return &___happenOnlyOnce_5; }
	inline void set_happenOnlyOnce_5(bool value)
	{
		___happenOnlyOnce_5 = value;
	}

	inline static int32_t get_offset_of_alreadyHappened_6() { return static_cast<int32_t>(offsetof(ConditionBase_t3399778919, ___alreadyHappened_6)); }
	inline bool get_alreadyHappened_6() const { return ___alreadyHappened_6; }
	inline bool* get_address_of_alreadyHappened_6() { return &___alreadyHappened_6; }
	inline void set_alreadyHappened_6(bool value)
	{
		___alreadyHappened_6 = value;
	}

	inline static int32_t get_offset_of_filterByTag_7() { return static_cast<int32_t>(offsetof(ConditionBase_t3399778919, ___filterByTag_7)); }
	inline bool get_filterByTag_7() const { return ___filterByTag_7; }
	inline bool* get_address_of_filterByTag_7() { return &___filterByTag_7; }
	inline void set_filterByTag_7(bool value)
	{
		___filterByTag_7 = value;
	}

	inline static int32_t get_offset_of_filterTag_8() { return static_cast<int32_t>(offsetof(ConditionBase_t3399778919, ___filterTag_8)); }
	inline String_t* get_filterTag_8() const { return ___filterTag_8; }
	inline String_t** get_address_of_filterTag_8() { return &___filterTag_8; }
	inline void set_filterTag_8(String_t* value)
	{
		___filterTag_8 = value;
		Il2CppCodeGenWriteBarrier((&___filterTag_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONBASE_T3399778919_H
#ifndef UISCRIPT_T1195624422_H
#define UISCRIPT_T1195624422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIScript
struct  UIScript_t1195624422  : public MonoBehaviour_t3962482529
{
public:
	// UIScript/Players UIScript::numberOfPlayers
	int32_t ___numberOfPlayers_2;
	// UIScript/GameType UIScript::gameType
	int32_t ___gameType_3;
	// System.Int32 UIScript::scoreToWin
	int32_t ___scoreToWin_4;
	// UnityEngine.UI.Text[] UIScript::numberLabels
	TextU5BU5D_t422084607* ___numberLabels_5;
	// UnityEngine.UI.Text UIScript::rightLabel
	Text_t1901882714 * ___rightLabel_6;
	// UnityEngine.UI.Text UIScript::leftLabel
	Text_t1901882714 * ___leftLabel_7;
	// UnityEngine.UI.Text UIScript::winLabel
	Text_t1901882714 * ___winLabel_8;
	// UnityEngine.GameObject UIScript::statsPanel
	GameObject_t1113636619 * ___statsPanel_9;
	// UnityEngine.GameObject UIScript::gameOverPanel
	GameObject_t1113636619 * ___gameOverPanel_10;
	// UnityEngine.GameObject UIScript::winPanel
	GameObject_t1113636619 * ___winPanel_11;
	// UnityEngine.Transform UIScript::inventory
	Transform_t3600365921 * ___inventory_12;
	// UnityEngine.GameObject UIScript::resourceItemPrefab
	GameObject_t1113636619 * ___resourceItemPrefab_13;
	// System.Int32[] UIScript::scores
	Int32U5BU5D_t385246372* ___scores_14;
	// System.Int32[] UIScript::playersHealth
	Int32U5BU5D_t385246372* ___playersHealth_15;
	// System.Collections.Generic.Dictionary`2<System.Int32,ResourceStruct> UIScript::resourcesDict
	Dictionary_2_t4045795053 * ___resourcesDict_16;
	// System.Boolean UIScript::gameOver
	bool ___gameOver_17;

public:
	inline static int32_t get_offset_of_numberOfPlayers_2() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___numberOfPlayers_2)); }
	inline int32_t get_numberOfPlayers_2() const { return ___numberOfPlayers_2; }
	inline int32_t* get_address_of_numberOfPlayers_2() { return &___numberOfPlayers_2; }
	inline void set_numberOfPlayers_2(int32_t value)
	{
		___numberOfPlayers_2 = value;
	}

	inline static int32_t get_offset_of_gameType_3() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___gameType_3)); }
	inline int32_t get_gameType_3() const { return ___gameType_3; }
	inline int32_t* get_address_of_gameType_3() { return &___gameType_3; }
	inline void set_gameType_3(int32_t value)
	{
		___gameType_3 = value;
	}

	inline static int32_t get_offset_of_scoreToWin_4() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___scoreToWin_4)); }
	inline int32_t get_scoreToWin_4() const { return ___scoreToWin_4; }
	inline int32_t* get_address_of_scoreToWin_4() { return &___scoreToWin_4; }
	inline void set_scoreToWin_4(int32_t value)
	{
		___scoreToWin_4 = value;
	}

	inline static int32_t get_offset_of_numberLabels_5() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___numberLabels_5)); }
	inline TextU5BU5D_t422084607* get_numberLabels_5() const { return ___numberLabels_5; }
	inline TextU5BU5D_t422084607** get_address_of_numberLabels_5() { return &___numberLabels_5; }
	inline void set_numberLabels_5(TextU5BU5D_t422084607* value)
	{
		___numberLabels_5 = value;
		Il2CppCodeGenWriteBarrier((&___numberLabels_5), value);
	}

	inline static int32_t get_offset_of_rightLabel_6() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___rightLabel_6)); }
	inline Text_t1901882714 * get_rightLabel_6() const { return ___rightLabel_6; }
	inline Text_t1901882714 ** get_address_of_rightLabel_6() { return &___rightLabel_6; }
	inline void set_rightLabel_6(Text_t1901882714 * value)
	{
		___rightLabel_6 = value;
		Il2CppCodeGenWriteBarrier((&___rightLabel_6), value);
	}

	inline static int32_t get_offset_of_leftLabel_7() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___leftLabel_7)); }
	inline Text_t1901882714 * get_leftLabel_7() const { return ___leftLabel_7; }
	inline Text_t1901882714 ** get_address_of_leftLabel_7() { return &___leftLabel_7; }
	inline void set_leftLabel_7(Text_t1901882714 * value)
	{
		___leftLabel_7 = value;
		Il2CppCodeGenWriteBarrier((&___leftLabel_7), value);
	}

	inline static int32_t get_offset_of_winLabel_8() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___winLabel_8)); }
	inline Text_t1901882714 * get_winLabel_8() const { return ___winLabel_8; }
	inline Text_t1901882714 ** get_address_of_winLabel_8() { return &___winLabel_8; }
	inline void set_winLabel_8(Text_t1901882714 * value)
	{
		___winLabel_8 = value;
		Il2CppCodeGenWriteBarrier((&___winLabel_8), value);
	}

	inline static int32_t get_offset_of_statsPanel_9() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___statsPanel_9)); }
	inline GameObject_t1113636619 * get_statsPanel_9() const { return ___statsPanel_9; }
	inline GameObject_t1113636619 ** get_address_of_statsPanel_9() { return &___statsPanel_9; }
	inline void set_statsPanel_9(GameObject_t1113636619 * value)
	{
		___statsPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___statsPanel_9), value);
	}

	inline static int32_t get_offset_of_gameOverPanel_10() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___gameOverPanel_10)); }
	inline GameObject_t1113636619 * get_gameOverPanel_10() const { return ___gameOverPanel_10; }
	inline GameObject_t1113636619 ** get_address_of_gameOverPanel_10() { return &___gameOverPanel_10; }
	inline void set_gameOverPanel_10(GameObject_t1113636619 * value)
	{
		___gameOverPanel_10 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverPanel_10), value);
	}

	inline static int32_t get_offset_of_winPanel_11() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___winPanel_11)); }
	inline GameObject_t1113636619 * get_winPanel_11() const { return ___winPanel_11; }
	inline GameObject_t1113636619 ** get_address_of_winPanel_11() { return &___winPanel_11; }
	inline void set_winPanel_11(GameObject_t1113636619 * value)
	{
		___winPanel_11 = value;
		Il2CppCodeGenWriteBarrier((&___winPanel_11), value);
	}

	inline static int32_t get_offset_of_inventory_12() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___inventory_12)); }
	inline Transform_t3600365921 * get_inventory_12() const { return ___inventory_12; }
	inline Transform_t3600365921 ** get_address_of_inventory_12() { return &___inventory_12; }
	inline void set_inventory_12(Transform_t3600365921 * value)
	{
		___inventory_12 = value;
		Il2CppCodeGenWriteBarrier((&___inventory_12), value);
	}

	inline static int32_t get_offset_of_resourceItemPrefab_13() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___resourceItemPrefab_13)); }
	inline GameObject_t1113636619 * get_resourceItemPrefab_13() const { return ___resourceItemPrefab_13; }
	inline GameObject_t1113636619 ** get_address_of_resourceItemPrefab_13() { return &___resourceItemPrefab_13; }
	inline void set_resourceItemPrefab_13(GameObject_t1113636619 * value)
	{
		___resourceItemPrefab_13 = value;
		Il2CppCodeGenWriteBarrier((&___resourceItemPrefab_13), value);
	}

	inline static int32_t get_offset_of_scores_14() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___scores_14)); }
	inline Int32U5BU5D_t385246372* get_scores_14() const { return ___scores_14; }
	inline Int32U5BU5D_t385246372** get_address_of_scores_14() { return &___scores_14; }
	inline void set_scores_14(Int32U5BU5D_t385246372* value)
	{
		___scores_14 = value;
		Il2CppCodeGenWriteBarrier((&___scores_14), value);
	}

	inline static int32_t get_offset_of_playersHealth_15() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___playersHealth_15)); }
	inline Int32U5BU5D_t385246372* get_playersHealth_15() const { return ___playersHealth_15; }
	inline Int32U5BU5D_t385246372** get_address_of_playersHealth_15() { return &___playersHealth_15; }
	inline void set_playersHealth_15(Int32U5BU5D_t385246372* value)
	{
		___playersHealth_15 = value;
		Il2CppCodeGenWriteBarrier((&___playersHealth_15), value);
	}

	inline static int32_t get_offset_of_resourcesDict_16() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___resourcesDict_16)); }
	inline Dictionary_2_t4045795053 * get_resourcesDict_16() const { return ___resourcesDict_16; }
	inline Dictionary_2_t4045795053 ** get_address_of_resourcesDict_16() { return &___resourcesDict_16; }
	inline void set_resourcesDict_16(Dictionary_2_t4045795053 * value)
	{
		___resourcesDict_16 = value;
		Il2CppCodeGenWriteBarrier((&___resourcesDict_16), value);
	}

	inline static int32_t get_offset_of_gameOver_17() { return static_cast<int32_t>(offsetof(UIScript_t1195624422, ___gameOver_17)); }
	inline bool get_gameOver_17() const { return ___gameOver_17; }
	inline bool* get_address_of_gameOver_17() { return &___gameOver_17; }
	inline void set_gameOver_17(bool value)
	{
		___gameOver_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCRIPT_T1195624422_H
#ifndef COLLECTABLEATTRIBUTE_T113388083_H
#define COLLECTABLEATTRIBUTE_T113388083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectableAttribute
struct  CollectableAttribute_t113388083  : public MonoBehaviour_t3962482529
{
public:
	// UIScript CollectableAttribute::userInterface
	UIScript_t1195624422 * ___userInterface_2;

public:
	inline static int32_t get_offset_of_userInterface_2() { return static_cast<int32_t>(offsetof(CollectableAttribute_t113388083, ___userInterface_2)); }
	inline UIScript_t1195624422 * get_userInterface_2() const { return ___userInterface_2; }
	inline UIScript_t1195624422 ** get_address_of_userInterface_2() { return &___userInterface_2; }
	inline void set_userInterface_2(UIScript_t1195624422 * value)
	{
		___userInterface_2 = value;
		Il2CppCodeGenWriteBarrier((&___userInterface_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTABLEATTRIBUTE_T113388083_H
#ifndef CAMERAFOLLOW_T129522575_H
#define CAMERAFOLLOW_T129522575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFollow
struct  CameraFollow_t129522575  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform CameraFollow::target
	Transform_t3600365921 * ___target_2;
	// System.Boolean CameraFollow::limitBounds
	bool ___limitBounds_3;
	// System.Single CameraFollow::left
	float ___left_4;
	// System.Single CameraFollow::right
	float ___right_5;
	// System.Single CameraFollow::bottom
	float ___bottom_6;
	// System.Single CameraFollow::top
	float ___top_7;
	// UnityEngine.Vector3 CameraFollow::lerpedPosition
	Vector3_t3722313464  ___lerpedPosition_8;
	// UnityEngine.Camera CameraFollow::_camera
	Camera_t4157153871 * ____camera_9;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_limitBounds_3() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___limitBounds_3)); }
	inline bool get_limitBounds_3() const { return ___limitBounds_3; }
	inline bool* get_address_of_limitBounds_3() { return &___limitBounds_3; }
	inline void set_limitBounds_3(bool value)
	{
		___limitBounds_3 = value;
	}

	inline static int32_t get_offset_of_left_4() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___left_4)); }
	inline float get_left_4() const { return ___left_4; }
	inline float* get_address_of_left_4() { return &___left_4; }
	inline void set_left_4(float value)
	{
		___left_4 = value;
	}

	inline static int32_t get_offset_of_right_5() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___right_5)); }
	inline float get_right_5() const { return ___right_5; }
	inline float* get_address_of_right_5() { return &___right_5; }
	inline void set_right_5(float value)
	{
		___right_5 = value;
	}

	inline static int32_t get_offset_of_bottom_6() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___bottom_6)); }
	inline float get_bottom_6() const { return ___bottom_6; }
	inline float* get_address_of_bottom_6() { return &___bottom_6; }
	inline void set_bottom_6(float value)
	{
		___bottom_6 = value;
	}

	inline static int32_t get_offset_of_top_7() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___top_7)); }
	inline float get_top_7() const { return ___top_7; }
	inline float* get_address_of_top_7() { return &___top_7; }
	inline void set_top_7(float value)
	{
		___top_7 = value;
	}

	inline static int32_t get_offset_of_lerpedPosition_8() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___lerpedPosition_8)); }
	inline Vector3_t3722313464  get_lerpedPosition_8() const { return ___lerpedPosition_8; }
	inline Vector3_t3722313464 * get_address_of_lerpedPosition_8() { return &___lerpedPosition_8; }
	inline void set_lerpedPosition_8(Vector3_t3722313464  value)
	{
		___lerpedPosition_8 = value;
	}

	inline static int32_t get_offset_of__camera_9() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ____camera_9)); }
	inline Camera_t4157153871 * get__camera_9() const { return ____camera_9; }
	inline Camera_t4157153871 ** get_address_of__camera_9() { return &____camera_9; }
	inline void set__camera_9(Camera_t4157153871 * value)
	{
		____camera_9 = value;
		Il2CppCodeGenWriteBarrier((&____camera_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFOLLOW_T129522575_H
#ifndef BULLETATTRIBUTE_T588811475_H
#define BULLETATTRIBUTE_T588811475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BulletAttribute
struct  BulletAttribute_t588811475  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 BulletAttribute::playerId
	int32_t ___playerId_2;

public:
	inline static int32_t get_offset_of_playerId_2() { return static_cast<int32_t>(offsetof(BulletAttribute_t588811475, ___playerId_2)); }
	inline int32_t get_playerId_2() const { return ___playerId_2; }
	inline int32_t* get_address_of_playerId_2() { return &___playerId_2; }
	inline void set_playerId_2(int32_t value)
	{
		___playerId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BULLETATTRIBUTE_T588811475_H
#ifndef HEALTHSYSTEMATTRIBUTE_T2647166000_H
#define HEALTHSYSTEMATTRIBUTE_T2647166000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HealthSystemAttribute
struct  HealthSystemAttribute_t2647166000  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 HealthSystemAttribute::health
	int32_t ___health_2;
	// UIScript HealthSystemAttribute::ui
	UIScript_t1195624422 * ___ui_3;
	// System.Int32 HealthSystemAttribute::maxHealth
	int32_t ___maxHealth_4;
	// System.Int32 HealthSystemAttribute::playerNumber
	int32_t ___playerNumber_5;

public:
	inline static int32_t get_offset_of_health_2() { return static_cast<int32_t>(offsetof(HealthSystemAttribute_t2647166000, ___health_2)); }
	inline int32_t get_health_2() const { return ___health_2; }
	inline int32_t* get_address_of_health_2() { return &___health_2; }
	inline void set_health_2(int32_t value)
	{
		___health_2 = value;
	}

	inline static int32_t get_offset_of_ui_3() { return static_cast<int32_t>(offsetof(HealthSystemAttribute_t2647166000, ___ui_3)); }
	inline UIScript_t1195624422 * get_ui_3() const { return ___ui_3; }
	inline UIScript_t1195624422 ** get_address_of_ui_3() { return &___ui_3; }
	inline void set_ui_3(UIScript_t1195624422 * value)
	{
		___ui_3 = value;
		Il2CppCodeGenWriteBarrier((&___ui_3), value);
	}

	inline static int32_t get_offset_of_maxHealth_4() { return static_cast<int32_t>(offsetof(HealthSystemAttribute_t2647166000, ___maxHealth_4)); }
	inline int32_t get_maxHealth_4() const { return ___maxHealth_4; }
	inline int32_t* get_address_of_maxHealth_4() { return &___maxHealth_4; }
	inline void set_maxHealth_4(int32_t value)
	{
		___maxHealth_4 = value;
	}

	inline static int32_t get_offset_of_playerNumber_5() { return static_cast<int32_t>(offsetof(HealthSystemAttribute_t2647166000, ___playerNumber_5)); }
	inline int32_t get_playerNumber_5() const { return ___playerNumber_5; }
	inline int32_t* get_address_of_playerNumber_5() { return &___playerNumber_5; }
	inline void set_playerNumber_5(int32_t value)
	{
		___playerNumber_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALTHSYSTEMATTRIBUTE_T2647166000_H
#ifndef MODIFYHEALTHATTRIBUTE_T3455596632_H
#define MODIFYHEALTHATTRIBUTE_T3455596632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModifyHealthAttribute
struct  ModifyHealthAttribute_t3455596632  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean ModifyHealthAttribute::destroyWhenActivated
	bool ___destroyWhenActivated_2;
	// System.Int32 ModifyHealthAttribute::healthChange
	int32_t ___healthChange_3;

public:
	inline static int32_t get_offset_of_destroyWhenActivated_2() { return static_cast<int32_t>(offsetof(ModifyHealthAttribute_t3455596632, ___destroyWhenActivated_2)); }
	inline bool get_destroyWhenActivated_2() const { return ___destroyWhenActivated_2; }
	inline bool* get_address_of_destroyWhenActivated_2() { return &___destroyWhenActivated_2; }
	inline void set_destroyWhenActivated_2(bool value)
	{
		___destroyWhenActivated_2 = value;
	}

	inline static int32_t get_offset_of_healthChange_3() { return static_cast<int32_t>(offsetof(ModifyHealthAttribute_t3455596632, ___healthChange_3)); }
	inline int32_t get_healthChange_3() const { return ___healthChange_3; }
	inline int32_t* get_address_of_healthChange_3() { return &___healthChange_3; }
	inline void set_healthChange_3(int32_t value)
	{
		___healthChange_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODIFYHEALTHATTRIBUTE_T3455596632_H
#ifndef OBJECTCREATORAREA_T9936620_H
#define OBJECTCREATORAREA_T9936620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectCreatorArea
struct  ObjectCreatorArea_t9936620  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ObjectCreatorArea::prefabToSpawn
	GameObject_t1113636619 * ___prefabToSpawn_2;
	// System.Single ObjectCreatorArea::spawnInterval
	float ___spawnInterval_3;
	// UnityEngine.BoxCollider2D ObjectCreatorArea::boxCollider2D
	BoxCollider2D_t3581341831 * ___boxCollider2D_4;

public:
	inline static int32_t get_offset_of_prefabToSpawn_2() { return static_cast<int32_t>(offsetof(ObjectCreatorArea_t9936620, ___prefabToSpawn_2)); }
	inline GameObject_t1113636619 * get_prefabToSpawn_2() const { return ___prefabToSpawn_2; }
	inline GameObject_t1113636619 ** get_address_of_prefabToSpawn_2() { return &___prefabToSpawn_2; }
	inline void set_prefabToSpawn_2(GameObject_t1113636619 * value)
	{
		___prefabToSpawn_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefabToSpawn_2), value);
	}

	inline static int32_t get_offset_of_spawnInterval_3() { return static_cast<int32_t>(offsetof(ObjectCreatorArea_t9936620, ___spawnInterval_3)); }
	inline float get_spawnInterval_3() const { return ___spawnInterval_3; }
	inline float* get_address_of_spawnInterval_3() { return &___spawnInterval_3; }
	inline void set_spawnInterval_3(float value)
	{
		___spawnInterval_3 = value;
	}

	inline static int32_t get_offset_of_boxCollider2D_4() { return static_cast<int32_t>(offsetof(ObjectCreatorArea_t9936620, ___boxCollider2D_4)); }
	inline BoxCollider2D_t3581341831 * get_boxCollider2D_4() const { return ___boxCollider2D_4; }
	inline BoxCollider2D_t3581341831 ** get_address_of_boxCollider2D_4() { return &___boxCollider2D_4; }
	inline void set_boxCollider2D_4(BoxCollider2D_t3581341831 * value)
	{
		___boxCollider2D_4 = value;
		Il2CppCodeGenWriteBarrier((&___boxCollider2D_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCREATORAREA_T9936620_H
#ifndef BOXCOLLIDER2D_T3581341831_H
#define BOXCOLLIDER2D_T3581341831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.BoxCollider2D
struct  BoxCollider2D_t3581341831  : public Collider2D_t2806799626
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXCOLLIDER2D_T3581341831_H
#ifndef CONDITIONCOLLISION_T1635305638_H
#define CONDITIONCOLLISION_T1635305638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConditionCollision
struct  ConditionCollision_t1635305638  : public ConditionBase_t3399778919
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONCOLLISION_T1635305638_H
#ifndef WANDER_T651605530_H
#define WANDER_T651605530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wander
struct  Wander_t651605530  : public Physics2DObject_t3026219368
{
public:
	// System.Single Wander::speed
	float ___speed_3;
	// System.Single Wander::directionChangeInterval
	float ___directionChangeInterval_4;
	// System.Boolean Wander::keepNearStartingPoint
	bool ___keepNearStartingPoint_5;
	// System.Boolean Wander::orientToDirection
	bool ___orientToDirection_6;
	// Enums/Directions Wander::lookAxis
	int32_t ___lookAxis_7;
	// UnityEngine.Vector2 Wander::direction
	Vector2_t2156229523  ___direction_8;
	// UnityEngine.Vector3 Wander::startingPoint
	Vector3_t3722313464  ___startingPoint_9;

public:
	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(Wander_t651605530, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_directionChangeInterval_4() { return static_cast<int32_t>(offsetof(Wander_t651605530, ___directionChangeInterval_4)); }
	inline float get_directionChangeInterval_4() const { return ___directionChangeInterval_4; }
	inline float* get_address_of_directionChangeInterval_4() { return &___directionChangeInterval_4; }
	inline void set_directionChangeInterval_4(float value)
	{
		___directionChangeInterval_4 = value;
	}

	inline static int32_t get_offset_of_keepNearStartingPoint_5() { return static_cast<int32_t>(offsetof(Wander_t651605530, ___keepNearStartingPoint_5)); }
	inline bool get_keepNearStartingPoint_5() const { return ___keepNearStartingPoint_5; }
	inline bool* get_address_of_keepNearStartingPoint_5() { return &___keepNearStartingPoint_5; }
	inline void set_keepNearStartingPoint_5(bool value)
	{
		___keepNearStartingPoint_5 = value;
	}

	inline static int32_t get_offset_of_orientToDirection_6() { return static_cast<int32_t>(offsetof(Wander_t651605530, ___orientToDirection_6)); }
	inline bool get_orientToDirection_6() const { return ___orientToDirection_6; }
	inline bool* get_address_of_orientToDirection_6() { return &___orientToDirection_6; }
	inline void set_orientToDirection_6(bool value)
	{
		___orientToDirection_6 = value;
	}

	inline static int32_t get_offset_of_lookAxis_7() { return static_cast<int32_t>(offsetof(Wander_t651605530, ___lookAxis_7)); }
	inline int32_t get_lookAxis_7() const { return ___lookAxis_7; }
	inline int32_t* get_address_of_lookAxis_7() { return &___lookAxis_7; }
	inline void set_lookAxis_7(int32_t value)
	{
		___lookAxis_7 = value;
	}

	inline static int32_t get_offset_of_direction_8() { return static_cast<int32_t>(offsetof(Wander_t651605530, ___direction_8)); }
	inline Vector2_t2156229523  get_direction_8() const { return ___direction_8; }
	inline Vector2_t2156229523 * get_address_of_direction_8() { return &___direction_8; }
	inline void set_direction_8(Vector2_t2156229523  value)
	{
		___direction_8 = value;
	}

	inline static int32_t get_offset_of_startingPoint_9() { return static_cast<int32_t>(offsetof(Wander_t651605530, ___startingPoint_9)); }
	inline Vector3_t3722313464  get_startingPoint_9() const { return ___startingPoint_9; }
	inline Vector3_t3722313464 * get_address_of_startingPoint_9() { return &___startingPoint_9; }
	inline void set_startingPoint_9(Vector3_t3722313464  value)
	{
		___startingPoint_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WANDER_T651605530_H
#ifndef MOVE_T3440333737_H
#define MOVE_T3440333737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Move
struct  Move_t3440333737  : public Physics2DObject_t3026219368
{
public:
	// Enums/KeyGroups Move::typeOfControl
	int32_t ___typeOfControl_3;
	// System.Single Move::speed
	float ___speed_4;
	// Enums/MovementType Move::movementType
	int32_t ___movementType_5;
	// System.Boolean Move::orientToDirection
	bool ___orientToDirection_6;
	// Enums/Directions Move::lookAxis
	int32_t ___lookAxis_7;
	// UnityEngine.Vector2 Move::movement
	Vector2_t2156229523  ___movement_8;
	// UnityEngine.Vector2 Move::cachedDirection
	Vector2_t2156229523  ___cachedDirection_9;
	// System.Single Move::moveHorizontal
	float ___moveHorizontal_10;
	// System.Single Move::moveVertical
	float ___moveVertical_11;
	// System.Boolean Move::holdingBall
	bool ___holdingBall_12;

public:
	inline static int32_t get_offset_of_typeOfControl_3() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___typeOfControl_3)); }
	inline int32_t get_typeOfControl_3() const { return ___typeOfControl_3; }
	inline int32_t* get_address_of_typeOfControl_3() { return &___typeOfControl_3; }
	inline void set_typeOfControl_3(int32_t value)
	{
		___typeOfControl_3 = value;
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_movementType_5() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___movementType_5)); }
	inline int32_t get_movementType_5() const { return ___movementType_5; }
	inline int32_t* get_address_of_movementType_5() { return &___movementType_5; }
	inline void set_movementType_5(int32_t value)
	{
		___movementType_5 = value;
	}

	inline static int32_t get_offset_of_orientToDirection_6() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___orientToDirection_6)); }
	inline bool get_orientToDirection_6() const { return ___orientToDirection_6; }
	inline bool* get_address_of_orientToDirection_6() { return &___orientToDirection_6; }
	inline void set_orientToDirection_6(bool value)
	{
		___orientToDirection_6 = value;
	}

	inline static int32_t get_offset_of_lookAxis_7() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___lookAxis_7)); }
	inline int32_t get_lookAxis_7() const { return ___lookAxis_7; }
	inline int32_t* get_address_of_lookAxis_7() { return &___lookAxis_7; }
	inline void set_lookAxis_7(int32_t value)
	{
		___lookAxis_7 = value;
	}

	inline static int32_t get_offset_of_movement_8() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___movement_8)); }
	inline Vector2_t2156229523  get_movement_8() const { return ___movement_8; }
	inline Vector2_t2156229523 * get_address_of_movement_8() { return &___movement_8; }
	inline void set_movement_8(Vector2_t2156229523  value)
	{
		___movement_8 = value;
	}

	inline static int32_t get_offset_of_cachedDirection_9() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___cachedDirection_9)); }
	inline Vector2_t2156229523  get_cachedDirection_9() const { return ___cachedDirection_9; }
	inline Vector2_t2156229523 * get_address_of_cachedDirection_9() { return &___cachedDirection_9; }
	inline void set_cachedDirection_9(Vector2_t2156229523  value)
	{
		___cachedDirection_9 = value;
	}

	inline static int32_t get_offset_of_moveHorizontal_10() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___moveHorizontal_10)); }
	inline float get_moveHorizontal_10() const { return ___moveHorizontal_10; }
	inline float* get_address_of_moveHorizontal_10() { return &___moveHorizontal_10; }
	inline void set_moveHorizontal_10(float value)
	{
		___moveHorizontal_10 = value;
	}

	inline static int32_t get_offset_of_moveVertical_11() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___moveVertical_11)); }
	inline float get_moveVertical_11() const { return ___moveVertical_11; }
	inline float* get_address_of_moveVertical_11() { return &___moveVertical_11; }
	inline void set_moveVertical_11(float value)
	{
		___moveVertical_11 = value;
	}

	inline static int32_t get_offset_of_holdingBall_12() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___holdingBall_12)); }
	inline bool get_holdingBall_12() const { return ___holdingBall_12; }
	inline bool* get_address_of_holdingBall_12() { return &___holdingBall_12; }
	inline void set_holdingBall_12(bool value)
	{
		___holdingBall_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVE_T3440333737_H
#ifndef CONDITIONAREA_T3498028374_H
#define CONDITIONAREA_T3498028374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConditionArea
struct  ConditionArea_t3498028374  : public ConditionBase_t3399778919
{
public:
	// System.Single ConditionArea::frequency
	float ___frequency_9;
	// ConditionArea/ColliderEventTypes ConditionArea::eventType
	int32_t ___eventType_10;
	// System.Single ConditionArea::lastTimeTriggerStayCalled
	float ___lastTimeTriggerStayCalled_11;

public:
	inline static int32_t get_offset_of_frequency_9() { return static_cast<int32_t>(offsetof(ConditionArea_t3498028374, ___frequency_9)); }
	inline float get_frequency_9() const { return ___frequency_9; }
	inline float* get_address_of_frequency_9() { return &___frequency_9; }
	inline void set_frequency_9(float value)
	{
		___frequency_9 = value;
	}

	inline static int32_t get_offset_of_eventType_10() { return static_cast<int32_t>(offsetof(ConditionArea_t3498028374, ___eventType_10)); }
	inline int32_t get_eventType_10() const { return ___eventType_10; }
	inline int32_t* get_address_of_eventType_10() { return &___eventType_10; }
	inline void set_eventType_10(int32_t value)
	{
		___eventType_10 = value;
	}

	inline static int32_t get_offset_of_lastTimeTriggerStayCalled_11() { return static_cast<int32_t>(offsetof(ConditionArea_t3498028374, ___lastTimeTriggerStayCalled_11)); }
	inline float get_lastTimeTriggerStayCalled_11() const { return ___lastTimeTriggerStayCalled_11; }
	inline float* get_address_of_lastTimeTriggerStayCalled_11() { return &___lastTimeTriggerStayCalled_11; }
	inline void set_lastTimeTriggerStayCalled_11(float value)
	{
		___lastTimeTriggerStayCalled_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONAREA_T3498028374_H
#ifndef PATROL_T2573740331_H
#define PATROL_T2573740331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Patrol
struct  Patrol_t2573740331  : public Physics2DObject_t3026219368
{
public:
	// System.Single Patrol::speed
	float ___speed_3;
	// System.Single Patrol::directionChangeInterval
	float ___directionChangeInterval_4;
	// System.Boolean Patrol::orientToDirection
	bool ___orientToDirection_5;
	// Enums/Directions Patrol::lookAxis
	int32_t ___lookAxis_6;
	// UnityEngine.Vector2[] Patrol::waypoints
	Vector2U5BU5D_t1457185986* ___waypoints_7;
	// UnityEngine.Vector2[] Patrol::newWaypoints
	Vector2U5BU5D_t1457185986* ___newWaypoints_8;
	// System.Int32 Patrol::currentTargetIndex
	int32_t ___currentTargetIndex_9;

public:
	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(Patrol_t2573740331, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_directionChangeInterval_4() { return static_cast<int32_t>(offsetof(Patrol_t2573740331, ___directionChangeInterval_4)); }
	inline float get_directionChangeInterval_4() const { return ___directionChangeInterval_4; }
	inline float* get_address_of_directionChangeInterval_4() { return &___directionChangeInterval_4; }
	inline void set_directionChangeInterval_4(float value)
	{
		___directionChangeInterval_4 = value;
	}

	inline static int32_t get_offset_of_orientToDirection_5() { return static_cast<int32_t>(offsetof(Patrol_t2573740331, ___orientToDirection_5)); }
	inline bool get_orientToDirection_5() const { return ___orientToDirection_5; }
	inline bool* get_address_of_orientToDirection_5() { return &___orientToDirection_5; }
	inline void set_orientToDirection_5(bool value)
	{
		___orientToDirection_5 = value;
	}

	inline static int32_t get_offset_of_lookAxis_6() { return static_cast<int32_t>(offsetof(Patrol_t2573740331, ___lookAxis_6)); }
	inline int32_t get_lookAxis_6() const { return ___lookAxis_6; }
	inline int32_t* get_address_of_lookAxis_6() { return &___lookAxis_6; }
	inline void set_lookAxis_6(int32_t value)
	{
		___lookAxis_6 = value;
	}

	inline static int32_t get_offset_of_waypoints_7() { return static_cast<int32_t>(offsetof(Patrol_t2573740331, ___waypoints_7)); }
	inline Vector2U5BU5D_t1457185986* get_waypoints_7() const { return ___waypoints_7; }
	inline Vector2U5BU5D_t1457185986** get_address_of_waypoints_7() { return &___waypoints_7; }
	inline void set_waypoints_7(Vector2U5BU5D_t1457185986* value)
	{
		___waypoints_7 = value;
		Il2CppCodeGenWriteBarrier((&___waypoints_7), value);
	}

	inline static int32_t get_offset_of_newWaypoints_8() { return static_cast<int32_t>(offsetof(Patrol_t2573740331, ___newWaypoints_8)); }
	inline Vector2U5BU5D_t1457185986* get_newWaypoints_8() const { return ___newWaypoints_8; }
	inline Vector2U5BU5D_t1457185986** get_address_of_newWaypoints_8() { return &___newWaypoints_8; }
	inline void set_newWaypoints_8(Vector2U5BU5D_t1457185986* value)
	{
		___newWaypoints_8 = value;
		Il2CppCodeGenWriteBarrier((&___newWaypoints_8), value);
	}

	inline static int32_t get_offset_of_currentTargetIndex_9() { return static_cast<int32_t>(offsetof(Patrol_t2573740331, ___currentTargetIndex_9)); }
	inline int32_t get_currentTargetIndex_9() const { return ___currentTargetIndex_9; }
	inline int32_t* get_address_of_currentTargetIndex_9() { return &___currentTargetIndex_9; }
	inline void set_currentTargetIndex_9(int32_t value)
	{
		___currentTargetIndex_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATROL_T2573740331_H
#ifndef ONOFFACTION_T1233588362_H
#define ONOFFACTION_T1233588362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnOffAction
struct  OnOffAction_t1233588362  : public Action_t3370723196
{
public:
	// UnityEngine.GameObject OnOffAction::objectToAffect
	GameObject_t1113636619 * ___objectToAffect_2;
	// System.Boolean OnOffAction::justMakeInvisible
	bool ___justMakeInvisible_3;

public:
	inline static int32_t get_offset_of_objectToAffect_2() { return static_cast<int32_t>(offsetof(OnOffAction_t1233588362, ___objectToAffect_2)); }
	inline GameObject_t1113636619 * get_objectToAffect_2() const { return ___objectToAffect_2; }
	inline GameObject_t1113636619 ** get_address_of_objectToAffect_2() { return &___objectToAffect_2; }
	inline void set_objectToAffect_2(GameObject_t1113636619 * value)
	{
		___objectToAffect_2 = value;
		Il2CppCodeGenWriteBarrier((&___objectToAffect_2), value);
	}

	inline static int32_t get_offset_of_justMakeInvisible_3() { return static_cast<int32_t>(offsetof(OnOffAction_t1233588362, ___justMakeInvisible_3)); }
	inline bool get_justMakeInvisible_3() const { return ___justMakeInvisible_3; }
	inline bool* get_address_of_justMakeInvisible_3() { return &___justMakeInvisible_3; }
	inline void set_justMakeInvisible_3(bool value)
	{
		___justMakeInvisible_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONOFFACTION_T1233588362_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_8() const { return ___m_CanvasRenderer_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_8() { return &___m_CanvasRenderer_8; }
	inline void set_m_CanvasRenderer_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef AUTOROTATE_T913508323_H
#define AUTOROTATE_T913508323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AutoRotate
struct  AutoRotate_t913508323  : public Physics2DObject_t3026219368
{
public:
	// System.Single AutoRotate::rotationSpeed
	float ___rotationSpeed_3;
	// System.Single AutoRotate::currentRotation
	float ___currentRotation_4;

public:
	inline static int32_t get_offset_of_rotationSpeed_3() { return static_cast<int32_t>(offsetof(AutoRotate_t913508323, ___rotationSpeed_3)); }
	inline float get_rotationSpeed_3() const { return ___rotationSpeed_3; }
	inline float* get_address_of_rotationSpeed_3() { return &___rotationSpeed_3; }
	inline void set_rotationSpeed_3(float value)
	{
		___rotationSpeed_3 = value;
	}

	inline static int32_t get_offset_of_currentRotation_4() { return static_cast<int32_t>(offsetof(AutoRotate_t913508323, ___currentRotation_4)); }
	inline float get_currentRotation_4() const { return ___currentRotation_4; }
	inline float* get_address_of_currentRotation_4() { return &___currentRotation_4; }
	inline void set_currentRotation_4(float value)
	{
		___currentRotation_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOROTATE_T913508323_H
#ifndef CONDITIONKEYPRESS_T1643004734_H
#define CONDITIONKEYPRESS_T1643004734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConditionKeyPress
struct  ConditionKeyPress_t1643004734  : public ConditionBase_t3399778919
{
public:
	// UnityEngine.KeyCode ConditionKeyPress::keyToPress
	int32_t ___keyToPress_9;
	// ConditionKeyPress/KeyEventTypes ConditionKeyPress::eventType
	int32_t ___eventType_10;
	// System.Single ConditionKeyPress::frequency
	float ___frequency_11;
	// System.Single ConditionKeyPress::timeLastEventFired
	float ___timeLastEventFired_12;

public:
	inline static int32_t get_offset_of_keyToPress_9() { return static_cast<int32_t>(offsetof(ConditionKeyPress_t1643004734, ___keyToPress_9)); }
	inline int32_t get_keyToPress_9() const { return ___keyToPress_9; }
	inline int32_t* get_address_of_keyToPress_9() { return &___keyToPress_9; }
	inline void set_keyToPress_9(int32_t value)
	{
		___keyToPress_9 = value;
	}

	inline static int32_t get_offset_of_eventType_10() { return static_cast<int32_t>(offsetof(ConditionKeyPress_t1643004734, ___eventType_10)); }
	inline int32_t get_eventType_10() const { return ___eventType_10; }
	inline int32_t* get_address_of_eventType_10() { return &___eventType_10; }
	inline void set_eventType_10(int32_t value)
	{
		___eventType_10 = value;
	}

	inline static int32_t get_offset_of_frequency_11() { return static_cast<int32_t>(offsetof(ConditionKeyPress_t1643004734, ___frequency_11)); }
	inline float get_frequency_11() const { return ___frequency_11; }
	inline float* get_address_of_frequency_11() { return &___frequency_11; }
	inline void set_frequency_11(float value)
	{
		___frequency_11 = value;
	}

	inline static int32_t get_offset_of_timeLastEventFired_12() { return static_cast<int32_t>(offsetof(ConditionKeyPress_t1643004734, ___timeLastEventFired_12)); }
	inline float get_timeLastEventFired_12() const { return ___timeLastEventFired_12; }
	inline float* get_address_of_timeLastEventFired_12() { return &___timeLastEventFired_12; }
	inline void set_timeLastEventFired_12(float value)
	{
		___timeLastEventFired_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONKEYPRESS_T1643004734_H
#ifndef JUMP_T2935391209_H
#define JUMP_T2935391209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jump
struct  Jump_t2935391209  : public Physics2DObject_t3026219368
{
public:
	// UnityEngine.KeyCode Jump::key
	int32_t ___key_3;
	// System.Single Jump::jumpStrength
	float ___jumpStrength_4;
	// System.String Jump::groundTag
	String_t* ___groundTag_5;
	// System.Boolean Jump::checkGround
	bool ___checkGround_6;
	// System.Boolean Jump::canJump
	bool ___canJump_7;

public:
	inline static int32_t get_offset_of_key_3() { return static_cast<int32_t>(offsetof(Jump_t2935391209, ___key_3)); }
	inline int32_t get_key_3() const { return ___key_3; }
	inline int32_t* get_address_of_key_3() { return &___key_3; }
	inline void set_key_3(int32_t value)
	{
		___key_3 = value;
	}

	inline static int32_t get_offset_of_jumpStrength_4() { return static_cast<int32_t>(offsetof(Jump_t2935391209, ___jumpStrength_4)); }
	inline float get_jumpStrength_4() const { return ___jumpStrength_4; }
	inline float* get_address_of_jumpStrength_4() { return &___jumpStrength_4; }
	inline void set_jumpStrength_4(float value)
	{
		___jumpStrength_4 = value;
	}

	inline static int32_t get_offset_of_groundTag_5() { return static_cast<int32_t>(offsetof(Jump_t2935391209, ___groundTag_5)); }
	inline String_t* get_groundTag_5() const { return ___groundTag_5; }
	inline String_t** get_address_of_groundTag_5() { return &___groundTag_5; }
	inline void set_groundTag_5(String_t* value)
	{
		___groundTag_5 = value;
		Il2CppCodeGenWriteBarrier((&___groundTag_5), value);
	}

	inline static int32_t get_offset_of_checkGround_6() { return static_cast<int32_t>(offsetof(Jump_t2935391209, ___checkGround_6)); }
	inline bool get_checkGround_6() const { return ___checkGround_6; }
	inline bool* get_address_of_checkGround_6() { return &___checkGround_6; }
	inline void set_checkGround_6(bool value)
	{
		___checkGround_6 = value;
	}

	inline static int32_t get_offset_of_canJump_7() { return static_cast<int32_t>(offsetof(Jump_t2935391209, ___canJump_7)); }
	inline bool get_canJump_7() const { return ___canJump_7; }
	inline bool* get_address_of_canJump_7() { return &___canJump_7; }
	inline void set_canJump_7(bool value)
	{
		___canJump_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JUMP_T2935391209_H
#ifndef FOLLOWTARGET_T1456261924_H
#define FOLLOWTARGET_T1456261924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FollowTarget
struct  FollowTarget_t1456261924  : public Physics2DObject_t3026219368
{
public:
	// UnityEngine.Transform FollowTarget::target
	Transform_t3600365921 * ___target_3;
	// System.Single FollowTarget::speed
	float ___speed_4;
	// System.Boolean FollowTarget::lookAtTarget
	bool ___lookAtTarget_5;
	// Enums/Directions FollowTarget::useSide
	int32_t ___useSide_6;

public:
	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(FollowTarget_t1456261924, ___target_3)); }
	inline Transform_t3600365921 * get_target_3() const { return ___target_3; }
	inline Transform_t3600365921 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Transform_t3600365921 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(FollowTarget_t1456261924, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_lookAtTarget_5() { return static_cast<int32_t>(offsetof(FollowTarget_t1456261924, ___lookAtTarget_5)); }
	inline bool get_lookAtTarget_5() const { return ___lookAtTarget_5; }
	inline bool* get_address_of_lookAtTarget_5() { return &___lookAtTarget_5; }
	inline void set_lookAtTarget_5(bool value)
	{
		___lookAtTarget_5 = value;
	}

	inline static int32_t get_offset_of_useSide_6() { return static_cast<int32_t>(offsetof(FollowTarget_t1456261924, ___useSide_6)); }
	inline int32_t get_useSide_6() const { return ___useSide_6; }
	inline int32_t* get_address_of_useSide_6() { return &___useSide_6; }
	inline void set_useSide_6(int32_t value)
	{
		___useSide_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTARGET_T1456261924_H
#ifndef ROTATE_T1850091912_H
#define ROTATE_T1850091912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rotate
struct  Rotate_t1850091912  : public Physics2DObject_t3026219368
{
public:
	// Enums/KeyGroups Rotate::typeOfControl
	int32_t ___typeOfControl_3;
	// System.Single Rotate::speed
	float ___speed_4;
	// System.Single Rotate::spin
	float ___spin_5;

public:
	inline static int32_t get_offset_of_typeOfControl_3() { return static_cast<int32_t>(offsetof(Rotate_t1850091912, ___typeOfControl_3)); }
	inline int32_t get_typeOfControl_3() const { return ___typeOfControl_3; }
	inline int32_t* get_address_of_typeOfControl_3() { return &___typeOfControl_3; }
	inline void set_typeOfControl_3(int32_t value)
	{
		___typeOfControl_3 = value;
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(Rotate_t1850091912, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_spin_5() { return static_cast<int32_t>(offsetof(Rotate_t1850091912, ___spin_5)); }
	inline float get_spin_5() const { return ___spin_5; }
	inline float* get_address_of_spin_5() { return &___spin_5; }
	inline void set_spin_5(float value)
	{
		___spin_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATE_T1850091912_H
#ifndef LOADLEVELACTION_T3647347530_H
#define LOADLEVELACTION_T3647347530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadLevelAction
struct  LoadLevelAction_t3647347530  : public Action_t3370723196
{
public:
	// System.String LoadLevelAction::levelName
	String_t* ___levelName_2;

public:
	inline static int32_t get_offset_of_levelName_2() { return static_cast<int32_t>(offsetof(LoadLevelAction_t3647347530, ___levelName_2)); }
	inline String_t* get_levelName_2() const { return ___levelName_2; }
	inline String_t** get_address_of_levelName_2() { return &___levelName_2; }
	inline void set_levelName_2(String_t* value)
	{
		___levelName_2 = value;
		Il2CppCodeGenWriteBarrier((&___levelName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADLEVELACTION_T3647347530_H
#ifndef DIALOGUEBALLOONACTION_T3348434864_H
#define DIALOGUEBALLOONACTION_T3348434864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DialogueBalloonAction
struct  DialogueBalloonAction_t3348434864  : public Action_t3370723196
{
public:
	// System.String DialogueBalloonAction::textToDisplay
	String_t* ___textToDisplay_2;
	// UnityEngine.Color DialogueBalloonAction::backgroundColor
	Color_t2555686324  ___backgroundColor_3;
	// UnityEngine.Color DialogueBalloonAction::textColor
	Color_t2555686324  ___textColor_4;
	// UnityEngine.Transform DialogueBalloonAction::targetObject
	Transform_t3600365921 * ___targetObject_5;
	// DialogueBalloonAction/DisappearMode DialogueBalloonAction::disappearMode
	int32_t ___disappearMode_6;
	// System.Single DialogueBalloonAction::timeToDisappear
	float ___timeToDisappear_7;
	// UnityEngine.KeyCode DialogueBalloonAction::keyToPress
	int32_t ___keyToPress_8;
	// DialogueBalloonAction DialogueBalloonAction::followingText
	DialogueBalloonAction_t3348434864 * ___followingText_9;
	// BalloonScript DialogueBalloonAction::b
	BalloonScript_t4017429074 * ___b_10;
	// System.Boolean DialogueBalloonAction::balloonIsActive
	bool ___balloonIsActive_11;

public:
	inline static int32_t get_offset_of_textToDisplay_2() { return static_cast<int32_t>(offsetof(DialogueBalloonAction_t3348434864, ___textToDisplay_2)); }
	inline String_t* get_textToDisplay_2() const { return ___textToDisplay_2; }
	inline String_t** get_address_of_textToDisplay_2() { return &___textToDisplay_2; }
	inline void set_textToDisplay_2(String_t* value)
	{
		___textToDisplay_2 = value;
		Il2CppCodeGenWriteBarrier((&___textToDisplay_2), value);
	}

	inline static int32_t get_offset_of_backgroundColor_3() { return static_cast<int32_t>(offsetof(DialogueBalloonAction_t3348434864, ___backgroundColor_3)); }
	inline Color_t2555686324  get_backgroundColor_3() const { return ___backgroundColor_3; }
	inline Color_t2555686324 * get_address_of_backgroundColor_3() { return &___backgroundColor_3; }
	inline void set_backgroundColor_3(Color_t2555686324  value)
	{
		___backgroundColor_3 = value;
	}

	inline static int32_t get_offset_of_textColor_4() { return static_cast<int32_t>(offsetof(DialogueBalloonAction_t3348434864, ___textColor_4)); }
	inline Color_t2555686324  get_textColor_4() const { return ___textColor_4; }
	inline Color_t2555686324 * get_address_of_textColor_4() { return &___textColor_4; }
	inline void set_textColor_4(Color_t2555686324  value)
	{
		___textColor_4 = value;
	}

	inline static int32_t get_offset_of_targetObject_5() { return static_cast<int32_t>(offsetof(DialogueBalloonAction_t3348434864, ___targetObject_5)); }
	inline Transform_t3600365921 * get_targetObject_5() const { return ___targetObject_5; }
	inline Transform_t3600365921 ** get_address_of_targetObject_5() { return &___targetObject_5; }
	inline void set_targetObject_5(Transform_t3600365921 * value)
	{
		___targetObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___targetObject_5), value);
	}

	inline static int32_t get_offset_of_disappearMode_6() { return static_cast<int32_t>(offsetof(DialogueBalloonAction_t3348434864, ___disappearMode_6)); }
	inline int32_t get_disappearMode_6() const { return ___disappearMode_6; }
	inline int32_t* get_address_of_disappearMode_6() { return &___disappearMode_6; }
	inline void set_disappearMode_6(int32_t value)
	{
		___disappearMode_6 = value;
	}

	inline static int32_t get_offset_of_timeToDisappear_7() { return static_cast<int32_t>(offsetof(DialogueBalloonAction_t3348434864, ___timeToDisappear_7)); }
	inline float get_timeToDisappear_7() const { return ___timeToDisappear_7; }
	inline float* get_address_of_timeToDisappear_7() { return &___timeToDisappear_7; }
	inline void set_timeToDisappear_7(float value)
	{
		___timeToDisappear_7 = value;
	}

	inline static int32_t get_offset_of_keyToPress_8() { return static_cast<int32_t>(offsetof(DialogueBalloonAction_t3348434864, ___keyToPress_8)); }
	inline int32_t get_keyToPress_8() const { return ___keyToPress_8; }
	inline int32_t* get_address_of_keyToPress_8() { return &___keyToPress_8; }
	inline void set_keyToPress_8(int32_t value)
	{
		___keyToPress_8 = value;
	}

	inline static int32_t get_offset_of_followingText_9() { return static_cast<int32_t>(offsetof(DialogueBalloonAction_t3348434864, ___followingText_9)); }
	inline DialogueBalloonAction_t3348434864 * get_followingText_9() const { return ___followingText_9; }
	inline DialogueBalloonAction_t3348434864 ** get_address_of_followingText_9() { return &___followingText_9; }
	inline void set_followingText_9(DialogueBalloonAction_t3348434864 * value)
	{
		___followingText_9 = value;
		Il2CppCodeGenWriteBarrier((&___followingText_9), value);
	}

	inline static int32_t get_offset_of_b_10() { return static_cast<int32_t>(offsetof(DialogueBalloonAction_t3348434864, ___b_10)); }
	inline BalloonScript_t4017429074 * get_b_10() const { return ___b_10; }
	inline BalloonScript_t4017429074 ** get_address_of_b_10() { return &___b_10; }
	inline void set_b_10(BalloonScript_t4017429074 * value)
	{
		___b_10 = value;
		Il2CppCodeGenWriteBarrier((&___b_10), value);
	}

	inline static int32_t get_offset_of_balloonIsActive_11() { return static_cast<int32_t>(offsetof(DialogueBalloonAction_t3348434864, ___balloonIsActive_11)); }
	inline bool get_balloonIsActive_11() const { return ___balloonIsActive_11; }
	inline bool* get_address_of_balloonIsActive_11() { return &___balloonIsActive_11; }
	inline void set_balloonIsActive_11(bool value)
	{
		___balloonIsActive_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGUEBALLOONACTION_T3348434864_H
#ifndef PUSH_T4181980645_H
#define PUSH_T4181980645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Push
struct  Push_t4181980645  : public Physics2DObject_t3026219368
{
public:
	// UnityEngine.KeyCode Push::key
	int32_t ___key_3;
	// System.Single Push::pushStrength
	float ___pushStrength_4;
	// Enums/Axes Push::axis
	int32_t ___axis_5;
	// System.Boolean Push::relativeAxis
	bool ___relativeAxis_6;
	// System.Boolean Push::keyPressed
	bool ___keyPressed_7;
	// UnityEngine.Vector2 Push::pushVector
	Vector2_t2156229523  ___pushVector_8;

public:
	inline static int32_t get_offset_of_key_3() { return static_cast<int32_t>(offsetof(Push_t4181980645, ___key_3)); }
	inline int32_t get_key_3() const { return ___key_3; }
	inline int32_t* get_address_of_key_3() { return &___key_3; }
	inline void set_key_3(int32_t value)
	{
		___key_3 = value;
	}

	inline static int32_t get_offset_of_pushStrength_4() { return static_cast<int32_t>(offsetof(Push_t4181980645, ___pushStrength_4)); }
	inline float get_pushStrength_4() const { return ___pushStrength_4; }
	inline float* get_address_of_pushStrength_4() { return &___pushStrength_4; }
	inline void set_pushStrength_4(float value)
	{
		___pushStrength_4 = value;
	}

	inline static int32_t get_offset_of_axis_5() { return static_cast<int32_t>(offsetof(Push_t4181980645, ___axis_5)); }
	inline int32_t get_axis_5() const { return ___axis_5; }
	inline int32_t* get_address_of_axis_5() { return &___axis_5; }
	inline void set_axis_5(int32_t value)
	{
		___axis_5 = value;
	}

	inline static int32_t get_offset_of_relativeAxis_6() { return static_cast<int32_t>(offsetof(Push_t4181980645, ___relativeAxis_6)); }
	inline bool get_relativeAxis_6() const { return ___relativeAxis_6; }
	inline bool* get_address_of_relativeAxis_6() { return &___relativeAxis_6; }
	inline void set_relativeAxis_6(bool value)
	{
		___relativeAxis_6 = value;
	}

	inline static int32_t get_offset_of_keyPressed_7() { return static_cast<int32_t>(offsetof(Push_t4181980645, ___keyPressed_7)); }
	inline bool get_keyPressed_7() const { return ___keyPressed_7; }
	inline bool* get_address_of_keyPressed_7() { return &___keyPressed_7; }
	inline void set_keyPressed_7(bool value)
	{
		___keyPressed_7 = value;
	}

	inline static int32_t get_offset_of_pushVector_8() { return static_cast<int32_t>(offsetof(Push_t4181980645, ___pushVector_8)); }
	inline Vector2_t2156229523  get_pushVector_8() const { return ___pushVector_8; }
	inline Vector2_t2156229523 * get_address_of_pushVector_8() { return &___pushVector_8; }
	inline void set_pushVector_8(Vector2_t2156229523  value)
	{
		___pushVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUSH_T4181980645_H
#ifndef AUTOMOVE_T3127166102_H
#define AUTOMOVE_T3127166102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AutoMove
struct  AutoMove_t3127166102  : public Physics2DObject_t3026219368
{
public:
	// UnityEngine.Vector2 AutoMove::direction
	Vector2_t2156229523  ___direction_3;
	// System.Boolean AutoMove::relativeToRotation
	bool ___relativeToRotation_4;

public:
	inline static int32_t get_offset_of_direction_3() { return static_cast<int32_t>(offsetof(AutoMove_t3127166102, ___direction_3)); }
	inline Vector2_t2156229523  get_direction_3() const { return ___direction_3; }
	inline Vector2_t2156229523 * get_address_of_direction_3() { return &___direction_3; }
	inline void set_direction_3(Vector2_t2156229523  value)
	{
		___direction_3 = value;
	}

	inline static int32_t get_offset_of_relativeToRotation_4() { return static_cast<int32_t>(offsetof(AutoMove_t3127166102, ___relativeToRotation_4)); }
	inline bool get_relativeToRotation_4() const { return ___relativeToRotation_4; }
	inline bool* get_address_of_relativeToRotation_4() { return &___relativeToRotation_4; }
	inline void set_relativeToRotation_4(bool value)
	{
		___relativeToRotation_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOVE_T3127166102_H
#ifndef CONDITIONREPEAT_T1215900890_H
#define CONDITIONREPEAT_T1215900890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConditionRepeat
struct  ConditionRepeat_t1215900890  : public ConditionBase_t3399778919
{
public:
	// System.Single ConditionRepeat::initialDelay
	float ___initialDelay_9;
	// System.Single ConditionRepeat::frequency
	float ___frequency_10;
	// System.Single ConditionRepeat::timeLastEventFired
	float ___timeLastEventFired_11;

public:
	inline static int32_t get_offset_of_initialDelay_9() { return static_cast<int32_t>(offsetof(ConditionRepeat_t1215900890, ___initialDelay_9)); }
	inline float get_initialDelay_9() const { return ___initialDelay_9; }
	inline float* get_address_of_initialDelay_9() { return &___initialDelay_9; }
	inline void set_initialDelay_9(float value)
	{
		___initialDelay_9 = value;
	}

	inline static int32_t get_offset_of_frequency_10() { return static_cast<int32_t>(offsetof(ConditionRepeat_t1215900890, ___frequency_10)); }
	inline float get_frequency_10() const { return ___frequency_10; }
	inline float* get_address_of_frequency_10() { return &___frequency_10; }
	inline void set_frequency_10(float value)
	{
		___frequency_10 = value;
	}

	inline static int32_t get_offset_of_timeLastEventFired_11() { return static_cast<int32_t>(offsetof(ConditionRepeat_t1215900890, ___timeLastEventFired_11)); }
	inline float get_timeLastEventFired_11() const { return ___timeLastEventFired_11; }
	inline float* get_address_of_timeLastEventFired_11() { return &___timeLastEventFired_11; }
	inline void set_timeLastEventFired_11(float value)
	{
		___timeLastEventFired_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONREPEAT_T1215900890_H
#ifndef CONSUMERESOURCEACTION_T2832324500_H
#define CONSUMERESOURCEACTION_T2832324500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConsumeResourceAction
struct  ConsumeResourceAction_t2832324500  : public Action_t3370723196
{
public:
	// System.Int32 ConsumeResourceAction::checkFor
	int32_t ___checkFor_2;
	// System.Int32 ConsumeResourceAction::amountNeeded
	int32_t ___amountNeeded_3;
	// UIScript ConsumeResourceAction::userInterface
	UIScript_t1195624422 * ___userInterface_4;

public:
	inline static int32_t get_offset_of_checkFor_2() { return static_cast<int32_t>(offsetof(ConsumeResourceAction_t2832324500, ___checkFor_2)); }
	inline int32_t get_checkFor_2() const { return ___checkFor_2; }
	inline int32_t* get_address_of_checkFor_2() { return &___checkFor_2; }
	inline void set_checkFor_2(int32_t value)
	{
		___checkFor_2 = value;
	}

	inline static int32_t get_offset_of_amountNeeded_3() { return static_cast<int32_t>(offsetof(ConsumeResourceAction_t2832324500, ___amountNeeded_3)); }
	inline int32_t get_amountNeeded_3() const { return ___amountNeeded_3; }
	inline int32_t* get_address_of_amountNeeded_3() { return &___amountNeeded_3; }
	inline void set_amountNeeded_3(int32_t value)
	{
		___amountNeeded_3 = value;
	}

	inline static int32_t get_offset_of_userInterface_4() { return static_cast<int32_t>(offsetof(ConsumeResourceAction_t2832324500, ___userInterface_4)); }
	inline UIScript_t1195624422 * get_userInterface_4() const { return ___userInterface_4; }
	inline UIScript_t1195624422 ** get_address_of_userInterface_4() { return &___userInterface_4; }
	inline void set_userInterface_4(UIScript_t1195624422 * value)
	{
		___userInterface_4 = value;
		Il2CppCodeGenWriteBarrier((&___userInterface_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSUMERESOURCEACTION_T2832324500_H
#ifndef DESTROYACTION_T1816662878_H
#define DESTROYACTION_T1816662878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyAction
struct  DestroyAction_t1816662878  : public Action_t3370723196
{
public:
	// Enums/Targets DestroyAction::target
	int32_t ___target_2;
	// UnityEngine.GameObject DestroyAction::deathEffect
	GameObject_t1113636619 * ___deathEffect_3;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(DestroyAction_t1816662878, ___target_2)); }
	inline int32_t get_target_2() const { return ___target_2; }
	inline int32_t* get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(int32_t value)
	{
		___target_2 = value;
	}

	inline static int32_t get_offset_of_deathEffect_3() { return static_cast<int32_t>(offsetof(DestroyAction_t1816662878, ___deathEffect_3)); }
	inline GameObject_t1113636619 * get_deathEffect_3() const { return ___deathEffect_3; }
	inline GameObject_t1113636619 ** get_address_of_deathEffect_3() { return &___deathEffect_3; }
	inline void set_deathEffect_3(GameObject_t1113636619 * value)
	{
		___deathEffect_3 = value;
		Il2CppCodeGenWriteBarrier((&___deathEffect_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYACTION_T1816662878_H
#ifndef TELEPORTACTION_T1205856175_H
#define TELEPORTACTION_T1205856175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TeleportAction
struct  TeleportAction_t1205856175  : public Action_t3370723196
{
public:
	// UnityEngine.GameObject TeleportAction::objectToMove
	GameObject_t1113636619 * ___objectToMove_2;
	// UnityEngine.Vector2 TeleportAction::newPosition
	Vector2_t2156229523  ___newPosition_3;
	// System.Boolean TeleportAction::stopMovements
	bool ___stopMovements_4;

public:
	inline static int32_t get_offset_of_objectToMove_2() { return static_cast<int32_t>(offsetof(TeleportAction_t1205856175, ___objectToMove_2)); }
	inline GameObject_t1113636619 * get_objectToMove_2() const { return ___objectToMove_2; }
	inline GameObject_t1113636619 ** get_address_of_objectToMove_2() { return &___objectToMove_2; }
	inline void set_objectToMove_2(GameObject_t1113636619 * value)
	{
		___objectToMove_2 = value;
		Il2CppCodeGenWriteBarrier((&___objectToMove_2), value);
	}

	inline static int32_t get_offset_of_newPosition_3() { return static_cast<int32_t>(offsetof(TeleportAction_t1205856175, ___newPosition_3)); }
	inline Vector2_t2156229523  get_newPosition_3() const { return ___newPosition_3; }
	inline Vector2_t2156229523 * get_address_of_newPosition_3() { return &___newPosition_3; }
	inline void set_newPosition_3(Vector2_t2156229523  value)
	{
		___newPosition_3 = value;
	}

	inline static int32_t get_offset_of_stopMovements_4() { return static_cast<int32_t>(offsetof(TeleportAction_t1205856175, ___stopMovements_4)); }
	inline bool get_stopMovements_4() const { return ___stopMovements_4; }
	inline bool* get_address_of_stopMovements_4() { return &___stopMovements_4; }
	inline void set_stopMovements_4(bool value)
	{
		___stopMovements_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELEPORTACTION_T1205856175_H
#ifndef CREATEOBJECTACTION_T2026806364_H
#define CREATEOBJECTACTION_T2026806364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateObjectAction
struct  CreateObjectAction_t2026806364  : public Action_t3370723196
{
public:
	// UnityEngine.GameObject CreateObjectAction::prefabToCreate
	GameObject_t1113636619 * ___prefabToCreate_2;
	// UnityEngine.Vector2 CreateObjectAction::newPosition
	Vector2_t2156229523  ___newPosition_3;
	// System.Boolean CreateObjectAction::relativeToThisObject
	bool ___relativeToThisObject_4;

public:
	inline static int32_t get_offset_of_prefabToCreate_2() { return static_cast<int32_t>(offsetof(CreateObjectAction_t2026806364, ___prefabToCreate_2)); }
	inline GameObject_t1113636619 * get_prefabToCreate_2() const { return ___prefabToCreate_2; }
	inline GameObject_t1113636619 ** get_address_of_prefabToCreate_2() { return &___prefabToCreate_2; }
	inline void set_prefabToCreate_2(GameObject_t1113636619 * value)
	{
		___prefabToCreate_2 = value;
		Il2CppCodeGenWriteBarrier((&___prefabToCreate_2), value);
	}

	inline static int32_t get_offset_of_newPosition_3() { return static_cast<int32_t>(offsetof(CreateObjectAction_t2026806364, ___newPosition_3)); }
	inline Vector2_t2156229523  get_newPosition_3() const { return ___newPosition_3; }
	inline Vector2_t2156229523 * get_address_of_newPosition_3() { return &___newPosition_3; }
	inline void set_newPosition_3(Vector2_t2156229523  value)
	{
		___newPosition_3 = value;
	}

	inline static int32_t get_offset_of_relativeToThisObject_4() { return static_cast<int32_t>(offsetof(CreateObjectAction_t2026806364, ___relativeToThisObject_4)); }
	inline bool get_relativeToThisObject_4() const { return ___relativeToThisObject_4; }
	inline bool* get_address_of_relativeToThisObject_4() { return &___relativeToThisObject_4; }
	inline void set_relativeToThisObject_4(bool value)
	{
		___relativeToThisObject_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEOBJECTACTION_T2026806364_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_28)); }
	inline FontData_t746620069 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t746620069 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t746620069 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_30)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_32)); }
	inline Material_t340375123 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t340375123 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_30;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_31;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_32;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_33;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_34;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_35;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_36;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_37;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_38;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_39;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_29)); }
	inline Sprite_t280657092 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t280657092 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_30)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_Type_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_31)); }
	inline int32_t get_m_Type_31() const { return ___m_Type_31; }
	inline int32_t* get_address_of_m_Type_31() { return &___m_Type_31; }
	inline void set_m_Type_31(int32_t value)
	{
		___m_Type_31 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_32)); }
	inline bool get_m_PreserveAspect_32() const { return ___m_PreserveAspect_32; }
	inline bool* get_address_of_m_PreserveAspect_32() { return &___m_PreserveAspect_32; }
	inline void set_m_PreserveAspect_32(bool value)
	{
		___m_PreserveAspect_32 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_33)); }
	inline bool get_m_FillCenter_33() const { return ___m_FillCenter_33; }
	inline bool* get_address_of_m_FillCenter_33() { return &___m_FillCenter_33; }
	inline void set_m_FillCenter_33(bool value)
	{
		___m_FillCenter_33 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_34)); }
	inline int32_t get_m_FillMethod_34() const { return ___m_FillMethod_34; }
	inline int32_t* get_address_of_m_FillMethod_34() { return &___m_FillMethod_34; }
	inline void set_m_FillMethod_34(int32_t value)
	{
		___m_FillMethod_34 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_35)); }
	inline float get_m_FillAmount_35() const { return ___m_FillAmount_35; }
	inline float* get_address_of_m_FillAmount_35() { return &___m_FillAmount_35; }
	inline void set_m_FillAmount_35(float value)
	{
		___m_FillAmount_35 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_36)); }
	inline bool get_m_FillClockwise_36() const { return ___m_FillClockwise_36; }
	inline bool* get_address_of_m_FillClockwise_36() { return &___m_FillClockwise_36; }
	inline void set_m_FillClockwise_36(bool value)
	{
		___m_FillClockwise_36 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_37)); }
	inline int32_t get_m_FillOrigin_37() const { return ___m_FillOrigin_37; }
	inline int32_t* get_address_of_m_FillOrigin_37() { return &___m_FillOrigin_37; }
	inline void set_m_FillOrigin_37(int32_t value)
	{
		___m_FillOrigin_37 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_38)); }
	inline float get_m_AlphaHitTestMinimumThreshold_38() const { return ___m_AlphaHitTestMinimumThreshold_38; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_38() { return &___m_AlphaHitTestMinimumThreshold_38; }
	inline void set_m_AlphaHitTestMinimumThreshold_38(float value)
	{
		___m_AlphaHitTestMinimumThreshold_38 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_39() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Tracked_39)); }
	inline bool get_m_Tracked_39() const { return ___m_Tracked_39; }
	inline bool* get_address_of_m_Tracked_39() { return &___m_Tracked_39; }
	inline void set_m_Tracked_39(bool value)
	{
		___m_Tracked_39 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_28;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_40;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_41;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_42;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_43;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t4142344393 * ___m_TrackedTexturelessImages_44;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_45;
	// System.Action`1<UnityEngine.U2D.SpriteAtlas> UnityEngine.UI.Image::<>f__mg$cache0
	Action_1_t819399007 * ___U3CU3Ef__mgU24cache0_46;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_40() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_40)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_40() const { return ___s_VertScratch_40; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_40() { return &___s_VertScratch_40; }
	inline void set_s_VertScratch_40(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_40), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_41() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_41)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_41() const { return ___s_UVScratch_41; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_41() { return &___s_UVScratch_41; }
	inline void set_s_UVScratch_41(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_41), value);
	}

	inline static int32_t get_offset_of_s_Xy_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_42)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_42() const { return ___s_Xy_42; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_42() { return &___s_Xy_42; }
	inline void set_s_Xy_42(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_42), value);
	}

	inline static int32_t get_offset_of_s_Uv_43() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_43)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_43() const { return ___s_Uv_43; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_43() { return &___s_Uv_43; }
	inline void set_s_Uv_43(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_43 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_43), value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_44() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___m_TrackedTexturelessImages_44)); }
	inline List_1_t4142344393 * get_m_TrackedTexturelessImages_44() const { return ___m_TrackedTexturelessImages_44; }
	inline List_1_t4142344393 ** get_address_of_m_TrackedTexturelessImages_44() { return &___m_TrackedTexturelessImages_44; }
	inline void set_m_TrackedTexturelessImages_44(List_1_t4142344393 * value)
	{
		___m_TrackedTexturelessImages_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedTexturelessImages_44), value);
	}

	inline static int32_t get_offset_of_s_Initialized_45() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Initialized_45)); }
	inline bool get_s_Initialized_45() const { return ___s_Initialized_45; }
	inline bool* get_address_of_s_Initialized_45() { return &___s_Initialized_45; }
	inline void set_s_Initialized_45(bool value)
	{
		___s_Initialized_45 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_46() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___U3CU3Ef__mgU24cache0_46)); }
	inline Action_1_t819399007 * get_U3CU3Ef__mgU24cache0_46() const { return ___U3CU3Ef__mgU24cache0_46; }
	inline Action_1_t819399007 ** get_address_of_U3CU3Ef__mgU24cache0_46() { return &___U3CU3Ef__mgU24cache0_46; }
	inline void set_U3CU3Ef__mgU24cache0_46(Action_1_t819399007 * value)
	{
		___U3CU3Ef__mgU24cache0_46 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_46), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_t2156229523  m_Items[1];

public:
	inline Vector2_t2156229523  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t2156229523 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t2156229523  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t2156229523  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t2156229523 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t2156229523  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t1113636619 * m_Items[1];

public:
	inline GameObject_t1113636619 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1113636619 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.UI.Text[]
struct TextU5BU5D_t422084607  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Text_t1901882714 * m_Items[1];

public:
	inline Text_t1901882714 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Text_t1901882714 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Text_t1901882714 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Text_t1901882714 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Text_t1901882714 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Text_t1901882714 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t385246372  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t2146457487  List_1_GetEnumerator_m2930774921_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m2446893047_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Func`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Func_1__ctor_m1399073142_gshared (Func_1_t3822001908 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m2601736566_gshared (Dictionary_2_t1968819495 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKey(!0)
extern "C" IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m2382293057_gshared (Dictionary_2_t1968819495 * __this, int32_t p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Item(!0)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Dictionary_2_get_Item_m107019914_gshared (Dictionary_2_t1968819495 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_Add_m2059424751_gshared (Dictionary_2_t1968819495 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Resources_Load_TisRuntimeObject_m597869152_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method);
// System.Void Physics2DObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Physics2DObject__ctor_m38296749 (Physics2DObject_t3026219368 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Multiply_m2347887432 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::AddRelativeForce(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddRelativeForce_m3358548001 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddForce_m1113499586 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C" IL2CPP_METHOD_ATTR bool Behaviour_get_enabled_m753527255 (Behaviour_t1437897464 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Transform_get_rotation_m3502953881 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Quaternion_get_eulerAngles_m3425202016 (Quaternion_t2301928331 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Void Utils::DrawMoveArrowGizmo(UnityEngine.Vector3,UnityEngine.Vector2,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Utils_DrawMoveArrowGizmo_m4283051242 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___position0, Vector2_t2156229523  ___direction1, float ___extraAngle2, float ___scale3, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::MoveRotation(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_MoveRotation_m3032842781 (Rigidbody2D_t939494601 * __this, float p0, const RuntimeMethod* method);
// System.Void Utils::DrawRotateArrowGizmo(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Utils_DrawRotateArrowGizmo_m3937198024 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___position0, float ___strength1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t3704657025_m3396022872(__this, method) ((  RectTransform_t3704657025 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2670269651_m980647750(__this, method) ((  Image_t2670269651 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_time()
extern "C" IL2CPP_METHOD_ATTR float Time_get_time_m2907476221 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void RectTransform_set_pivot_m909387058 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C" IL2CPP_METHOD_ATTR Camera_t4157153871 * Camera_get_main_m3643453163 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_zero_m1409827619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  RectTransformUtility_WorldToScreenPoint_m3914148572 (RuntimeObject * __this /* static, unused */, Camera_t4157153871 * p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector2_op_Implicit_m1860157806 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Void BalloonScript::FollowTarget()
extern "C" IL2CPP_METHOD_ATTR void BalloonScript_FollowTarget_m625056729 (BalloonScript_t4017429074 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyUp(UnityEngine.KeyCode)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetKeyUp_m2808015270 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C" IL2CPP_METHOD_ATTR void UnityAction_Invoke_m893829196 (UnityAction_t3245792599 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502(__this, method) ((  SpriteRenderer_t3235626157 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C" IL2CPP_METHOD_ATTR Bounds_t2266837910  Renderer_get_bounds_m1803204000 (Renderer_t2627027031 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Bounds_get_size_m1178783246 (Bounds_t2266837910 * __this, const RuntimeMethod* method);
// System.Void Utils::Collider2DDialogWindow(UnityEngine.GameObject,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Utils_Collider2DDialogWindow_m3028026175 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___gameObjectRef0, bool ___makeItTrigger1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, method) ((  Camera_t4157153871 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_Lerp_m407887542 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Camera_ScreenToWorldPoint_m3978588570 (Camera_t4157153871 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Camera::get_pixelWidth()
extern "C" IL2CPP_METHOD_ATTR int32_t Camera_get_pixelWidth_m1110053668 (Camera_t4157153871 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Camera::get_pixelHeight()
extern "C" IL2CPP_METHOD_ATTR int32_t Camera_get_pixelHeight_m722276884 (Camera_t4157153871 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m1719387948 (Vector3_t3722313464 * __this, float p0, float p1, const RuntimeMethod* method);
// System.String UnityEngine.GameObject::get_tag()
extern "C" IL2CPP_METHOD_ATTR String_t* GameObject_get_tag_m3951609671 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void ConditionBase::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ConditionBase__ctor_m238416164 (ConditionBase_t3399778919 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Component::CompareTag(System.String)
extern "C" IL2CPP_METHOD_ATTR bool Component_CompareTag_m1328479619 (Component_t1923634451 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void ConditionBase::ExecuteAllActions(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void ConditionBase_ExecuteAllActions_m3297109267 (ConditionBase_t3399778919 * __this, GameObject_t1113636619 * ___dataObject0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Action>::.ctor()
#define List_1__ctor_m3594099166(__this, method) ((  void (*) (List_1_t547830642 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Action>::GetEnumerator()
#define List_1_GetEnumerator_m2023103186(__this, method) ((  Enumerator_t2437074519  (*) (List_1_t547830642 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<Action>::get_Current()
#define Enumerator_get_Current_m263105171(__this, method) ((  Action_t3370723196 * (*) (Enumerator_t2437074519 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method)
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogWarning_m3752629331 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<Action>::MoveNext()
#define Enumerator_MoveNext_m4019089914(__this, method) ((  bool (*) (Enumerator_t2437074519 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Action>::Dispose()
#define Enumerator_Dispose_m1654570958(__this, method) ((  void (*) (Enumerator_t2437074519 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C" IL2CPP_METHOD_ATTR void UnityEvent_Invoke_m3065672636 (UnityEvent_t2581268647 * __this, const RuntimeMethod* method);
// UnityEngine.Collider2D UnityEngine.Collision2D::get_collider()
extern "C" IL2CPP_METHOD_ATTR Collider2D_t2806799626 * Collision2D_get_collider_m4087612183 (Collision2D_t2842956331 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Collision2D::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Collision2D_get_gameObject_m1443495885 (Collision2D_t2842956331 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m17791917 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetKey_m3736388334 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Void Action::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Action__ctor_m2761626621 (Action_t3370723196 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<UIScript>()
#define Object_FindObjectOfType_TisUIScript_t1195624422_m1742595665(__this /* static, unused */, method) ((  UIScript_t1195624422 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared)(__this /* static, unused */, method)
// System.Boolean UIScript::CheckIfHasResources(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool UIScript_CheckIfHasResources_m685094672 (UIScript_t1195624422 * __this, int32_t ___resourceType0, int32_t ___amountNeeded1, const RuntimeMethod* method);
// System.Void UIScript::ConsumeResource(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIScript_ConsumeResource_m3280961331 (UIScript_t1195624422 * __this, int32_t ___resourceType0, int32_t ___amountNeeded1, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t1113636619_m4070250708(__this /* static, unused */, p0, method) ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m2446893047_gshared)(__this /* static, unused */, p0, method)
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Implicit_m4260192859 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Addition_m800700293 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void DestroyForPointsAttribute::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void DestroyForPointsAttribute_OnTriggerEnter2D_m3805996196 (DestroyForPointsAttribute_t2602039747 * __this, Collider2D_t2806799626 * ___collisionData0, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::CompareTag(System.String)
extern "C" IL2CPP_METHOD_ATTR bool GameObject_CompareTag_m3144439756 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<BulletAttribute>()
#define GameObject_GetComponent_TisBulletAttribute_t588811475_m587253523(__this, method) ((  BulletAttribute_t588811475 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UIScript::AddPoints(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIScript_AddPoints_m1714476938 (UIScript_t1195624422 * __this, int32_t ___playerNumber0, int32_t ___amount1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C" IL2CPP_METHOD_ATTR void Color32__ctor_m4150508762 (Color32_t2600501292 * __this, uint8_t p0, uint8_t p1, uint8_t p2, uint8_t p3, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color32_op_Implicit_m213813866 (RuntimeObject * __this /* static, unused */, Color32_t2600501292  p0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_white_m332174077 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<DialogueSystem>()
#define Object_FindObjectOfType_TisDialogueSystem_t292220827_m1864039531(__this /* static, unused */, method) ((  DialogueSystem_t292220827 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared)(__this /* static, unused */, method)
// BalloonScript DialogueSystem::CreateBalloon(System.String,System.Boolean,UnityEngine.KeyCode,System.Single,UnityEngine.Color,UnityEngine.Color,UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR BalloonScript_t4017429074 * DialogueSystem_CreateBalloon_m1415492347 (DialogueSystem_t292220827 * __this, String_t* ___dialogueString0, bool ___usingButton1, int32_t ___button2, float ___timeToDisappear3, Color_t2555686324  ___backgroundC4, Color_t2555686324  ___textC5, Transform_t3600365921 * ___targetObj6, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void UnityAction__ctor_m772160306 (UnityAction_t3245792599 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method);
// System.Collections.IEnumerator DialogueBalloonAction::WaitForBallonDestroyed()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* DialogueBalloonAction_WaitForBallonDestroyed_m1668273814 (DialogueBalloonAction_t3348434864 * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void DialogueBalloonAction/<WaitForBallonDestroyed>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CWaitForBallonDestroyedU3Ec__Iterator0__ctor_m108888311 (U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479 * __this, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR Delegate_t1188392813 * Delegate_Remove_m334097152 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.Func`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_1__ctor_m1399073142(__this, p0, p1, method) ((  void (*) (Func_1_t3822001908 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_1__ctor_m1399073142_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.WaitUntil::.ctor(System.Func`1<System.Boolean>)
extern "C" IL2CPP_METHOD_ATTR void WaitUntil__ctor_m4227046299 (WaitUntil_t3373419216 * __this, Func_1_t3822001908 * p0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<BalloonScript>()
#define GameObject_GetComponent_TisBalloonScript_t4017429074_m2020822283(__this, method) ((  BalloonScript_t4017429074 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Transform_SetParent_m273471670 (Transform_t3600365921 * __this, Transform_t3600365921 * p0, bool p1, const RuntimeMethod* method);
// System.Void BalloonScript::Setup(System.String,System.Boolean,UnityEngine.KeyCode,System.Single,UnityEngine.Color,UnityEngine.Color,UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void BalloonScript_Setup_m3797033188 (BalloonScript_t4017429074 * __this, String_t* ___dialogueString0, bool ____isUsingButton1, int32_t ____buttonUsed2, float ____time3, Color_t2555686324  ___backgroundC4, Color_t2555686324  ___textC5, Transform_t3600365921 * ____targetObj6, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Void Utils::SetAxisTowards(Enums/Directions,UnityEngine.Transform,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void Utils_SetAxisTowards_m1999213583 (RuntimeObject * __this /* static, unused */, int32_t ___axis0, Transform_t3600365921 * ___t1, Vector2_t2156229523  ___direction2, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_fixedDeltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_fixedDeltaTime_m3595802076 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::Lerp(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_Lerp_m854472224 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::MovePosition(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_MovePosition_m1934912072 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method);
// System.Void UIScript::SetHealth(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIScript_SetHealth_m2132392576 (UIScript_t1195624422 * __this, int32_t ___amount0, int32_t ___playerNumber1, const RuntimeMethod* method);
// System.Void UIScript::ChangeHealth(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIScript_ChangeHealth_m2753858397 (UIScript_t1195624422 * __this, int32_t ___change0, int32_t ___playerNumber1, const RuntimeMethod* method);
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m1310743131 (ScriptableObject_t2528358522 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
#define List_1_get_Count_m2276455407(__this, method) ((  int32_t (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
#define List_1_get_Item_m3346958548(__this, p0, method) ((  String_t* (*) (List_1_t3319525431 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_get_up_m2647420593 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddForce_m1099013366 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  p0, int32_t p1, const RuntimeMethod* method);
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C" IL2CPP_METHOD_ATTR Scene_t2348375561  SceneManager_GetActiveScene_m1825203488 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* Scene_get_name_m622963475 (Scene_t2348375561 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C" IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m2298600132 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Void ModifyHealthAttribute::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void ModifyHealthAttribute_OnTriggerEnter2D_m197713471 (ModifyHealthAttribute_t3455596632 * __this, Collider2D_t2806799626 * ___colliderData0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<HealthSystemAttribute>()
#define GameObject_GetComponent_TisHealthSystemAttribute_t2647166000_m3641020519(__this, method) ((  HealthSystemAttribute_t2647166000 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void HealthSystemAttribute::ModifyHealth(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void HealthSystemAttribute_ModifyHealth_m2785567747 (HealthSystemAttribute_t2647166000 * __this, int32_t ___amount0, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxis(System.String)
extern "C" IL2CPP_METHOD_ATTR float Input_GetAxis_m4009438427 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector2_get_sqrMagnitude_m837837635 (Vector2_t2156229523 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(System.String)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m2928138282 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider2D>()
#define Component_GetComponent_TisBoxCollider2D_t3581341831_m414724273(__this, method) ((  BoxCollider2D_t3581341831 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Collections.IEnumerator ObjectCreatorArea::SpawnObject()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* ObjectCreatorArea_SpawnObject_m1647072653 (ObjectCreatorArea_t9936620 * __this, const RuntimeMethod* method);
// System.Void ObjectCreatorArea/<SpawnObject>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CSpawnObjectU3Ec__Iterator0__ctor_m177627470 (U3CSpawnObjectU3Ec__Iterator0_t3452356763 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.BoxCollider2D::get_size()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  BoxCollider2D_get_size_m3529128736 (BoxCollider2D_t3581341831 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Random_Range_m2202990745 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m2199082655 (WaitForSeconds_t1699091251 * __this, float p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_eulerAngles_m2743581774 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_Euler_m3049309462 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Quaternion_op_Multiply_m2607404835 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Single Utils::Angle(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR float Utils_Angle_m3844311520 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___inputVector0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_eulerAngles_m135219616 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::set_tag(System.String)
extern "C" IL2CPP_METHOD_ATTR void GameObject_set_tag_m2353670106 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody2D>()
#define GameObject_GetComponent_TisRigidbody2D_t939494601_m2767154013(__this, method) ((  Rigidbody2D_t939494601 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<BulletAttribute>()
#define GameObject_AddComponent_TisBulletAttribute_t588811475_m718945470(__this, method) ((  BulletAttribute_t588811475 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void Utils::DrawShootArrowGizmo(UnityEngine.Vector3,UnityEngine.Vector2,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Utils_DrawShootArrowGizmo_m2541881908 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___position0, Vector2_t2156229523  ___direction1, float ___extraAngle2, float ___scale3, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeSelf()
extern "C" IL2CPP_METHOD_ATTR bool GameObject_get_activeSelf_m1767405923 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(__this, method) ((  SpriteRenderer_t3235626157 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Boolean UnityEngine.Renderer::get_enabled()
extern "C" IL2CPP_METHOD_ATTR bool Renderer_get_enabled_m3482452518 (Renderer_t2627027031 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Renderer_set_enabled_m1727253150 (Renderer_t2627027031 * __this, bool p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_normalized_m2454957984 (Vector3_t3722313464 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR float Vector2_Distance_m3048868881 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(__this, method) ((  Rigidbody2D_t939494601 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Boolean PickUpAndHold::PickUp()
extern "C" IL2CPP_METHOD_ATTR bool PickUpAndHold_PickUp_m1915750871 (PickUpAndHold_t4109171223 * __this, const RuntimeMethod* method);
// System.Void PickUpAndHold::Drop()
extern "C" IL2CPP_METHOD_ATTR void PickUpAndHold_Drop_m1762827602 (PickUpAndHold_t4109171223 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_bodyType(UnityEngine.RigidbodyType2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_bodyType_m1814100804 (Rigidbody2D_t939494601 * __this, int32_t p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_get_zero_m540426400 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_velocity_m2898400508 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_parent_m786917804 (Transform_t3600365921 * __this, Transform_t3600365921 * p0, const RuntimeMethod* method);
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObjectU5BU5D_t3328599146* GameObject_FindGameObjectsWithTag_m2585173894 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector3_get_sqrMagnitude_m1474274574 (Vector3_t3722313464 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Transform_get_parent_m835071599 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<PickUpAndHold>()
#define Component_GetComponent_TisPickUpAndHold_t4109171223_m2673350194(__this, method) ((  PickUpAndHold_t4109171223 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Vector2 Utils::GetVectorFromAxis(Enums/Axes)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Utils_GetVectorFromAxis_m3396922722 (RuntimeObject * __this /* static, unused */, int32_t ___axis0, const RuntimeMethod* method);
// UnityEngine.Sprite UnityEngine.SpriteRenderer::get_sprite()
extern "C" IL2CPP_METHOD_ATTR Sprite_t280657092 * SpriteRenderer_get_sprite_m663386871 (SpriteRenderer_t3235626157 * __this, const RuntimeMethod* method);
// System.Void UIScript::AddResource(System.Int32,System.Int32,UnityEngine.Sprite)
extern "C" IL2CPP_METHOD_ATTR void UIScript_AddResource_m2508601531 (UIScript_t1195624422 * __this, int32_t ___resourceType0, int32_t ___pickedUpAmount1, Sprite_t280657092 * ___graphics2, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::AddTorque(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddTorque_m1535770154 (Rigidbody2D_t939494601 * __this, float p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CircleCollider2D>()
#define Component_GetComponent_TisCircleCollider2D_t662546754_m1597288992(__this, method) ((  CircleCollider2D_t662546754 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Single UnityEngine.Rigidbody2D::get_mass()
extern "C" IL2CPP_METHOD_ATTR float Rigidbody2D_get_mass_m816391807 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method);
// System.Void Stopper::throwBall()
extern "C" IL2CPP_METHOD_ATTR void Stopper_throwBall_m3126347534 (Stopper_t1976456672 * __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_red_m3227813939 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void SpriteRenderer_set_color_m3056777566 (SpriteRenderer_t3235626157 * __this, Color_t2555686324  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_angularVelocity(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_angularVelocity_m2791812150 (Rigidbody2D_t939494601 * __this, float p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_up_m3972993886 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Void Stopper::onHolder(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Stopper_onHolder_m969266357 (Stopper_t1976456672 * __this, bool ___result0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Collision2D::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Collision2D_get_transform_m2670923831 (Collision2D_t2842956331 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Transform_GetChild_m1092972975 (Transform_t3600365921 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Collider2D::set_isTrigger(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Collider2D_set_isTrigger_m2923405871 (Collider2D_t2806799626 * __this, bool p0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_isKinematic_m1070983191 (Rigidbody2D_t939494601 * __this, bool p0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour_Invoke_m4227543964 (MonoBehaviour_t3962482529 * __this, String_t* p0, float p1, const RuntimeMethod* method);
// System.String System.Int32::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Int32_ToString_m141394615 (int32_t* __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C" IL2CPP_METHOD_ATTR void Image_set_sprite_m2369174689 (Image_t2670269651 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ResourceStruct>::.ctor()
#define Dictionary_2__ctor_m790289403(__this, method) ((  void (*) (Dictionary_2_t4045795053 *, const RuntimeMethod*))Dictionary_2__ctor_m2601736566_gshared)(__this, method)
// System.Void UIScript::GameWon(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIScript_GameWon_m3372413357 (UIScript_t1195624422 * __this, int32_t ___playerNumber0, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m1715369213 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void UIScript::GameOver(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIScript_GameOver_m2568581351 (UIScript_t1195624422 * __this, int32_t ___playerNumber0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,ResourceStruct>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m537408582(__this, p0, method) ((  bool (*) (Dictionary_2_t4045795053 *, int32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m2382293057_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.Int32,ResourceStruct>::get_Item(!0)
#define Dictionary_2_get_Item_m3023042062(__this, p0, method) ((  ResourceStruct_t862114426 * (*) (Dictionary_2_t4045795053 *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_m107019914_gshared)(__this, p0, method)
// System.Void UIItemScript::ShowNumber(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIItemScript_ShowNumber_m4287243492 (UIItemScript_t1692532892 * __this, int32_t ___n0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UIItemScript>()
#define GameObject_GetComponent_TisUIItemScript_t1692532892_m3238252138(__this, method) ((  UIItemScript_t1692532892 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void ResourceStruct::.ctor(System.Int32,UIItemScript)
extern "C" IL2CPP_METHOD_ATTR void ResourceStruct__ctor_m3839822749 (ResourceStruct_t862114426 * __this, int32_t ___a0, UIItemScript_t1692532892 * ___uiRef1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,ResourceStruct>::Add(!0,!1)
#define Dictionary_2_Add_m1464489386(__this, p0, p1, method) ((  void (*) (Dictionary_2_t4045795053 *, int32_t, ResourceStruct_t862114426 *, const RuntimeMethod*))Dictionary_2_Add_m2059424751_gshared)(__this, p0, p1, method)
// System.Void UIItemScript::DisplayIcon(UnityEngine.Sprite)
extern "C" IL2CPP_METHOD_ATTR void UIItemScript_DisplayIcon_m618755502 (UIItemScript_t1692532892 * __this, Sprite_t280657092 * ___s0, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<UnityEngine.Mesh>(System.String)
#define Resources_Load_TisMesh_t3648964284_m3514705491(__this /* static, unused */, p0, method) ((  Mesh_t3648964284 * (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m597869152_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Transform::set_up(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_up_m3321958190 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_UnaryNegation(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_UnaryNegation_m2172448356 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_right(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_right_m1787339266 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_right()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_get_right_m1027081661 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void Utils::DrawGizmo(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Vector2,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Utils_DrawGizmo_m3328925645 (RuntimeObject * __this /* static, unused */, Mesh_t3648964284 * ___meshToDraw0, Vector3_t3722313464  ___position1, Vector2_t2156229523  ___direction2, float ___extraAngle3, float ___scale4, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_green_m490390750 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void Gizmos_set_color_m3399737545 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_get_identity_m3722672781 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Sign_m3457838305 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::DrawMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Gizmos_DrawMesh_m113137071 (RuntimeObject * __this /* static, unused */, Mesh_t3648964284 * p0, Vector3_t3722313464  p1, Quaternion_t2301928331  p2, Vector3_t3722313464  p3, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector2_get_magnitude_m2752892833 (Vector2_t2156229523 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_forward_m3100859705 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_AngleAxis_m1767165696 (RuntimeObject * __this /* static, unused */, float p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_one_m1629952498 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Collections.IEnumerator Wander::ChangeDirection()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Wander_ChangeDirection_m1837724728 (Wander_t651605530 * __this, const RuntimeMethod* method);
// System.Void Wander/<ChangeDirection>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CChangeDirectionU3Ec__Iterator0__ctor_m557163208 (U3CChangeDirectionU3Ec__Iterator0_t1842818970 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Random::get_insideUnitCircle()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Random_get_insideUnitCircle_m1267895911 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Action::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Action__ctor_m2761626621 (Action_t3370723196 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Action::ExecuteAction(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR bool Action_ExecuteAction_m1759531290 (Action_t3370723196 * __this, GameObject_t1113636619 * ___other0, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AutoMove::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AutoMove__ctor_m3443296991 (AutoMove_t3127166102 * __this, const RuntimeMethod* method)
{
	{
		Vector2_t2156229523  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3970636864((&L_0), (1.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_direction_3(L_0);
		__this->set_relativeToRotation_4((bool)1);
		Physics2DObject__ctor_m38296749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AutoMove::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void AutoMove_FixedUpdate_m1334605003 (AutoMove_t3127166102 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoMove_FixedUpdate_m1334605003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_relativeToRotation_4();
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		Rigidbody2D_t939494601 * L_1 = ((Physics2DObject_t3026219368 *)__this)->get_rigidbody2D_2();
		Vector2_t2156229523  L_2 = __this->get_direction_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_3 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_2, (2.0f), /*hidden argument*/NULL);
		Rigidbody2D_AddRelativeForce_m3358548001(L_1, L_3, /*hidden argument*/NULL);
		goto IL_0046;
	}

IL_002b:
	{
		Rigidbody2D_t939494601 * L_4 = ((Physics2DObject_t3026219368 *)__this)->get_rigidbody2D_2();
		Vector2_t2156229523  L_5 = __this->get_direction_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_6 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_5, (2.0f), /*hidden argument*/NULL);
		Rigidbody2D_AddForce_m1113499586(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// System.Void AutoMove::OnDrawGizmosSelected()
extern "C" IL2CPP_METHOD_ATTR void AutoMove_OnDrawGizmosSelected_m2054477897 (AutoMove_t3127166102 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoMove_OnDrawGizmosSelected_m2054477897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float G_B4_0 = 0.0f;
	{
		bool L_0 = Behaviour_get_enabled_m753527255(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0058;
		}
	}
	{
		bool L_1 = __this->get_relativeToRotation_4();
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_3 = Transform_get_rotation_m3502953881(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_t3722313464  L_4 = Quaternion_get_eulerAngles_m3425202016((&V_1), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = (&V_2)->get_z_3();
		G_B4_0 = L_5;
		goto IL_003b;
	}

IL_0036:
	{
		G_B4_0 = (0.0f);
	}

IL_003b:
	{
		V_0 = G_B4_0;
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		Vector2_t2156229523  L_8 = __this->get_direction_3();
		float L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_DrawMoveArrowGizmo_m4283051242(NULL /*static, unused*/, L_7, L_8, L_9, (0.0f), /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AutoRotate::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AutoRotate__ctor_m2743996959 (AutoRotate_t913508323 * __this, const RuntimeMethod* method)
{
	{
		__this->set_rotationSpeed_3((5.0f));
		Physics2DObject__ctor_m38296749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AutoRotate::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void AutoRotate_FixedUpdate_m1842031208 (AutoRotate_t913508323 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_currentRotation_4();
		float L_1 = __this->get_rotationSpeed_3();
		__this->set_currentRotation_4(((float)il2cpp_codegen_add((float)L_0, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(0.02f), (float)L_1)), (float)(10.0f))))));
		Rigidbody2D_t939494601 * L_2 = ((Physics2DObject_t3026219368 *)__this)->get_rigidbody2D_2();
		float L_3 = __this->get_currentRotation_4();
		Rigidbody2D_MoveRotation_m3032842781(L_2, ((-L_3)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void AutoRotate::OnDrawGizmosSelected()
extern "C" IL2CPP_METHOD_ATTR void AutoRotate_OnDrawGizmosSelected_m911995967 (AutoRotate_t913508323 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoRotate_OnDrawGizmosSelected_m911995967_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Behaviour_get_enabled_m753527255(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_rotationSpeed_3();
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_DrawRotateArrowGizmo_m3937198024(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BalloonScript::.ctor()
extern "C" IL2CPP_METHOD_ATTR void BalloonScript__ctor_m3424785481 (BalloonScript_t4017429074 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BalloonScript::Awake()
extern "C" IL2CPP_METHOD_ATTR void BalloonScript_Awake_m536274945 (BalloonScript_t4017429074 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BalloonScript_Awake_m536274945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3704657025 * L_0 = Component_GetComponent_TisRectTransform_t3704657025_m3396022872(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var);
		__this->set_rectTransform_5(L_0);
		return;
	}
}
// System.Void BalloonScript::Setup(System.String,System.Boolean,UnityEngine.KeyCode,System.Single,UnityEngine.Color,UnityEngine.Color,UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void BalloonScript_Setup_m3797033188 (BalloonScript_t4017429074 * __this, String_t* ___dialogueString0, bool ____isUsingButton1, int32_t ____buttonUsed2, float ____time3, Color_t2555686324  ___backgroundC4, Color_t2555686324  ___textC5, Transform_t3600365921 * ____targetObj6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BalloonScript_Setup_m3797033188_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ____isUsingButton1;
		__this->set_isUsingButton_6(L_0);
		int32_t L_1 = ____buttonUsed2;
		__this->set_buttonUsed_7(L_1);
		Transform_t3600365921 * L_2 = ____targetObj6;
		__this->set_targetObj_8(L_2);
		float L_3 = ____time3;
		__this->set_duration_10(L_3);
		Image_t2670269651 * L_4 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		Color_t2555686324  L_5 = ___backgroundC4;
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_4, L_5);
		Text_t1901882714 * L_6 = __this->get_dialogueText_2();
		String_t* L_7 = ___dialogueString0;
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_7);
		Text_t1901882714 * L_8 = __this->get_dialogueText_2();
		Color_t2555686324  L_9 = ___textC5;
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_8, L_9);
		bool L_10 = __this->get_isUsingButton_6();
		if (!L_10)
		{
			goto IL_0087;
		}
	}
	{
		Text_t1901882714 * L_11 = __this->get_buttonText_3();
		int32_t* L_12 = __this->get_address_of_buttonUsed_7();
		RuntimeObject * L_13 = Box(KeyCode_t2599294277_il2cpp_TypeInfo_var, L_12);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
		*L_12 = *(int32_t*)UnBox(L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2700639336, L_14, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_15);
		Text_t1901882714 * L_16 = __this->get_buttonText_3();
		Color_t2555686324  L_17 = ___textC5;
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_16, L_17);
		goto IL_00a3;
	}

IL_0087:
	{
		Text_t1901882714 * L_18 = __this->get_buttonText_3();
		GameObject_t1113636619 * L_19 = Component_get_gameObject_m442555142(L_18, /*hidden argument*/NULL);
		GameObject_SetActive_m796801857(L_19, (bool)0, /*hidden argument*/NULL);
		float L_20 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startTime_9(L_20);
	}

IL_00a3:
	{
		Transform_t3600365921 * L_21 = __this->get_targetObj_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_21, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00f2;
		}
	}
	{
		RectTransform_t3704657025 * L_23 = __this->get_rectTransform_5();
		Vector2_t2156229523  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector2__ctor_m3970636864((&L_24), (0.5f), (0.5f), /*hidden argument*/NULL);
		RectTransform_set_pivot_m909387058(L_23, L_24, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_25 = __this->get_rectTransform_5();
		Camera_t4157153871 * L_26 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_27 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_28 = RectTransformUtility_WorldToScreenPoint_m3914148572(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_29 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_25, L_29, /*hidden argument*/NULL);
		goto IL_0112;
	}

IL_00f2:
	{
		RectTransform_t3704657025 * L_30 = __this->get_rectTransform_5();
		Vector2_t2156229523  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector2__ctor_m3970636864((&L_31), (0.5f), (0.0f), /*hidden argument*/NULL);
		RectTransform_set_pivot_m909387058(L_30, L_31, /*hidden argument*/NULL);
		BalloonScript_FollowTarget_m625056729(__this, /*hidden argument*/NULL);
	}

IL_0112:
	{
		return;
	}
}
// System.Void BalloonScript::Update()
extern "C" IL2CPP_METHOD_ATTR void BalloonScript_Update_m1258479472 (BalloonScript_t4017429074 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BalloonScript_Update_m1258479472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = __this->get_targetObj_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		BalloonScript_FollowTarget_m625056729(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		bool L_2 = __this->get_isUsingButton_6();
		if (!L_2)
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_3 = __this->get_buttonUsed_7();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetKeyUp_m2808015270(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003d;
		}
	}
	{
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_003d:
	{
		goto IL_0064;
	}

IL_0042:
	{
		float L_6 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = __this->get_startTime_9();
		float L_8 = __this->get_duration_10();
		if ((!(((float)L_6) >= ((float)((float)il2cpp_codegen_add((float)L_7, (float)L_8))))))
		{
			goto IL_0064;
		}
	}
	{
		GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void BalloonScript::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void BalloonScript_OnDestroy_m337934040 (BalloonScript_t4017429074 * __this, const RuntimeMethod* method)
{
	{
		UnityAction_t3245792599 * L_0 = __this->get_BalloonDestroyed_4();
		UnityAction_Invoke_m893829196(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BalloonScript::FollowTarget()
extern "C" IL2CPP_METHOD_ATTR void BalloonScript_FollowTarget_m625056729 (BalloonScript_t4017429074 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BalloonScript_FollowTarget_m625056729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	SpriteRenderer_t3235626157 * V_1 = NULL;
	Bounds_t2266837910  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Transform_t3600365921 * L_0 = __this->get_targetObj_8();
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Transform_t3600365921 * L_2 = __this->get_targetObj_8();
		SpriteRenderer_t3235626157 * L_3 = Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502(L_2, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502_RuntimeMethod_var);
		V_1 = L_3;
		SpriteRenderer_t3235626157 * L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		Vector3_t3722313464 * L_6 = (&V_0);
		float L_7 = L_6->get_y_2();
		SpriteRenderer_t3235626157 * L_8 = V_1;
		Bounds_t2266837910  L_9 = Renderer_get_bounds_m1803204000(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		Vector3_t3722313464  L_10 = Bounds_get_size_m1178783246((&V_2), /*hidden argument*/NULL);
		V_3 = L_10;
		float L_11 = (&V_3)->get_y_2();
		L_6->set_y_2(((float)il2cpp_codegen_add((float)L_7, (float)L_11)));
		goto IL_0068;
	}

IL_004d:
	{
		Transform_t3600365921 * L_12 = __this->get_targetObj_8();
		Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		float L_14 = (&V_4)->get_y_2();
		(&V_0)->set_y_2(L_14);
	}

IL_0068:
	{
		RectTransform_t3704657025 * L_15 = __this->get_rectTransform_5();
		Camera_t4157153871 * L_16 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_18 = RectTransformUtility_WorldToScreenPoint_m3914148572(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_19 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_15, L_19, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BulletAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void BulletAttribute__ctor_m241866660 (BulletAttribute_t588811475 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BulletAttribute::Reset()
extern "C" IL2CPP_METHOD_ATTR void BulletAttribute_Reset_m495814584 (BulletAttribute_t588811475 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BulletAttribute_Reset_m495814584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_Collider2DDialogWindow_m3028026175(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CameraFollow::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CameraFollow__ctor_m2357661391 (CameraFollow_t129522575 * __this, const RuntimeMethod* method)
{
	{
		__this->set_left_4((-5.0f));
		__this->set_right_5((5.0f));
		__this->set_bottom_6((-5.0f));
		__this->set_top_7((5.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraFollow::Awake()
extern "C" IL2CPP_METHOD_ATTR void CameraFollow_Awake_m1034099068 (CameraFollow_t129522575 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraFollow_Awake_m1034099068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t4157153871 * L_0 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		__this->set__camera_9(L_0);
		return;
	}
}
// System.Void CameraFollow::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void CameraFollow_FixedUpdate_m1628278094 (CameraFollow_t129522575 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraFollow_FixedUpdate_m1628278094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = __this->get_target_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = __this->get_target_2();
		Vector3_t3722313464  L_5 = Transform_get_position_m36019626(L_4, /*hidden argument*/NULL);
		float L_6 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_7 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_3, L_5, ((float)il2cpp_codegen_multiply((float)L_6, (float)(10.0f))), /*hidden argument*/NULL);
		__this->set_lerpedPosition_8(L_7);
		Vector3_t3722313464 * L_8 = __this->get_address_of_lerpedPosition_8();
		L_8->set_z_3((-10.0f));
	}

IL_004d:
	{
		return;
	}
}
// System.Void CameraFollow::LateUpdate()
extern "C" IL2CPP_METHOD_ATTR void CameraFollow_LateUpdate_m2490584337 (CameraFollow_t129522575 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraFollow_LateUpdate_m2490584337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Transform_t3600365921 * L_0 = __this->get_target_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0190;
		}
	}
	{
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = __this->get_lerpedPosition_8();
		Transform_set_position_m3387557959(L_2, L_3, /*hidden argument*/NULL);
		bool L_4 = __this->get_limitBounds_3();
		if (!L_4)
		{
			goto IL_0190;
		}
	}
	{
		Camera_t4157153871 * L_5 = __this->get__camera_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Camera_ScreenToWorldPoint_m3978588570(L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Camera_t4157153871 * L_8 = __this->get__camera_9();
		Camera_t4157153871 * L_9 = __this->get__camera_9();
		int32_t L_10 = Camera_get_pixelWidth_m1110053668(L_9, /*hidden argument*/NULL);
		Camera_t4157153871 * L_11 = __this->get__camera_9();
		int32_t L_12 = Camera_get_pixelHeight_m722276884(L_11, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m1719387948((&L_13), (((float)((float)L_10))), (((float)((float)L_12))), /*hidden argument*/NULL);
		Vector3_t3722313464  L_14 = Camera_ScreenToWorldPoint_m3978588570(L_8, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = (&V_1)->get_x_1();
		float L_16 = (&V_0)->get_x_1();
		float L_17 = (&V_1)->get_y_2();
		float L_18 = (&V_0)->get_y_2();
		Vector2__ctor_m3970636864((&V_2), ((float)il2cpp_codegen_subtract((float)L_15, (float)L_16)), ((float)il2cpp_codegen_subtract((float)L_17, (float)L_18)), /*hidden argument*/NULL);
		Transform_t3600365921 * L_19 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_20 = Transform_get_position_m36019626(L_19, /*hidden argument*/NULL);
		V_3 = L_20;
		float L_21 = (&V_3)->get_x_1();
		float L_22 = __this->get_right_5();
		float L_23 = (&V_2)->get_x_0();
		if ((!(((float)L_21) > ((float)((float)il2cpp_codegen_subtract((float)L_22, (float)((float)((float)L_23/(float)(2.0f)))))))))
		{
			goto IL_00d3;
		}
	}
	{
		float L_24 = __this->get_right_5();
		float L_25 = (&V_2)->get_x_0();
		(&V_3)->set_x_1(((float)il2cpp_codegen_subtract((float)L_24, (float)((float)((float)L_25/(float)(2.0f))))));
	}

IL_00d3:
	{
		float L_26 = (&V_3)->get_x_1();
		float L_27 = __this->get_left_4();
		float L_28 = (&V_2)->get_x_0();
		if ((!(((float)L_26) < ((float)((float)il2cpp_codegen_add((float)L_27, (float)((float)((float)L_28/(float)(2.0f)))))))))
		{
			goto IL_010e;
		}
	}
	{
		float L_29 = __this->get_left_4();
		float L_30 = (&V_2)->get_x_0();
		(&V_3)->set_x_1(((float)il2cpp_codegen_add((float)L_29, (float)((float)((float)L_30/(float)(2.0f))))));
	}

IL_010e:
	{
		float L_31 = (&V_3)->get_y_2();
		float L_32 = __this->get_top_7();
		float L_33 = (&V_2)->get_y_1();
		if ((!(((float)L_31) > ((float)((float)il2cpp_codegen_subtract((float)L_32, (float)((float)((float)L_33/(float)(2.0f)))))))))
		{
			goto IL_0149;
		}
	}
	{
		float L_34 = __this->get_top_7();
		float L_35 = (&V_2)->get_y_1();
		(&V_3)->set_y_2(((float)il2cpp_codegen_subtract((float)L_34, (float)((float)((float)L_35/(float)(2.0f))))));
	}

IL_0149:
	{
		float L_36 = (&V_3)->get_y_2();
		float L_37 = __this->get_bottom_6();
		float L_38 = (&V_2)->get_y_1();
		if ((!(((float)L_36) < ((float)((float)il2cpp_codegen_add((float)L_37, (float)((float)((float)L_38/(float)(2.0f)))))))))
		{
			goto IL_0184;
		}
	}
	{
		float L_39 = __this->get_bottom_6();
		float L_40 = (&V_2)->get_y_1();
		(&V_3)->set_y_2(((float)il2cpp_codegen_add((float)L_39, (float)((float)((float)L_40/(float)(2.0f))))));
	}

IL_0184:
	{
		Transform_t3600365921 * L_41 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_42 = V_3;
		Transform_set_position_m3387557959(L_41, L_42, /*hidden argument*/NULL);
	}

IL_0190:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CollectableAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CollectableAttribute__ctor_m3920486330 (CollectableAttribute_t113388083 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CollectableAttribute::Start()
extern "C" IL2CPP_METHOD_ATTR void CollectableAttribute_Start_m174462918 (CollectableAttribute_t113388083 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void CollectableAttribute::Reset()
extern "C" IL2CPP_METHOD_ATTR void CollectableAttribute_Reset_m1687984457 (CollectableAttribute_t113388083 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CollectableAttribute_Reset_m1687984457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_Collider2DDialogWindow_m3028026175(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CollectableAttribute::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void CollectableAttribute_OnTriggerEnter2D_m2028075393 (CollectableAttribute_t113388083 * __this, Collider2D_t2806799626 * ___otherCollider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CollectableAttribute_OnTriggerEnter2D_m2028075393_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Collider2D_t2806799626 * L_0 = ___otherCollider0;
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		String_t* L_2 = GameObject_get_tag_m3951609671(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_3, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002c;
		}
	}
	{
		String_t* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m920492651(NULL /*static, unused*/, L_5, _stringLiteral259609454, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0048;
		}
	}

IL_002c:
	{
		UIScript_t1195624422 * L_7 = __this->get_userInterface_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_7, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003d;
		}
	}

IL_003d:
	{
		GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ConditionArea::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ConditionArea__ctor_m1241105313 (ConditionArea_t3498028374 * __this, const RuntimeMethod* method)
{
	{
		__this->set_frequency_9((1.0f));
		ConditionBase__ctor_m238416164(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConditionArea::Start()
extern "C" IL2CPP_METHOD_ATTR void ConditionArea_Start_m1719519405 (ConditionArea_t3498028374 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_frequency_9();
		__this->set_lastTimeTriggerStayCalled_11(((-L_0)));
		return;
	}
}
// System.Void ConditionArea::Reset()
extern "C" IL2CPP_METHOD_ATTR void ConditionArea_Reset_m4059968981 (ConditionArea_t3498028374 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConditionArea_Reset_m4059968981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_Collider2DDialogWindow_m3028026175(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConditionArea::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void ConditionArea_OnTriggerEnter2D_m270726032 (ConditionArea_t3498028374 * __this, Collider2D_t2806799626 * ___otherCollider0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_eventType_10();
		if (L_0)
		{
			goto IL_0033;
		}
	}
	{
		Collider2D_t2806799626 * L_1 = ___otherCollider0;
		String_t* L_2 = ((ConditionBase_t3399778919 *)__this)->get_filterTag_8();
		bool L_3 = Component_CompareTag_m1328479619(L_1, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		bool L_4 = ((ConditionBase_t3399778919 *)__this)->get_filterByTag_7();
		if (L_4)
		{
			goto IL_0033;
		}
	}

IL_0027:
	{
		Collider2D_t2806799626 * L_5 = ___otherCollider0;
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m442555142(L_5, /*hidden argument*/NULL);
		ConditionBase_ExecuteAllActions_m3297109267(__this, L_6, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void ConditionArea::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void ConditionArea_OnTriggerStay2D_m2792604318 (ConditionArea_t3498028374 * __this, Collider2D_t2806799626 * ___otherCollider0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_eventType_10();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0056;
		}
	}
	{
		float L_1 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_lastTimeTriggerStayCalled_11();
		float L_3 = __this->get_frequency_9();
		if ((!(((float)L_1) >= ((float)((float)il2cpp_codegen_add((float)L_2, (float)L_3))))))
		{
			goto IL_0056;
		}
	}
	{
		Collider2D_t2806799626 * L_4 = ___otherCollider0;
		String_t* L_5 = ((ConditionBase_t3399778919 *)__this)->get_filterTag_8();
		bool L_6 = Component_CompareTag_m1328479619(L_4, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_003f;
		}
	}
	{
		bool L_7 = ((ConditionBase_t3399778919 *)__this)->get_filterByTag_7();
		if (L_7)
		{
			goto IL_0056;
		}
	}

IL_003f:
	{
		Collider2D_t2806799626 * L_8 = ___otherCollider0;
		GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(L_8, /*hidden argument*/NULL);
		ConditionBase_ExecuteAllActions_m3297109267(__this, L_9, /*hidden argument*/NULL);
		float L_10 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastTimeTriggerStayCalled_11(L_10);
	}

IL_0056:
	{
		return;
	}
}
// System.Void ConditionArea::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void ConditionArea_OnTriggerExit2D_m2330579351 (ConditionArea_t3498028374 * __this, Collider2D_t2806799626 * ___otherCollider0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_eventType_10();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0034;
		}
	}
	{
		Collider2D_t2806799626 * L_1 = ___otherCollider0;
		String_t* L_2 = ((ConditionBase_t3399778919 *)__this)->get_filterTag_8();
		bool L_3 = Component_CompareTag_m1328479619(L_1, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0028;
		}
	}
	{
		bool L_4 = ((ConditionBase_t3399778919 *)__this)->get_filterByTag_7();
		if (L_4)
		{
			goto IL_0034;
		}
	}

IL_0028:
	{
		Collider2D_t2806799626 * L_5 = ___otherCollider0;
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m442555142(L_5, /*hidden argument*/NULL);
		ConditionBase_ExecuteAllActions_m3297109267(__this, L_6, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ConditionBase::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ConditionBase__ctor_m238416164 (ConditionBase_t3399778919 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConditionBase__ctor_m238416164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t547830642 * L_0 = (List_1_t547830642 *)il2cpp_codegen_object_new(List_1_t547830642_il2cpp_TypeInfo_var);
		List_1__ctor_m3594099166(L_0, /*hidden argument*/List_1__ctor_m3594099166_RuntimeMethod_var);
		__this->set_actions_2(L_0);
		__this->set_filterTag_8(_stringLiteral2261822918);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConditionBase::ExecuteAllActions(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void ConditionBase_ExecuteAllActions_m3297109267 (ConditionBase_t3399778919 * __this, GameObject_t1113636619 * ___dataObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConditionBase_ExecuteAllActions_m3297109267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Action_t3370723196 * V_1 = NULL;
	Enumerator_t2437074519  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_happenOnlyOnce_5();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		bool L_1 = __this->get_alreadyHappened_6();
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		List_1_t547830642 * L_2 = __this->get_actions_2();
		Enumerator_t2437074519  L_3 = List_1_GetEnumerator_m2023103186(L_2, /*hidden argument*/List_1_GetEnumerator_m2023103186_RuntimeMethod_var);
		V_2 = L_3;
	}

IL_0023:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0059;
		}

IL_0028:
		{
			Action_t3370723196 * L_4 = Enumerator_get_Current_m263105171((&V_2), /*hidden argument*/Enumerator_get_Current_m263105171_RuntimeMethod_var);
			V_1 = L_4;
			Action_t3370723196 * L_5 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			bool L_6 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_5, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0059;
			}
		}

IL_003c:
		{
			Action_t3370723196 * L_7 = V_1;
			GameObject_t1113636619 * L_8 = ___dataObject0;
			bool L_9 = VirtFuncInvoker1< bool, GameObject_t1113636619 * >::Invoke(4 /* System.Boolean Action::ExecuteAction(UnityEngine.GameObject) */, L_7, L_8);
			V_0 = L_9;
			bool L_10 = V_0;
			if (L_10)
			{
				goto IL_0059;
			}
		}

IL_004a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral1632036919, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x95, FINALLY_006a);
		}

IL_0059:
		{
			bool L_11 = Enumerator_MoveNext_m4019089914((&V_2), /*hidden argument*/Enumerator_MoveNext_m4019089914_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0028;
			}
		}

IL_0065:
		{
			IL2CPP_LEAVE(0x78, FINALLY_006a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_006a;
	}

FINALLY_006a:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1654570958((&V_2), /*hidden argument*/Enumerator_Dispose_m1654570958_RuntimeMethod_var);
		IL2CPP_END_FINALLY(106)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(106)
	{
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_JUMP_TBL(0x78, IL_0078)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0078:
	{
		bool L_12 = __this->get_useCustomActions_3();
		if (!L_12)
		{
			goto IL_008e;
		}
	}
	{
		UnityEvent_t2581268647 * L_13 = __this->get_customActions_4();
		UnityEvent_Invoke_m3065672636(L_13, /*hidden argument*/NULL);
	}

IL_008e:
	{
		__this->set_alreadyHappened_6((bool)1);
	}

IL_0095:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ConditionCollision::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ConditionCollision__ctor_m2157866107 (ConditionCollision_t1635305638 * __this, const RuntimeMethod* method)
{
	{
		ConditionBase__ctor_m238416164(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConditionCollision::Reset()
extern "C" IL2CPP_METHOD_ATTR void ConditionCollision_Reset_m4122520466 (ConditionCollision_t1635305638 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConditionCollision_Reset_m4122520466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_Collider2DDialogWindow_m3028026175(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConditionCollision::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void ConditionCollision_OnCollisionEnter2D_m2164275008 (ConditionCollision_t1635305638 * __this, Collision2D_t2842956331 * ___collision0, const RuntimeMethod* method)
{
	{
		Collision2D_t2842956331 * L_0 = ___collision0;
		Collider2D_t2806799626 * L_1 = Collision2D_get_collider_m4087612183(L_0, /*hidden argument*/NULL);
		String_t* L_2 = ((ConditionBase_t3399778919 *)__this)->get_filterTag_8();
		bool L_3 = Component_CompareTag_m1328479619(L_1, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		bool L_4 = ((ConditionBase_t3399778919 *)__this)->get_filterByTag_7();
		if (L_4)
		{
			goto IL_002d;
		}
	}

IL_0021:
	{
		Collision2D_t2842956331 * L_5 = ___collision0;
		GameObject_t1113636619 * L_6 = Collision2D_get_gameObject_m1443495885(L_5, /*hidden argument*/NULL);
		ConditionBase_ExecuteAllActions_m3297109267(__this, L_6, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ConditionKeyPress::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ConditionKeyPress__ctor_m2599461838 (ConditionKeyPress_t1643004734 * __this, const RuntimeMethod* method)
{
	{
		__this->set_keyToPress_9(((int32_t)32));
		__this->set_frequency_11((0.5f));
		ConditionBase__ctor_m238416164(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConditionKeyPress::Start()
extern "C" IL2CPP_METHOD_ATTR void ConditionKeyPress_Start_m4003518448 (ConditionKeyPress_t1643004734 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_frequency_11();
		__this->set_timeLastEventFired_12(((-L_0)));
		return;
	}
}
// System.Void ConditionKeyPress::Update()
extern "C" IL2CPP_METHOD_ATTR void ConditionKeyPress_Update_m856179128 (ConditionKeyPress_t1643004734 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConditionKeyPress_Update_m856179128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_eventType_10();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_0058;
		}
	}
	{
		goto IL_0096;
	}

IL_0020:
	{
		int32_t L_4 = __this->get_keyToPress_9();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_5 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		ConditionBase_ExecuteAllActions_m3297109267(__this, (GameObject_t1113636619 *)NULL, /*hidden argument*/NULL);
	}

IL_0037:
	{
		goto IL_0096;
	}

IL_003c:
	{
		int32_t L_6 = __this->get_keyToPress_9();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_7 = Input_GetKeyUp_m2808015270(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0053;
		}
	}
	{
		ConditionBase_ExecuteAllActions_m3297109267(__this, (GameObject_t1113636619 *)NULL, /*hidden argument*/NULL);
	}

IL_0053:
	{
		goto IL_0096;
	}

IL_0058:
	{
		float L_8 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = __this->get_timeLastEventFired_12();
		float L_10 = __this->get_frequency_11();
		if ((!(((float)L_8) >= ((float)((float)il2cpp_codegen_add((float)L_9, (float)L_10))))))
		{
			goto IL_0091;
		}
	}
	{
		int32_t L_11 = __this->get_keyToPress_9();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_12 = Input_GetKey_m3736388334(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0091;
		}
	}
	{
		ConditionBase_ExecuteAllActions_m3297109267(__this, (GameObject_t1113636619 *)NULL, /*hidden argument*/NULL);
		float L_13 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timeLastEventFired_12(L_13);
	}

IL_0091:
	{
		goto IL_0096;
	}

IL_0096:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ConditionRepeat::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ConditionRepeat__ctor_m1684787843 (ConditionRepeat_t1215900890 * __this, const RuntimeMethod* method)
{
	{
		__this->set_frequency_10((1.0f));
		ConditionBase__ctor_m238416164(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConditionRepeat::Start()
extern "C" IL2CPP_METHOD_ATTR void ConditionRepeat_Start_m972960755 (ConditionRepeat_t1215900890 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_initialDelay_9();
		float L_1 = __this->get_frequency_10();
		__this->set_timeLastEventFired_11(((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)));
		return;
	}
}
// System.Void ConditionRepeat::Update()
extern "C" IL2CPP_METHOD_ATTR void ConditionRepeat_Update_m1218833902 (ConditionRepeat_t1215900890 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_timeLastEventFired_11();
		float L_2 = __this->get_frequency_10();
		if ((!(((float)L_0) >= ((float)((float)il2cpp_codegen_add((float)L_1, (float)L_2))))))
		{
			goto IL_0029;
		}
	}
	{
		ConditionBase_ExecuteAllActions_m3297109267(__this, (GameObject_t1113636619 *)NULL, /*hidden argument*/NULL);
		float L_3 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timeLastEventFired_11(L_3);
	}

IL_0029:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ConsumeResourceAction::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ConsumeResourceAction__ctor_m3054392568 (ConsumeResourceAction_t2832324500 * __this, const RuntimeMethod* method)
{
	{
		__this->set_amountNeeded_3(1);
		Action__ctor_m2761626621(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ConsumeResourceAction::Start()
extern "C" IL2CPP_METHOD_ATTR void ConsumeResourceAction_Start_m1402206288 (ConsumeResourceAction_t2832324500 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsumeResourceAction_Start_m1402206288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		UIScript_t1195624422 * L_0 = Object_FindObjectOfType_TisUIScript_t1195624422_m1742595665(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisUIScript_t1195624422_m1742595665_RuntimeMethod_var);
		__this->set_userInterface_4(L_0);
		return;
	}
}
// System.Boolean ConsumeResourceAction::ExecuteAction(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR bool ConsumeResourceAction_ExecuteAction_m3605170454 (ConsumeResourceAction_t2832324500 * __this, GameObject_t1113636619 * ___dataObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsumeResourceAction_ExecuteAction_m3605170454_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		UIScript_t1195624422 * L_0 = __this->get_userInterface_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0048;
		}
	}
	{
		UIScript_t1195624422 * L_2 = __this->get_userInterface_4();
		int32_t L_3 = __this->get_checkFor_2();
		int32_t L_4 = __this->get_amountNeeded_3();
		bool L_5 = UIScript_CheckIfHasResources_m685094672(L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0046;
		}
	}
	{
		UIScript_t1195624422 * L_7 = __this->get_userInterface_4();
		int32_t L_8 = __this->get_checkFor_2();
		int32_t L_9 = __this->get_amountNeeded_3();
		UIScript_ConsumeResource_m3280961331(L_7, L_8, L_9, /*hidden argument*/NULL);
	}

IL_0046:
	{
		bool L_10 = V_0;
		return L_10;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral920216033, /*hidden argument*/NULL);
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CreateObjectAction::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CreateObjectAction__ctor_m3019702124 (CreateObjectAction_t2026806364 * __this, const RuntimeMethod* method)
{
	{
		Action__ctor_m2761626621(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean CreateObjectAction::ExecuteAction(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR bool CreateObjectAction_ExecuteAction_m3938939490 (CreateObjectAction_t2026806364 * __this, GameObject_t1113636619 * ___dataObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CreateObjectAction_ExecuteAction_m3938939490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		GameObject_t1113636619 * L_0 = __this->get_prefabToCreate_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005e;
		}
	}
	{
		GameObject_t1113636619 * L_2 = __this->get_prefabToCreate_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_3 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		V_0 = L_3;
		Vector2_t2156229523  L_4 = __this->get_newPosition_3();
		V_1 = L_4;
		bool L_5 = __this->get_relativeToThisObject_4();
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_8 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Vector2_t2156229523  L_9 = __this->get_newPosition_3();
		Vector2_t2156229523  L_10 = Vector2_op_Addition_m800700293(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
	}

IL_004b:
	{
		GameObject_t1113636619 * L_11 = V_0;
		Transform_t3600365921 * L_12 = GameObject_get_transform_m1369836730(L_11, /*hidden argument*/NULL);
		Vector2_t2156229523  L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_14 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_12, L_14, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_005e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral2979139915, /*hidden argument*/NULL);
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DestroyAction::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DestroyAction__ctor_m4221538832 (DestroyAction_t1816662878 * __this, const RuntimeMethod* method)
{
	{
		__this->set_target_2(1);
		Action__ctor_m2761626621(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DestroyAction::ExecuteAction(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR bool DestroyAction_ExecuteAction_m2632851313 (DestroyAction_t1816662878 * __this, GameObject_t1113636619 * ___otherObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DestroyAction_ExecuteAction_m2632851313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	Transform_t3600365921 * G_B6_0 = NULL;
	Transform_t3600365921 * G_B5_0 = NULL;
	Vector3_t3722313464  G_B7_0;
	memset(&G_B7_0, 0, sizeof(G_B7_0));
	Transform_t3600365921 * G_B7_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = __this->get_deathEffect_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006d;
		}
	}
	{
		GameObject_t1113636619 * L_2 = __this->get_deathEffect_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_3 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		V_0 = L_3;
		GameObject_t1113636619 * L_4 = ___otherObject0;
		bool L_5 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0039;
		}
	}
	{
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		goto IL_0044;
	}

IL_0039:
	{
		GameObject_t1113636619 * L_8 = ___otherObject0;
		Transform_t3600365921 * L_9 = GameObject_get_transform_m1369836730(L_8, /*hidden argument*/NULL);
		Vector3_t3722313464  L_10 = Transform_get_position_m36019626(L_9, /*hidden argument*/NULL);
		G_B4_0 = L_10;
	}

IL_0044:
	{
		V_1 = G_B4_0;
		GameObject_t1113636619 * L_11 = V_0;
		Transform_t3600365921 * L_12 = GameObject_get_transform_m1369836730(L_11, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_target_2();
		G_B5_0 = L_12;
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			G_B6_0 = L_12;
			goto IL_005d;
		}
	}
	{
		Vector3_t3722313464  L_14 = V_1;
		G_B7_0 = L_14;
		G_B7_1 = G_B5_0;
		goto IL_0068;
	}

IL_005d:
	{
		Transform_t3600365921 * L_15 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_16 = Transform_get_position_m36019626(L_15, /*hidden argument*/NULL);
		G_B7_0 = L_16;
		G_B7_1 = G_B6_0;
	}

IL_0068:
	{
		Transform_set_position_m3387557959(G_B7_1, G_B7_0, /*hidden argument*/NULL);
	}

IL_006d:
	{
		int32_t L_17 = __this->get_target_2();
		if ((!(((uint32_t)L_17) == ((uint32_t)1))))
		{
			goto IL_0090;
		}
	}
	{
		GameObject_t1113636619 * L_18 = ___otherObject0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_18, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_008b;
		}
	}
	{
		GameObject_t1113636619 * L_20 = ___otherObject0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
	}

IL_008b:
	{
		goto IL_009b;
	}

IL_0090:
	{
		GameObject_t1113636619 * L_21 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
	}

IL_009b:
	{
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DestroyForPointsAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DestroyForPointsAttribute__ctor_m3035654550 (DestroyForPointsAttribute_t2602039747 * __this, const RuntimeMethod* method)
{
	{
		__this->set_pointsWorth_2(1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyForPointsAttribute::Start()
extern "C" IL2CPP_METHOD_ATTR void DestroyForPointsAttribute_Start_m2308896846 (DestroyForPointsAttribute_t2602039747 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DestroyForPointsAttribute_Start_m2308896846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		UIScript_t1195624422 * L_0 = Object_FindObjectOfType_TisUIScript_t1195624422_m1742595665(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisUIScript_t1195624422_m1742595665_RuntimeMethod_var);
		__this->set_userInterface_3(L_0);
		return;
	}
}
// System.Void DestroyForPointsAttribute::Reset()
extern "C" IL2CPP_METHOD_ATTR void DestroyForPointsAttribute_Reset_m2910058454 (DestroyForPointsAttribute_t2602039747 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DestroyForPointsAttribute_Reset_m2910058454_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_Collider2DDialogWindow_m3028026175(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyForPointsAttribute::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void DestroyForPointsAttribute_OnCollisionEnter2D_m2037477202 (DestroyForPointsAttribute_t2602039747 * __this, Collision2D_t2842956331 * ___collisionData0, const RuntimeMethod* method)
{
	{
		Collision2D_t2842956331 * L_0 = ___collisionData0;
		Collider2D_t2806799626 * L_1 = Collision2D_get_collider_m4087612183(L_0, /*hidden argument*/NULL);
		DestroyForPointsAttribute_OnTriggerEnter2D_m3805996196(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyForPointsAttribute::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void DestroyForPointsAttribute_OnTriggerEnter2D_m3805996196 (DestroyForPointsAttribute_t2602039747 * __this, Collider2D_t2806799626 * ___collisionData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DestroyForPointsAttribute_OnTriggerEnter2D_m3805996196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BulletAttribute_t588811475 * V_0 = NULL;
	{
		Collider2D_t2806799626 * L_0 = ___collisionData0;
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		bool L_2 = GameObject_CompareTag_m3144439756(L_1, _stringLiteral37315637, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_007e;
		}
	}
	{
		UIScript_t1195624422 * L_3 = __this->get_userInterface_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0069;
		}
	}
	{
		Collider2D_t2806799626 * L_5 = ___collisionData0;
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m442555142(L_5, /*hidden argument*/NULL);
		BulletAttribute_t588811475 * L_7 = GameObject_GetComponent_TisBulletAttribute_t588811475_m587253523(L_6, /*hidden argument*/GameObject_GetComponent_TisBulletAttribute_t588811475_m587253523_RuntimeMethod_var);
		V_0 = L_7;
		BulletAttribute_t588811475 * L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_8, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005a;
		}
	}
	{
		UIScript_t1195624422 * L_10 = __this->get_userInterface_3();
		BulletAttribute_t588811475 * L_11 = V_0;
		int32_t L_12 = L_11->get_playerId_2();
		int32_t L_13 = __this->get_pointsWorth_2();
		UIScript_AddPoints_m1714476938(L_10, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0064;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3499710428, /*hidden argument*/NULL);
	}

IL_0064:
	{
		goto IL_0073;
	}

IL_0069:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2632304242, /*hidden argument*/NULL);
	}

IL_0073:
	{
		GameObject_t1113636619 * L_14 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_007e:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DialogueBalloonAction::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DialogueBalloonAction__ctor_m398991252 (DialogueBalloonAction_t3348434864 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DialogueBalloonAction__ctor_m398991252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_textToDisplay_2(_stringLiteral2734162053);
		Color32_t2600501292  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color32__ctor_m4150508762((&L_0), (uint8_t)((int32_t)113), (uint8_t)((int32_t)132), (uint8_t)((int32_t)146), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		Color_t2555686324  L_1 = Color32_op_Implicit_m213813866(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_backgroundColor_3(L_1);
		Color_t2555686324  L_2 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_textColor_4(L_2);
		__this->set_disappearMode_6(1);
		__this->set_timeToDisappear_7((2.0f));
		__this->set_keyToPress_8(((int32_t)13));
		Action__ctor_m2761626621(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DialogueBalloonAction::ExecuteAction(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR bool DialogueBalloonAction_ExecuteAction_m1502107245 (DialogueBalloonAction_t3348434864 * __this, GameObject_t1113636619 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DialogueBalloonAction_ExecuteAction_m1502107245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DialogueSystem_t292220827 * V_0 = NULL;
	{
		bool L_0 = __this->get_balloonIsActive_11();
		if (L_0)
		{
			goto IL_009f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		DialogueSystem_t292220827 * L_1 = Object_FindObjectOfType_TisDialogueSystem_t292220827_m1864039531(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisDialogueSystem_t292220827_m1864039531_RuntimeMethod_var);
		V_0 = L_1;
		DialogueSystem_t292220827 * L_2 = V_0;
		bool L_3 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral2002083477, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0029:
	{
		DialogueSystem_t292220827 * L_4 = V_0;
		String_t* L_5 = __this->get_textToDisplay_2();
		int32_t L_6 = __this->get_disappearMode_6();
		int32_t L_7 = __this->get_keyToPress_8();
		float L_8 = __this->get_timeToDisappear_7();
		Color_t2555686324  L_9 = __this->get_backgroundColor_3();
		Color_t2555686324  L_10 = __this->get_textColor_4();
		Transform_t3600365921 * L_11 = __this->get_targetObject_5();
		BalloonScript_t4017429074 * L_12 = DialogueSystem_CreateBalloon_m1415492347(L_4, L_5, (bool)((((int32_t)L_6) == ((int32_t)1))? 1 : 0), L_7, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		__this->set_b_10(L_12);
		BalloonScript_t4017429074 * L_13 = __this->get_b_10();
		BalloonScript_t4017429074 * L_14 = L_13;
		UnityAction_t3245792599 * L_15 = L_14->get_BalloonDestroyed_4();
		intptr_t L_16 = (intptr_t)DialogueBalloonAction_OnBalloonDestroyed_m1884765915_RuntimeMethod_var;
		UnityAction_t3245792599 * L_17 = (UnityAction_t3245792599 *)il2cpp_codegen_object_new(UnityAction_t3245792599_il2cpp_TypeInfo_var);
		UnityAction__ctor_m772160306(L_17, __this, L_16, /*hidden argument*/NULL);
		Delegate_t1188392813 * L_18 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		L_14->set_BalloonDestroyed_4(((UnityAction_t3245792599 *)CastclassSealed((RuntimeObject*)L_18, UnityAction_t3245792599_il2cpp_TypeInfo_var)));
		__this->set_balloonIsActive_11((bool)1);
		RuntimeObject* L_19 = DialogueBalloonAction_WaitForBallonDestroyed_m1668273814(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_19, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_009f:
	{
		return (bool)0;
	}
}
// System.Collections.IEnumerator DialogueBalloonAction::WaitForBallonDestroyed()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* DialogueBalloonAction_WaitForBallonDestroyed_m1668273814 (DialogueBalloonAction_t3348434864 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DialogueBalloonAction_WaitForBallonDestroyed_m1668273814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479 * V_0 = NULL;
	{
		U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479 * L_0 = (U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479 *)il2cpp_codegen_object_new(U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479_il2cpp_TypeInfo_var);
		U3CWaitForBallonDestroyedU3Ec__Iterator0__ctor_m108888311(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479 * L_2 = V_0;
		return L_2;
	}
}
// System.Void DialogueBalloonAction::OnBalloonDestroyed()
extern "C" IL2CPP_METHOD_ATTR void DialogueBalloonAction_OnBalloonDestroyed_m1884765915 (DialogueBalloonAction_t3348434864 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DialogueBalloonAction_OnBalloonDestroyed_m1884765915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BalloonScript_t4017429074 * L_0 = __this->get_b_10();
		BalloonScript_t4017429074 * L_1 = L_0;
		UnityAction_t3245792599 * L_2 = L_1->get_BalloonDestroyed_4();
		intptr_t L_3 = (intptr_t)DialogueBalloonAction_OnBalloonDestroyed_m1884765915_RuntimeMethod_var;
		UnityAction_t3245792599 * L_4 = (UnityAction_t3245792599 *)il2cpp_codegen_object_new(UnityAction_t3245792599_il2cpp_TypeInfo_var);
		UnityAction__ctor_m772160306(L_4, __this, L_3, /*hidden argument*/NULL);
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		L_1->set_BalloonDestroyed_4(((UnityAction_t3245792599 *)CastclassSealed((RuntimeObject*)L_5, UnityAction_t3245792599_il2cpp_TypeInfo_var)));
		__this->set_b_10((BalloonScript_t4017429074 *)NULL);
		__this->set_balloonIsActive_11((bool)0);
		DialogueBalloonAction_t3348434864 * L_6 = __this->get_followingText_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_6, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		DialogueBalloonAction_t3348434864 * L_8 = __this->get_followingText_9();
		GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		VirtFuncInvoker1< bool, GameObject_t1113636619 * >::Invoke(4 /* System.Boolean Action::ExecuteAction(UnityEngine.GameObject) */, L_8, L_9);
	}

IL_0058:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DialogueBalloonAction/<WaitForBallonDestroyed>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CWaitForBallonDestroyedU3Ec__Iterator0__ctor_m108888311 (U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DialogueBalloonAction/<WaitForBallonDestroyed>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CWaitForBallonDestroyedU3Ec__Iterator0_MoveNext_m376558171 (U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForBallonDestroyedU3Ec__Iterator0_MoveNext_m376558171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_004c;
			}
		}
	}
	{
		goto IL_0053;
	}

IL_0021:
	{
		intptr_t L_2 = (intptr_t)U3CWaitForBallonDestroyedU3Ec__Iterator0_U3CU3Em__0_m2819031299_RuntimeMethod_var;
		Func_1_t3822001908 * L_3 = (Func_1_t3822001908 *)il2cpp_codegen_object_new(Func_1_t3822001908_il2cpp_TypeInfo_var);
		Func_1__ctor_m1399073142(L_3, __this, L_2, /*hidden argument*/Func_1__ctor_m1399073142_RuntimeMethod_var);
		WaitUntil_t3373419216 * L_4 = (WaitUntil_t3373419216 *)il2cpp_codegen_object_new(WaitUntil_t3373419216_il2cpp_TypeInfo_var);
		WaitUntil__ctor_m4227046299(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		bool L_5 = __this->get_U24disposing_2();
		if (L_5)
		{
			goto IL_0047;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0047:
	{
		goto IL_0055;
	}

IL_004c:
	{
		__this->set_U24PC_3((-1));
	}

IL_0053:
	{
		return (bool)0;
	}

IL_0055:
	{
		return (bool)1;
	}
}
// System.Object DialogueBalloonAction/<WaitForBallonDestroyed>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitForBallonDestroyedU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4121751871 (U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object DialogueBalloonAction/<WaitForBallonDestroyed>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitForBallonDestroyedU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m510007023 (U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void DialogueBalloonAction/<WaitForBallonDestroyed>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CWaitForBallonDestroyedU3Ec__Iterator0_Dispose_m1032254360 (U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void DialogueBalloonAction/<WaitForBallonDestroyed>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CWaitForBallonDestroyedU3Ec__Iterator0_Reset_m2388530936 (U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForBallonDestroyedU3Ec__Iterator0_Reset_m2388530936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0,U3CWaitForBallonDestroyedU3Ec__Iterator0_Reset_m2388530936_RuntimeMethod_var);
	}
}
// System.Boolean DialogueBalloonAction/<WaitForBallonDestroyed>c__Iterator0::<>m__0()
extern "C" IL2CPP_METHOD_ATTR bool U3CWaitForBallonDestroyedU3Ec__Iterator0_U3CU3Em__0_m2819031299 (U3CWaitForBallonDestroyedU3Ec__Iterator0_t3522846479 * __this, const RuntimeMethod* method)
{
	{
		DialogueBalloonAction_t3348434864 * L_0 = __this->get_U24this_0();
		bool L_1 = L_0->get_balloonIsActive_11();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DialogueSystem::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DialogueSystem__ctor_m3828846134 (DialogueSystem_t292220827 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// BalloonScript DialogueSystem::CreateBalloon(System.String,System.Boolean,UnityEngine.KeyCode,System.Single,UnityEngine.Color,UnityEngine.Color,UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR BalloonScript_t4017429074 * DialogueSystem_CreateBalloon_m1415492347 (DialogueSystem_t292220827 * __this, String_t* ___dialogueString0, bool ___usingButton1, int32_t ___button2, float ___timeToDisappear3, Color_t2555686324  ___backgroundC4, Color_t2555686324  ___textC5, Transform_t3600365921 * ___targetObj6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DialogueSystem_CreateBalloon_m1415492347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BalloonScript_t4017429074 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = __this->get_balloonPrefab_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_1 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_0, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		BalloonScript_t4017429074 * L_2 = GameObject_GetComponent_TisBalloonScript_t4017429074_m2020822283(L_1, /*hidden argument*/GameObject_GetComponent_TisBalloonScript_t4017429074_m2020822283_RuntimeMethod_var);
		V_0 = L_2;
		BalloonScript_t4017429074 * L_3 = V_0;
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(L_3, /*hidden argument*/NULL);
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_SetParent_m273471670(L_4, L_5, (bool)0, /*hidden argument*/NULL);
		BalloonScript_t4017429074 * L_6 = V_0;
		String_t* L_7 = ___dialogueString0;
		bool L_8 = ___usingButton1;
		int32_t L_9 = ___button2;
		float L_10 = ___timeToDisappear3;
		Color_t2555686324  L_11 = ___backgroundC4;
		Color_t2555686324  L_12 = ___textC5;
		Transform_t3600365921 * L_13 = ___targetObj6;
		BalloonScript_Setup_m3797033188(L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		BalloonScript_t4017429074 * L_14 = V_0;
		return L_14;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Enums::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Enums__ctor_m426293430 (Enums_t1053268879 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FollowTarget::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FollowTarget__ctor_m3876070367 (FollowTarget_t1456261924 * __this, const RuntimeMethod* method)
{
	{
		__this->set_speed_4((1.0f));
		Physics2DObject__ctor_m38296749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FollowTarget::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void FollowTarget_FixedUpdate_m3007629599 (FollowTarget_t1456261924 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FollowTarget_FixedUpdate_m3007629599_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = __this->get_target_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		bool L_2 = __this->get_lookAtTarget_5();
		if (!L_2)
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_3 = __this->get_useSide_6();
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_5 = __this->get_target_3();
		Vector3_t3722313464  L_6 = Transform_get_position_m36019626(L_5, /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_9 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_10 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_SetAxisTowards_m1999213583(NULL /*static, unused*/, L_3, L_4, L_10, /*hidden argument*/NULL);
	}

IL_004e:
	{
		Rigidbody2D_t939494601 * L_11 = ((Physics2DObject_t3026219368 *)__this)->get_rigidbody2D_2();
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_14 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		Transform_t3600365921 * L_15 = __this->get_target_3();
		Vector3_t3722313464  L_16 = Transform_get_position_m36019626(L_15, /*hidden argument*/NULL);
		Vector2_t2156229523  L_17 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		float L_18 = Time_get_fixedDeltaTime_m3595802076(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_19 = __this->get_speed_4();
		Vector2_t2156229523  L_20 = Vector2_Lerp_m854472224(NULL /*static, unused*/, L_14, L_17, ((float)il2cpp_codegen_multiply((float)L_18, (float)L_19)), /*hidden argument*/NULL);
		Rigidbody2D_MovePosition_m1934912072(L_11, L_20, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HealthSystemAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void HealthSystemAttribute__ctor_m2080356521 (HealthSystemAttribute_t2647166000 * __this, const RuntimeMethod* method)
{
	{
		__this->set_health_2(3);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HealthSystemAttribute::Start()
extern "C" IL2CPP_METHOD_ATTR void HealthSystemAttribute_Start_m151931609 (HealthSystemAttribute_t2647166000 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystemAttribute_Start_m151931609_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		UIScript_t1195624422 * L_0 = Object_FindObjectOfType_TisUIScript_t1195624422_m1742595665(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisUIScript_t1195624422_m1742595665_RuntimeMethod_var);
		__this->set_ui_3(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		String_t* L_2 = GameObject_get_tag_m3951609671(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m920492651(NULL /*static, unused*/, L_4, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_6, _stringLiteral259609454, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_004e;
		}
	}
	{
		goto IL_005a;
	}

IL_0042:
	{
		__this->set_playerNumber_5(0);
		goto IL_0066;
	}

IL_004e:
	{
		__this->set_playerNumber_5(1);
		goto IL_0066;
	}

IL_005a:
	{
		__this->set_playerNumber_5((-1));
		goto IL_0066;
	}

IL_0066:
	{
		UIScript_t1195624422 * L_8 = __this->get_ui_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_8, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_10 = __this->get_playerNumber_5();
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_009a;
		}
	}
	{
		UIScript_t1195624422 * L_11 = __this->get_ui_3();
		int32_t L_12 = __this->get_health_2();
		int32_t L_13 = __this->get_playerNumber_5();
		UIScript_SetHealth_m2132392576(L_11, L_12, L_13, /*hidden argument*/NULL);
	}

IL_009a:
	{
		int32_t L_14 = __this->get_health_2();
		__this->set_maxHealth_4(L_14);
		return;
	}
}
// System.Void HealthSystemAttribute::ModifyHealth(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void HealthSystemAttribute_ModifyHealth_m2785567747 (HealthSystemAttribute_t2647166000 * __this, int32_t ___amount0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystemAttribute_ModifyHealth_m2785567747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_health_2();
		int32_t L_1 = ___amount0;
		int32_t L_2 = __this->get_maxHealth_4();
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1))) <= ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_3 = __this->get_maxHealth_4();
		int32_t L_4 = __this->get_health_2();
		___amount0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)L_4));
	}

IL_0022:
	{
		int32_t L_5 = __this->get_health_2();
		int32_t L_6 = ___amount0;
		__this->set_health_2(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_6)));
		UIScript_t1195624422 * L_7 = __this->get_ui_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_7, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_9 = __this->get_playerNumber_5();
		if ((((int32_t)L_9) == ((int32_t)(-1))))
		{
			goto IL_005f;
		}
	}
	{
		UIScript_t1195624422 * L_10 = __this->get_ui_3();
		int32_t L_11 = ___amount0;
		int32_t L_12 = __this->get_playerNumber_5();
		UIScript_ChangeHealth_m2753858397(L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_005f:
	{
		int32_t L_13 = __this->get_health_2();
		if ((((int32_t)L_13) > ((int32_t)0)))
		{
			goto IL_0076;
		}
	}
	{
		GameObject_t1113636619 * L_14 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_0076:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InventoryResources::.ctor()
extern "C" IL2CPP_METHOD_ATTR void InventoryResources__ctor_m1076814834 (InventoryResources_t222662465 * __this, const RuntimeMethod* method)
{
	{
		ScriptableObject__ctor_m1310743131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String[] InventoryResources::GetResourceTypes()
extern "C" IL2CPP_METHOD_ATTR StringU5BU5D_t1281789340* InventoryResources_GetResourceTypes_m27341909 (InventoryResources_t222662465 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InventoryResources_GetResourceTypes_m27341909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	int32_t V_1 = 0;
	{
		List_1_t3319525431 * L_0 = __this->get_resourcesTypes_2();
		int32_t L_1 = List_1_get_Count_m2276455407(L_0, /*hidden argument*/List_1_get_Count_m2276455407_RuntimeMethod_var);
		V_0 = ((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)L_1));
		V_1 = 0;
		goto IL_002b;
	}

IL_0018:
	{
		StringU5BU5D_t1281789340* L_2 = V_0;
		int32_t L_3 = V_1;
		List_1_t3319525431 * L_4 = __this->get_resourcesTypes_2();
		int32_t L_5 = V_1;
		String_t* L_6 = List_1_get_Item_m3346958548(L_4, L_5, /*hidden argument*/List_1_get_Item_m3346958548_RuntimeMethod_var);
		ArrayElementTypeCheck (L_2, L_6);
		(L_2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3), (String_t*)L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_002b:
	{
		int32_t L_8 = V_1;
		List_1_t3319525431 * L_9 = __this->get_resourcesTypes_2();
		int32_t L_10 = List_1_get_Count_m2276455407(L_9, /*hidden argument*/List_1_get_Count_m2276455407_RuntimeMethod_var);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0018;
		}
	}
	{
		StringU5BU5D_t1281789340* L_11 = V_0;
		return L_11;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Jump::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Jump__ctor_m2243614027 (Jump_t2935391209 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Jump__ctor_m2243614027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_key_3(((int32_t)32));
		__this->set_jumpStrength_4((10.0f));
		__this->set_groundTag_5(_stringLiteral3128803744);
		__this->set_checkGround_6((bool)1);
		__this->set_canJump_7((bool)1);
		Physics2DObject__ctor_m38296749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Jump::Update()
extern "C" IL2CPP_METHOD_ATTR void Jump_Update_m2663392189 (Jump_t2935391209 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Jump_Update_m2663392189_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_canJump_7();
		if (!L_0)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_1 = __this->get_key_3();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0046;
		}
	}
	{
		Rigidbody2D_t939494601 * L_3 = ((Physics2DObject_t3026219368 *)__this)->get_rigidbody2D_2();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_4 = Vector2_get_up_m2647420593(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_jumpStrength_4();
		Vector2_t2156229523  L_6 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Rigidbody2D_AddForce_m1099013366(L_3, L_6, 1, /*hidden argument*/NULL);
		bool L_7 = __this->get_checkGround_6();
		__this->set_canJump_7((bool)((((int32_t)L_7) == ((int32_t)0))? 1 : 0));
	}

IL_0046:
	{
		return;
	}
}
// System.Void Jump::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void Jump_OnCollisionEnter2D_m1997108267 (Jump_t2935391209 * __this, Collision2D_t2842956331 * ___collisionData0, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_checkGround_6();
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		Collision2D_t2842956331 * L_1 = ___collisionData0;
		GameObject_t1113636619 * L_2 = Collision2D_get_gameObject_m1443495885(L_1, /*hidden argument*/NULL);
		String_t* L_3 = __this->get_groundTag_5();
		bool L_4 = GameObject_CompareTag_m3144439756(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		__this->set_canJump_7((bool)1);
	}

IL_0028:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LoadLevelAction::.ctor()
extern "C" IL2CPP_METHOD_ATTR void LoadLevelAction__ctor_m1384665989 (LoadLevelAction_t3647347530 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LoadLevelAction__ctor_m1384665989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_levelName_2(_stringLiteral3452614544);
		Action__ctor_m2761626621(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LoadLevelAction::ExecuteAction(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR bool LoadLevelAction_ExecuteAction_m1200367372 (LoadLevelAction_t3647347530 * __this, GameObject_t1113636619 * ___dataObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LoadLevelAction_ExecuteAction_m1200367372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t2348375561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = __this->get_levelName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral3452614544, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		Scene_t2348375561  L_2 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = Scene_get_name_m622963475((&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m2298600132(NULL /*static, unused*/, L_3, 0, /*hidden argument*/NULL);
		goto IL_0039;
	}

IL_002d:
	{
		String_t* L_4 = __this->get_levelName_2();
		SceneManager_LoadScene_m2298600132(NULL /*static, unused*/, L_4, 0, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ModifyHealthAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ModifyHealthAttribute__ctor_m3030725621 (ModifyHealthAttribute_t3455596632 * __this, const RuntimeMethod* method)
{
	{
		__this->set_healthChange_3((-1));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ModifyHealthAttribute::Reset()
extern "C" IL2CPP_METHOD_ATTR void ModifyHealthAttribute_Reset_m296152965 (ModifyHealthAttribute_t3455596632 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ModifyHealthAttribute_Reset_m296152965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_Collider2DDialogWindow_m3028026175(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ModifyHealthAttribute::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void ModifyHealthAttribute_OnCollisionEnter2D_m2323062171 (ModifyHealthAttribute_t3455596632 * __this, Collision2D_t2842956331 * ___collisionData0, const RuntimeMethod* method)
{
	{
		Collision2D_t2842956331 * L_0 = ___collisionData0;
		Collider2D_t2806799626 * L_1 = Collision2D_get_collider_m4087612183(L_0, /*hidden argument*/NULL);
		ModifyHealthAttribute_OnTriggerEnter2D_m197713471(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ModifyHealthAttribute::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void ModifyHealthAttribute_OnTriggerEnter2D_m197713471 (ModifyHealthAttribute_t3455596632 * __this, Collider2D_t2806799626 * ___colliderData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ModifyHealthAttribute_OnTriggerEnter2D_m197713471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HealthSystemAttribute_t2647166000 * V_0 = NULL;
	{
		Collider2D_t2806799626 * L_0 = ___colliderData0;
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		HealthSystemAttribute_t2647166000 * L_2 = GameObject_GetComponent_TisHealthSystemAttribute_t2647166000_m3641020519(L_1, /*hidden argument*/GameObject_GetComponent_TisHealthSystemAttribute_t2647166000_m3641020519_RuntimeMethod_var);
		V_0 = L_2;
		HealthSystemAttribute_t2647166000 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		HealthSystemAttribute_t2647166000 * L_5 = V_0;
		int32_t L_6 = __this->get_healthChange_3();
		HealthSystemAttribute_ModifyHealth_m2785567747(L_5, L_6, /*hidden argument*/NULL);
		bool L_7 = __this->get_destroyWhenActivated_2();
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		GameObject_t1113636619 * L_8 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_003a:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Move::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Move__ctor_m3047431633 (Move_t3440333737 * __this, const RuntimeMethod* method)
{
	{
		__this->set_speed_4((5.0f));
		Physics2DObject__ctor_m38296749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Move::Update()
extern "C" IL2CPP_METHOD_ATTR void Move_Update_m2605015955 (Move_t3440333737 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Move_Update_m2605015955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_typeOfControl_3();
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_1 = Input_GetAxis_m4009438427(NULL /*static, unused*/, _stringLiteral1828639942, /*hidden argument*/NULL);
		__this->set_moveHorizontal_10(L_1);
		float L_2 = Input_GetAxis_m4009438427(NULL /*static, unused*/, _stringLiteral2984908384, /*hidden argument*/NULL);
		__this->set_moveVertical_11(L_2);
		goto IL_0050;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_3 = Input_GetAxis_m4009438427(NULL /*static, unused*/, _stringLiteral4105162317, /*hidden argument*/NULL);
		__this->set_moveHorizontal_10(L_3);
		float L_4 = Input_GetAxis_m4009438427(NULL /*static, unused*/, _stringLiteral2167972859, /*hidden argument*/NULL);
		__this->set_moveVertical_11(L_4);
	}

IL_0050:
	{
		int32_t L_5 = __this->get_movementType_5();
		V_0 = L_5;
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_006a;
		}
	}
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) == ((int32_t)2)))
		{
			goto IL_007a;
		}
	}
	{
		goto IL_008a;
	}

IL_006a:
	{
		__this->set_moveVertical_11((0.0f));
		goto IL_008a;
	}

IL_007a:
	{
		__this->set_moveHorizontal_10((0.0f));
		goto IL_008a;
	}

IL_008a:
	{
		float L_8 = __this->get_moveHorizontal_10();
		float L_9 = __this->get_moveVertical_11();
		Vector2_t2156229523  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector2__ctor_m3970636864((&L_10), L_8, L_9, /*hidden argument*/NULL);
		__this->set_movement_8(L_10);
		bool L_11 = __this->get_orientToDirection_6();
		if (!L_11)
		{
			goto IL_00e4;
		}
	}
	{
		Vector2_t2156229523 * L_12 = __this->get_address_of_movement_8();
		float L_13 = Vector2_get_sqrMagnitude_m837837635(L_12, /*hidden argument*/NULL);
		if ((!(((float)L_13) >= ((float)(0.01f)))))
		{
			goto IL_00cd;
		}
	}
	{
		Vector2_t2156229523  L_14 = __this->get_movement_8();
		__this->set_cachedDirection_9(L_14);
	}

IL_00cd:
	{
		int32_t L_15 = __this->get_lookAxis_7();
		Transform_t3600365921 * L_16 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_17 = __this->get_cachedDirection_9();
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_SetAxisTowards_m1999213583(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
	}

IL_00e4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_18 = Input_GetKeyDown_m2928138282(NULL /*static, unused*/, _stringLiteral1613539661, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00f3;
		}
	}

IL_00f3:
	{
		return;
	}
}
// System.Void Move::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void Move_FixedUpdate_m2825193255 (Move_t3440333737 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Move_FixedUpdate_m2825193255_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t939494601 * L_0 = ((Physics2DObject_t3026219368 *)__this)->get_rigidbody2D_2();
		Vector2_t2156229523  L_1 = __this->get_movement_8();
		float L_2 = __this->get_speed_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_3 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector2_t2156229523  L_4 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_3, (10.0f), /*hidden argument*/NULL);
		Rigidbody2D_AddForce_m1113499586(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ObjectCreatorArea::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ObjectCreatorArea__ctor_m555900113 (ObjectCreatorArea_t9936620 * __this, const RuntimeMethod* method)
{
	{
		__this->set_spawnInterval_3((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ObjectCreatorArea::Start()
extern "C" IL2CPP_METHOD_ATTR void ObjectCreatorArea_Start_m1518290345 (ObjectCreatorArea_t9936620 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectCreatorArea_Start_m1518290345_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BoxCollider2D_t3581341831 * L_0 = Component_GetComponent_TisBoxCollider2D_t3581341831_m414724273(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t3581341831_m414724273_RuntimeMethod_var);
		__this->set_boxCollider2D_4(L_0);
		RuntimeObject* L_1 = ObjectCreatorArea_SpawnObject_m1647072653(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ObjectCreatorArea::SpawnObject()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* ObjectCreatorArea_SpawnObject_m1647072653 (ObjectCreatorArea_t9936620 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectCreatorArea_SpawnObject_m1647072653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSpawnObjectU3Ec__Iterator0_t3452356763 * V_0 = NULL;
	{
		U3CSpawnObjectU3Ec__Iterator0_t3452356763 * L_0 = (U3CSpawnObjectU3Ec__Iterator0_t3452356763 *)il2cpp_codegen_object_new(U3CSpawnObjectU3Ec__Iterator0_t3452356763_il2cpp_TypeInfo_var);
		U3CSpawnObjectU3Ec__Iterator0__ctor_m177627470(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSpawnObjectU3Ec__Iterator0_t3452356763 * L_1 = V_0;
		L_1->set_U24this_3(__this);
		U3CSpawnObjectU3Ec__Iterator0_t3452356763 * L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ObjectCreatorArea/<SpawnObject>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CSpawnObjectU3Ec__Iterator0__ctor_m177627470 (U3CSpawnObjectU3Ec__Iterator0_t3452356763 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ObjectCreatorArea/<SpawnObject>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CSpawnObjectU3Ec__Iterator0_MoveNext_m1266700745 (U3CSpawnObjectU3Ec__Iterator0_t3452356763 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSpawnObjectU3Ec__Iterator0_MoveNext_m1266700745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2156229523  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0140;
			}
		}
	}
	{
		goto IL_014c;
	}

IL_0021:
	{
		ObjectCreatorArea_t9936620 * L_2 = __this->get_U24this_3();
		BoxCollider2D_t3581341831 * L_3 = L_2->get_boxCollider2D_4();
		Vector2_t2156229523  L_4 = BoxCollider2D_get_size_m3529128736(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_x_0();
		ObjectCreatorArea_t9936620 * L_6 = __this->get_U24this_3();
		BoxCollider2D_t3581341831 * L_7 = L_6->get_boxCollider2D_4();
		Vector2_t2156229523  L_8 = BoxCollider2D_get_size_m3529128736(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = (&V_2)->get_x_0();
		float L_10 = Random_Range_m2202990745(NULL /*static, unused*/, ((-L_5)), L_9, /*hidden argument*/NULL);
		__this->set_U3CrandomXU3E__1_0(((float)il2cpp_codegen_multiply((float)L_10, (float)(0.5f))));
		ObjectCreatorArea_t9936620 * L_11 = __this->get_U24this_3();
		BoxCollider2D_t3581341831 * L_12 = L_11->get_boxCollider2D_4();
		Vector2_t2156229523  L_13 = BoxCollider2D_get_size_m3529128736(L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = (&V_3)->get_y_1();
		ObjectCreatorArea_t9936620 * L_15 = __this->get_U24this_3();
		BoxCollider2D_t3581341831 * L_16 = L_15->get_boxCollider2D_4();
		Vector2_t2156229523  L_17 = BoxCollider2D_get_size_m3529128736(L_16, /*hidden argument*/NULL);
		V_4 = L_17;
		float L_18 = (&V_4)->get_y_1();
		float L_19 = Random_Range_m2202990745(NULL /*static, unused*/, ((-L_14)), L_18, /*hidden argument*/NULL);
		__this->set_U3CrandomYU3E__1_1(((float)il2cpp_codegen_multiply((float)L_19, (float)(0.5f))));
		ObjectCreatorArea_t9936620 * L_20 = __this->get_U24this_3();
		GameObject_t1113636619 * L_21 = L_20->get_prefabToSpawn_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_22 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_21, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		__this->set_U3CnewObjectU3E__1_2(L_22);
		GameObject_t1113636619 * L_23 = __this->get_U3CnewObjectU3E__1_2();
		Transform_t3600365921 * L_24 = GameObject_get_transform_m1369836730(L_23, /*hidden argument*/NULL);
		float L_25 = __this->get_U3CrandomXU3E__1_0();
		ObjectCreatorArea_t9936620 * L_26 = __this->get_U24this_3();
		Transform_t3600365921 * L_27 = Component_get_transform_m3162698980(L_26, /*hidden argument*/NULL);
		Vector3_t3722313464  L_28 = Transform_get_position_m36019626(L_27, /*hidden argument*/NULL);
		V_5 = L_28;
		float L_29 = (&V_5)->get_x_1();
		float L_30 = __this->get_U3CrandomYU3E__1_1();
		ObjectCreatorArea_t9936620 * L_31 = __this->get_U24this_3();
		Transform_t3600365921 * L_32 = Component_get_transform_m3162698980(L_31, /*hidden argument*/NULL);
		Vector3_t3722313464  L_33 = Transform_get_position_m36019626(L_32, /*hidden argument*/NULL);
		V_6 = L_33;
		float L_34 = (&V_6)->get_y_2();
		Vector2_t2156229523  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector2__ctor_m3970636864((&L_35), ((float)il2cpp_codegen_add((float)L_25, (float)L_29)), ((float)il2cpp_codegen_add((float)L_30, (float)L_34)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_36 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_24, L_36, /*hidden argument*/NULL);
		ObjectCreatorArea_t9936620 * L_37 = __this->get_U24this_3();
		float L_38 = L_37->get_spawnInterval_3();
		WaitForSeconds_t1699091251 * L_39 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_39, L_38, /*hidden argument*/NULL);
		__this->set_U24current_4(L_39);
		bool L_40 = __this->get_U24disposing_5();
		if (L_40)
		{
			goto IL_013b;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_013b:
	{
		goto IL_014e;
	}

IL_0140:
	{
		goto IL_0021;
	}
	// Dead block : IL_0145: ldarg.0

IL_014c:
	{
		return (bool)0;
	}

IL_014e:
	{
		return (bool)1;
	}
}
// System.Object ObjectCreatorArea/<SpawnObject>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CSpawnObjectU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1969543014 (U3CSpawnObjectU3Ec__Iterator0_t3452356763 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object ObjectCreatorArea/<SpawnObject>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CSpawnObjectU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m287127324 (U3CSpawnObjectU3Ec__Iterator0_t3452356763 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void ObjectCreatorArea/<SpawnObject>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CSpawnObjectU3Ec__Iterator0_Dispose_m349995409 (U3CSpawnObjectU3Ec__Iterator0_t3452356763 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void ObjectCreatorArea/<SpawnObject>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CSpawnObjectU3Ec__Iterator0_Reset_m1582788942 (U3CSpawnObjectU3Ec__Iterator0_t3452356763 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSpawnObjectU3Ec__Iterator0_Reset_m1582788942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0,U3CSpawnObjectU3Ec__Iterator0_Reset_m1582788942_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ObjectShooter::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ObjectShooter__ctor_m3043071501 (ObjectShooter_t734116591 * __this, const RuntimeMethod* method)
{
	{
		__this->set_keyToPress_3(((int32_t)32));
		__this->set_creationRate_4((0.5f));
		__this->set_shootSpeed_5((5.0f));
		Vector2_t2156229523  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3970636864((&L_0), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_shootDirection_6(L_0);
		__this->set_relativeToRotation_7((bool)1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ObjectShooter::Start()
extern "C" IL2CPP_METHOD_ATTR void ObjectShooter_Start_m1222625711 (ObjectShooter_t734116591 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectShooter_Start_m1222625711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectShooter_t734116591 * G_B2_0 = NULL;
	ObjectShooter_t734116591 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ObjectShooter_t734116591 * G_B3_1 = NULL;
	{
		float L_0 = __this->get_creationRate_4();
		__this->set_timeOfLastSpawn_8(((-L_0)));
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		bool L_2 = GameObject_CompareTag_m3144439756(L_1, _stringLiteral2261822918, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_2)
		{
			G_B2_0 = __this;
			goto IL_0029;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_002a;
	}

IL_0029:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_002a:
	{
		G_B3_1->set_playerNumber_9(G_B3_0);
		return;
	}
}
// System.Void ObjectShooter::Update()
extern "C" IL2CPP_METHOD_ATTR void ObjectShooter_Update_m4161465741 (ObjectShooter_t734116591 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectShooter_Update_m4161465741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GameObject_t1113636619 * V_2 = NULL;
	Rigidbody2D_t939494601 * V_3 = NULL;
	BulletAttribute_t588811475 * V_4 = NULL;
	Vector2_t2156229523  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	{
		int32_t L_0 = __this->get_keyToPress_3();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKey_m3736388334(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_011d;
		}
	}
	{
		float L_2 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = __this->get_timeOfLastSpawn_8();
		float L_4 = __this->get_creationRate_4();
		if ((!(((float)L_2) >= ((float)((float)il2cpp_codegen_add((float)L_3, (float)L_4))))))
		{
			goto IL_011d;
		}
	}
	{
		bool L_5 = __this->get_relativeToRotation_7();
		if (!L_5)
		{
			goto IL_006e;
		}
	}
	{
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Transform_get_eulerAngles_m2743581774(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = (&V_1)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_9 = Quaternion_Euler_m3049309462(NULL /*static, unused*/, (0.0f), (0.0f), L_8, /*hidden argument*/NULL);
		Vector2_t2156229523  L_10 = __this->get_shootDirection_6();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_11 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Quaternion_op_Multiply_m2607404835(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		Vector2_t2156229523  L_13 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		G_B5_0 = L_13;
		goto IL_0074;
	}

IL_006e:
	{
		Vector2_t2156229523  L_14 = __this->get_shootDirection_6();
		G_B5_0 = L_14;
	}

IL_0074:
	{
		V_0 = G_B5_0;
		GameObject_t1113636619 * L_15 = __this->get_prefabToSpawn_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_16 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_15, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		V_2 = L_16;
		GameObject_t1113636619 * L_17 = V_2;
		Transform_t3600365921 * L_18 = GameObject_get_transform_m1369836730(L_17, /*hidden argument*/NULL);
		Transform_t3600365921 * L_19 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_20 = Transform_get_position_m36019626(L_19, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_18, L_20, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_21 = V_2;
		Transform_t3600365921 * L_22 = GameObject_get_transform_m1369836730(L_21, /*hidden argument*/NULL);
		Vector2_t2156229523  L_23 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		float L_24 = Utils_Angle_m3844311520(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		Vector3_t3722313464  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector3__ctor_m3353183577((&L_25), (0.0f), (0.0f), L_24, /*hidden argument*/NULL);
		Transform_set_eulerAngles_m135219616(L_22, L_25, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_26 = V_2;
		GameObject_set_tag_m2353670106(L_26, _stringLiteral37315637, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_27 = V_2;
		Rigidbody2D_t939494601 * L_28 = GameObject_GetComponent_TisRigidbody2D_t939494601_m2767154013(L_27, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t939494601_m2767154013_RuntimeMethod_var);
		V_3 = L_28;
		Rigidbody2D_t939494601 * L_29 = V_3;
		bool L_30 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_29, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00e8;
		}
	}
	{
		Rigidbody2D_t939494601 * L_31 = V_3;
		Vector2_t2156229523  L_32 = V_0;
		float L_33 = __this->get_shootSpeed_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_34 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		Rigidbody2D_AddForce_m1099013366(L_31, L_34, 1, /*hidden argument*/NULL);
	}

IL_00e8:
	{
		GameObject_t1113636619 * L_35 = V_2;
		BulletAttribute_t588811475 * L_36 = GameObject_GetComponent_TisBulletAttribute_t588811475_m587253523(L_35, /*hidden argument*/GameObject_GetComponent_TisBulletAttribute_t588811475_m587253523_RuntimeMethod_var);
		V_4 = L_36;
		BulletAttribute_t588811475 * L_37 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_38 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_37, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0105;
		}
	}
	{
		GameObject_t1113636619 * L_39 = V_2;
		BulletAttribute_t588811475 * L_40 = GameObject_AddComponent_TisBulletAttribute_t588811475_m718945470(L_39, /*hidden argument*/GameObject_AddComponent_TisBulletAttribute_t588811475_m718945470_RuntimeMethod_var);
		V_4 = L_40;
	}

IL_0105:
	{
		BulletAttribute_t588811475 * L_41 = V_4;
		int32_t L_42 = __this->get_playerNumber_9();
		L_41->set_playerId_2(L_42);
		float L_43 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timeOfLastSpawn_8(L_43);
	}

IL_011d:
	{
		return;
	}
}
// System.Void ObjectShooter::OnDrawGizmosSelected()
extern "C" IL2CPP_METHOD_ATTR void ObjectShooter_OnDrawGizmosSelected_m2214603868 (ObjectShooter_t734116591 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectShooter_OnDrawGizmosSelected_m2214603868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float G_B4_0 = 0.0f;
	{
		bool L_0 = Behaviour_get_enabled_m753527255(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0058;
		}
	}
	{
		bool L_1 = __this->get_relativeToRotation_7();
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_3 = Transform_get_rotation_m3502953881(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_t3722313464  L_4 = Quaternion_get_eulerAngles_m3425202016((&V_1), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = (&V_2)->get_z_3();
		G_B4_0 = L_5;
		goto IL_003b;
	}

IL_0036:
	{
		G_B4_0 = (0.0f);
	}

IL_003b:
	{
		V_0 = G_B4_0;
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		Vector2_t2156229523  L_8 = __this->get_shootDirection_6();
		float L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_DrawShootArrowGizmo_m2541881908(NULL /*static, unused*/, L_7, L_8, L_9, (1.0f), /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OnOffAction::.ctor()
extern "C" IL2CPP_METHOD_ATTR void OnOffAction__ctor_m4064504940 (OnOffAction_t1233588362 * __this, const RuntimeMethod* method)
{
	{
		Action__ctor_m2761626621(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean OnOffAction::ExecuteAction(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR bool OnOffAction_ExecuteAction_m1936074012 (OnOffAction_t1233588362 * __this, GameObject_t1113636619 * ___dataObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnOffAction_ExecuteAction_m1936074012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpriteRenderer_t3235626157 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = __this->get_objectToAffect_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006a;
		}
	}
	{
		bool L_2 = __this->get_justMakeInvisible_3();
		if (L_2)
		{
			goto IL_003a;
		}
	}
	{
		GameObject_t1113636619 * L_3 = __this->get_objectToAffect_2();
		GameObject_t1113636619 * L_4 = __this->get_objectToAffect_2();
		bool L_5 = GameObject_get_activeSelf_m1767405923(L_4, /*hidden argument*/NULL);
		GameObject_SetActive_m796801857(L_3, (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		goto IL_0068;
	}

IL_003a:
	{
		GameObject_t1113636619 * L_6 = __this->get_objectToAffect_2();
		SpriteRenderer_t3235626157 * L_7 = GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593(L_6, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3235626157_m1701782593_RuntimeMethod_var);
		V_0 = L_7;
		SpriteRenderer_t3235626157 * L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_8, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0066;
		}
	}
	{
		SpriteRenderer_t3235626157 * L_10 = V_0;
		SpriteRenderer_t3235626157 * L_11 = V_0;
		bool L_12 = Renderer_get_enabled_m3482452518(L_11, /*hidden argument*/NULL);
		Renderer_set_enabled_m1727253150(L_10, (bool)((((int32_t)L_12) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		goto IL_0068;
	}

IL_0066:
	{
		return (bool)0;
	}

IL_0068:
	{
		return (bool)1;
	}

IL_006a:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Patrol::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Patrol__ctor_m3849852994 (Patrol_t2573740331 * __this, const RuntimeMethod* method)
{
	{
		__this->set_speed_3((5.0f));
		__this->set_directionChangeInterval_4((3.0f));
		Physics2DObject__ctor_m38296749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Patrol::Start()
extern "C" IL2CPP_METHOD_ATTR void Patrol_Start_m2892979382 (Patrol_t2573740331 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Patrol_Start_m2892979382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t G_B6_0 = 0;
	{
		__this->set_currentTargetIndex_9(0);
		Vector2U5BU5D_t1457185986* L_0 = __this->get_waypoints_7();
		__this->set_newWaypoints_8(((Vector2U5BU5D_t1457185986*)SZArrayNew(Vector2U5BU5D_t1457185986_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length)))), (int32_t)1)))));
		V_0 = 0;
		V_1 = 0;
		goto IL_004d;
	}

IL_0025:
	{
		Vector2U5BU5D_t1457185986* L_1 = __this->get_newWaypoints_8();
		int32_t L_2 = V_1;
		Vector2U5BU5D_t1457185986* L_3 = __this->get_waypoints_7();
		int32_t L_4 = V_1;
		*(Vector2_t2156229523 *)((L_1)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_2))) = (*(Vector2_t2156229523 *)((L_3)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = V_1;
		V_0 = L_5;
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_004d:
	{
		int32_t L_7 = V_1;
		Vector2U5BU5D_t1457185986* L_8 = __this->get_waypoints_7();
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_8)->max_length)))))))
		{
			goto IL_0025;
		}
	}
	{
		Vector2U5BU5D_t1457185986* L_9 = __this->get_newWaypoints_8();
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length))))) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_10 = V_0;
		G_B6_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
		goto IL_0072;
	}

IL_0071:
	{
		G_B6_0 = 0;
	}

IL_0072:
	{
		V_2 = G_B6_0;
		Vector2U5BU5D_t1457185986* L_11 = __this->get_newWaypoints_8();
		int32_t L_12 = V_2;
		Transform_t3600365921 * L_13 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_14 = Transform_get_position_m36019626(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_15 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		*(Vector2_t2156229523 *)((L_11)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_12))) = L_15;
		bool L_16 = __this->get_orientToDirection_5();
		if (!L_16)
		{
			goto IL_00e3;
		}
	}
	{
		int32_t L_17 = __this->get_lookAxis_6();
		Transform_t3600365921 * L_18 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector2U5BU5D_t1457185986* L_19 = __this->get_newWaypoints_8();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_20 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, (*(Vector2_t2156229523 *)((L_19)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(1)))), /*hidden argument*/NULL);
		Transform_t3600365921 * L_21 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_22 = Transform_get_position_m36019626(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_23 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		Vector3_t3722313464  L_24 = Vector3_get_normalized_m2454957984((&V_3), /*hidden argument*/NULL);
		Vector2_t2156229523  L_25 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_SetAxisTowards_m1999213583(NULL /*static, unused*/, L_17, L_18, L_25, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		return;
	}
}
// System.Void Patrol::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void Patrol_FixedUpdate_m2597641244 (Patrol_t2573740331 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Patrol_FixedUpdate_m2597641244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Patrol_t2573740331 * G_B3_0 = NULL;
	Patrol_t2573740331 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	Patrol_t2573740331 * G_B4_1 = NULL;
	{
		Vector2U5BU5D_t1457185986* L_0 = __this->get_newWaypoints_8();
		int32_t L_1 = __this->get_currentTargetIndex_9();
		V_0 = (*(Vector2_t2156229523 *)((L_0)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_1))));
		Rigidbody2D_t939494601 * L_2 = ((Physics2DObject_t3026219368 *)__this)->get_rigidbody2D_2();
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		Vector2_t2156229523  L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_9 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Vector3_t3722313464  L_10 = Vector3_get_normalized_m2454957984((&V_1), /*hidden argument*/NULL);
		float L_11 = __this->get_speed_3();
		Vector3_t3722313464  L_12 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		float L_13 = Time_get_fixedDeltaTime_m3595802076(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_14 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Vector3_t3722313464  L_15 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_4, L_14, /*hidden argument*/NULL);
		Vector2_t2156229523  L_16 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Rigidbody2D_MovePosition_m1934912072(L_2, L_16, /*hidden argument*/NULL);
		Transform_t3600365921 * L_17 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_18 = Transform_get_position_m36019626(L_17, /*hidden argument*/NULL);
		Vector2_t2156229523  L_19 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		Vector2_t2156229523  L_20 = V_0;
		float L_21 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		if ((!(((float)L_21) <= ((float)(0.1f)))))
		{
			goto IL_0109;
		}
	}
	{
		int32_t L_22 = __this->get_currentTargetIndex_9();
		Vector2U5BU5D_t1457185986* L_23 = __this->get_newWaypoints_8();
		G_B2_0 = __this;
		if ((((int32_t)L_22) >= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_23)->max_length)))), (int32_t)1)))))
		{
			G_B3_0 = __this;
			goto IL_00ad;
		}
	}
	{
		int32_t L_24 = __this->get_currentTargetIndex_9();
		G_B4_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1));
		G_B4_1 = G_B2_0;
		goto IL_00ae;
	}

IL_00ad:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_00ae:
	{
		G_B4_1->set_currentTargetIndex_9(G_B4_0);
		bool L_25 = __this->get_orientToDirection_5();
		if (!L_25)
		{
			goto IL_0109;
		}
	}
	{
		Vector2U5BU5D_t1457185986* L_26 = __this->get_newWaypoints_8();
		int32_t L_27 = __this->get_currentTargetIndex_9();
		V_0 = (*(Vector2_t2156229523 *)((L_26)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_27))));
		int32_t L_28 = __this->get_lookAxis_6();
		Transform_t3600365921 * L_29 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_30 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_31 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Transform_t3600365921 * L_32 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_33 = Transform_get_position_m36019626(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_34 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_31, L_33, /*hidden argument*/NULL);
		V_2 = L_34;
		Vector3_t3722313464  L_35 = Vector3_get_normalized_m2454957984((&V_2), /*hidden argument*/NULL);
		Vector2_t2156229523  L_36 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_SetAxisTowards_m1999213583(NULL /*static, unused*/, L_28, L_29, L_36, /*hidden argument*/NULL);
	}

IL_0109:
	{
		return;
	}
}
// System.Void Patrol::Reset()
extern "C" IL2CPP_METHOD_ATTR void Patrol_Reset_m2317667825 (Patrol_t2573740331 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Patrol_Reset_m2317667825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		__this->set_waypoints_7(((Vector2U5BU5D_t1457185986*)SZArrayNew(Vector2U5BU5D_t1457185986_il2cpp_TypeInfo_var, (uint32_t)1)));
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector2U5BU5D_t1457185986* L_3 = __this->get_waypoints_7();
		Vector2_t2156229523  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3970636864((&L_4), (2.0f), (0.5f), /*hidden argument*/NULL);
		Vector2_t2156229523  L_5 = V_0;
		Vector2_t2156229523  L_6 = Vector2_op_Addition_m800700293(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		*(Vector2_t2156229523 *)((L_3)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(0))) = L_6;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Physics2DObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Physics2DObject__ctor_m38296749 (Physics2DObject_t3026219368 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Physics2DObject::Awake()
extern "C" IL2CPP_METHOD_ATTR void Physics2DObject_Awake_m2862751549 (Physics2DObject_t3026219368 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2DObject_Awake_m2862751549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t939494601 * L_0 = Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var);
		__this->set_rigidbody2D_2(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PickUpAndHold::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PickUpAndHold__ctor_m3315327593 (PickUpAndHold_t4109171223 * __this, const RuntimeMethod* method)
{
	{
		__this->set_pickupKey_2(((int32_t)98));
		__this->set_dropKey_3(((int32_t)98));
		__this->set_pickUpDistance_4((2.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PickUpAndHold::Update()
extern "C" IL2CPP_METHOD_ATTR void PickUpAndHold_Update_m2356086211 (PickUpAndHold_t4109171223 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PickUpAndHold_Update_m2356086211_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		int32_t L_0 = __this->get_pickupKey_2();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		Transform_t3600365921 * L_2 = __this->get_carriedObject_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		bool L_4 = PickUpAndHold_PickUp_m1915750871(__this, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002a:
	{
		int32_t L_5 = __this->get_dropKey_3();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_6 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0057;
		}
	}
	{
		Transform_t3600365921 * L_7 = __this->get_carriedObject_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_7, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0057;
		}
	}
	{
		bool L_9 = V_0;
		if (L_9)
		{
			goto IL_0057;
		}
	}
	{
		PickUpAndHold_Drop_m1762827602(__this, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void PickUpAndHold::Drop()
extern "C" IL2CPP_METHOD_ATTR void PickUpAndHold_Drop_m1762827602 (PickUpAndHold_t4109171223 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PickUpAndHold_Drop_m1762827602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody2D_t939494601 * V_0 = NULL;
	{
		Transform_t3600365921 * L_0 = __this->get_carriedObject_5();
		Rigidbody2D_t939494601 * L_1 = Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(L_0, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var);
		V_0 = L_1;
		Rigidbody2D_t939494601 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		Rigidbody2D_t939494601 * L_4 = V_0;
		Rigidbody2D_set_bodyType_m1814100804(L_4, 0, /*hidden argument*/NULL);
		Rigidbody2D_t939494601 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_6 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m2898400508(L_5, L_6, /*hidden argument*/NULL);
	}

IL_002a:
	{
		Transform_t3600365921 * L_7 = __this->get_carriedObject_5();
		Transform_set_parent_m786917804(L_7, (Transform_t3600365921 *)NULL, /*hidden argument*/NULL);
		__this->set_carriedObject_5((Transform_t3600365921 *)NULL);
		return;
	}
}
// System.Boolean PickUpAndHold::PickUp()
extern "C" IL2CPP_METHOD_ATTR bool PickUpAndHold_PickUp_m1915750871 (PickUpAndHold_t4109171223 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PickUpAndHold_PickUp_m1915750871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_t3328599146* V_0 = NULL;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Transform_t3600365921 * V_5 = NULL;
	PickUpAndHold_t4109171223 * V_6 = NULL;
	Rigidbody2D_t939494601 * V_7 = NULL;
	{
		GameObjectU5BU5D_t3328599146* L_0 = GameObject_FindGameObjectsWithTag_m2585173894(NULL /*static, unused*/, _stringLiteral4289369868, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = __this->get_pickUpDistance_4();
		V_1 = L_1;
		V_2 = 0;
		goto IL_005b;
	}

IL_0019:
	{
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_4 = V_0;
		int32_t L_5 = V_2;
		int32_t L_6 = L_5;
		GameObject_t1113636619 * L_7 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_6));
		Transform_t3600365921 * L_8 = GameObject_get_transform_m1369836730(L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_10 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_3, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = Vector3_get_sqrMagnitude_m1474274574((&V_4), /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = V_3;
		float L_13 = V_1;
		if ((!(((float)L_12) < ((float)L_13))))
		{
			goto IL_0057;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_14 = V_0;
		int32_t L_15 = V_2;
		int32_t L_16 = L_15;
		GameObject_t1113636619 * L_17 = (L_14)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_16));
		Transform_t3600365921 * L_18 = GameObject_get_transform_m1369836730(L_17, /*hidden argument*/NULL);
		__this->set_carriedObject_5(L_18);
		float L_19 = V_3;
		V_1 = L_19;
	}

IL_0057:
	{
		int32_t L_20 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_005b:
	{
		int32_t L_21 = V_2;
		GameObjectU5BU5D_t3328599146* L_22 = V_0;
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_22)->max_length)))))))
		{
			goto IL_0019;
		}
	}
	{
		Transform_t3600365921 * L_23 = __this->get_carriedObject_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_23, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00eb;
		}
	}
	{
		Transform_t3600365921 * L_25 = __this->get_carriedObject_5();
		Transform_t3600365921 * L_26 = Transform_get_parent_m835071599(L_25, /*hidden argument*/NULL);
		V_5 = L_26;
		Transform_t3600365921 * L_27 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_28 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_27, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00ac;
		}
	}
	{
		Transform_t3600365921 * L_29 = V_5;
		PickUpAndHold_t4109171223 * L_30 = Component_GetComponent_TisPickUpAndHold_t4109171223_m2673350194(L_29, /*hidden argument*/Component_GetComponent_TisPickUpAndHold_t4109171223_m2673350194_RuntimeMethod_var);
		V_6 = L_30;
		PickUpAndHold_t4109171223 * L_31 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_32 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_31, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00ac;
		}
	}
	{
		PickUpAndHold_t4109171223 * L_33 = V_6;
		PickUpAndHold_Drop_m1762827602(L_33, /*hidden argument*/NULL);
	}

IL_00ac:
	{
		Transform_t3600365921 * L_34 = __this->get_carriedObject_5();
		GameObject_t1113636619 * L_35 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_36 = GameObject_get_transform_m1369836730(L_35, /*hidden argument*/NULL);
		Transform_set_parent_m786917804(L_34, L_36, /*hidden argument*/NULL);
		Transform_t3600365921 * L_37 = __this->get_carriedObject_5();
		Rigidbody2D_t939494601 * L_38 = Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(L_37, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var);
		V_7 = L_38;
		Rigidbody2D_t939494601 * L_39 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_40 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_39, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_00e9;
		}
	}
	{
		Rigidbody2D_t939494601 * L_41 = V_7;
		Rigidbody2D_t939494601 * L_42 = Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(L_41, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var);
		Rigidbody2D_set_bodyType_m1814100804(L_42, 1, /*hidden argument*/NULL);
	}

IL_00e9:
	{
		return (bool)1;
	}

IL_00eb:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Push::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Push__ctor_m508165095 (Push_t4181980645 * __this, const RuntimeMethod* method)
{
	{
		__this->set_key_3(((int32_t)32));
		__this->set_pushStrength_4((5.0f));
		__this->set_axis_5(1);
		__this->set_relativeAxis_6((bool)1);
		Physics2DObject__ctor_m38296749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Push::Update()
extern "C" IL2CPP_METHOD_ATTR void Push_Update_m2660533297 (Push_t4181980645 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Push_Update_m2660533297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_key_3();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKey_m3736388334(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_keyPressed_7(L_1);
		return;
	}
}
// System.Void Push::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void Push_FixedUpdate_m920108308 (Push_t4181980645 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Push_FixedUpdate_m920108308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_keyPressed_7();
		if (!L_0)
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_1 = __this->get_axis_5();
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Utils_GetVectorFromAxis_m3396922722(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_pushStrength_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_4 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_pushVector_8(L_4);
		bool L_5 = __this->get_relativeAxis_6();
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		Rigidbody2D_t939494601 * L_6 = ((Physics2DObject_t3026219368 *)__this)->get_rigidbody2D_2();
		Vector2_t2156229523  L_7 = __this->get_pushVector_8();
		Rigidbody2D_AddRelativeForce_m3358548001(L_6, L_7, /*hidden argument*/NULL);
		goto IL_0059;
	}

IL_0048:
	{
		Rigidbody2D_t939494601 * L_8 = ((Physics2DObject_t3026219368 *)__this)->get_rigidbody2D_2();
		Vector2_t2156229523  L_9 = __this->get_pushVector_8();
		Rigidbody2D_AddForce_m1113499586(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0059:
	{
		return;
	}
}
// System.Void Push::OnDrawGizmosSelected()
extern "C" IL2CPP_METHOD_ATTR void Push_OnDrawGizmosSelected_m1355904309 (Push_t4181980645 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Push_OnDrawGizmosSelected_m1355904309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float G_B4_0 = 0.0f;
	{
		bool L_0 = Behaviour_get_enabled_m753527255(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_007b;
		}
	}
	{
		bool L_1 = __this->get_relativeAxis_6();
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_3 = Transform_get_rotation_m3502953881(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_t3722313464  L_4 = Quaternion_get_eulerAngles_m3425202016((&V_1), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = (&V_2)->get_z_3();
		G_B4_0 = L_5;
		goto IL_003b;
	}

IL_0036:
	{
		G_B4_0 = (0.0f);
	}

IL_003b:
	{
		V_0 = G_B4_0;
		int32_t L_6 = __this->get_axis_5();
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_7 = Utils_GetVectorFromAxis_m3396922722(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		float L_8 = __this->get_pushStrength_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_9 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		__this->set_pushVector_8(L_9);
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Transform_get_position_m36019626(L_10, /*hidden argument*/NULL);
		Vector2_t2156229523  L_12 = __this->get_pushVector_8();
		float L_13 = V_0;
		float L_14 = __this->get_pushStrength_4();
		Utils_DrawMoveArrowGizmo_m4283051242(NULL /*static, unused*/, L_11, L_12, L_13, ((float)il2cpp_codegen_multiply((float)L_14, (float)(0.5f))), /*hidden argument*/NULL);
	}

IL_007b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ResourceAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ResourceAttribute__ctor_m1943800212 (ResourceAttribute_t892502608 * __this, const RuntimeMethod* method)
{
	{
		__this->set_amount_3(1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ResourceAttribute::Start()
extern "C" IL2CPP_METHOD_ATTR void ResourceAttribute_Start_m4290658858 (ResourceAttribute_t892502608 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ResourceAttribute_Start_m4290658858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		UIScript_t1195624422 * L_0 = Object_FindObjectOfType_TisUIScript_t1195624422_m1742595665(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisUIScript_t1195624422_m1742595665_RuntimeMethod_var);
		__this->set_userInterface_4(L_0);
		return;
	}
}
// System.Void ResourceAttribute::Reset()
extern "C" IL2CPP_METHOD_ATTR void ResourceAttribute_Reset_m2199242252 (ResourceAttribute_t892502608 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ResourceAttribute_Reset_m2199242252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_Collider2DDialogWindow_m3028026175(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ResourceAttribute::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void ResourceAttribute_OnTriggerEnter2D_m3680524752 (ResourceAttribute_t892502608 * __this, Collider2D_t2806799626 * ___otherCollider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ResourceAttribute_OnTriggerEnter2D_m3680524752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t2806799626 * L_0 = ___otherCollider0;
		bool L_1 = Component_CompareTag_m1328479619(L_0, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		Collider2D_t2806799626 * L_2 = ___otherCollider0;
		bool L_3 = Component_CompareTag_m1328479619(L_2, _stringLiteral259609454, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_006d;
		}
	}

IL_0020:
	{
		UIScript_t1195624422 * L_4 = __this->get_userInterface_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0058;
		}
	}
	{
		UIScript_t1195624422 * L_6 = __this->get_userInterface_4();
		int32_t L_7 = __this->get_resourceIndex_2();
		int32_t L_8 = __this->get_amount_3();
		SpriteRenderer_t3235626157 * L_9 = Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502_RuntimeMethod_var);
		Sprite_t280657092 * L_10 = SpriteRenderer_get_sprite_m663386871(L_9, /*hidden argument*/NULL);
		UIScript_AddResource_m2508601531(L_6, L_7, L_8, L_10, /*hidden argument*/NULL);
		goto IL_0062;
	}

IL_0058:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral975027493, /*hidden argument*/NULL);
	}

IL_0062:
	{
		GameObject_t1113636619 * L_11 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ResourceStruct::.ctor(System.Int32,UIItemScript)
extern "C" IL2CPP_METHOD_ATTR void ResourceStruct__ctor_m3839822749 (ResourceStruct_t862114426 * __this, int32_t ___a0, UIItemScript_t1692532892 * ___uiRef1, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___a0;
		__this->set_amount_0(L_0);
		UIItemScript_t1692532892 * L_1 = ___uiRef1;
		__this->set_UIItem_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Rotate::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Rotate__ctor_m838679357 (Rotate_t1850091912 * __this, const RuntimeMethod* method)
{
	{
		__this->set_speed_4((5.0f));
		Physics2DObject__ctor_m38296749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Rotate::Update()
extern "C" IL2CPP_METHOD_ATTR void Rotate_Update_m2566458797 (Rotate_t1850091912 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rotate_Update_m2566458797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_typeOfControl_3();
		if (L_0)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_1 = Input_GetAxis_m4009438427(NULL /*static, unused*/, _stringLiteral1828639942, /*hidden argument*/NULL);
		__this->set_spin_5(L_1);
		goto IL_0030;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_2 = Input_GetAxis_m4009438427(NULL /*static, unused*/, _stringLiteral4105162317, /*hidden argument*/NULL);
		__this->set_spin_5(L_2);
	}

IL_0030:
	{
		return;
	}
}
// System.Void Rotate::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void Rotate_FixedUpdate_m833050613 (Rotate_t1850091912 * __this, const RuntimeMethod* method)
{
	{
		Rigidbody2D_t939494601 * L_0 = ((Physics2DObject_t3026219368 *)__this)->get_rigidbody2D_2();
		float L_1 = __this->get_spin_5();
		float L_2 = __this->get_speed_4();
		Rigidbody2D_AddTorque_m1535770154(L_0, ((float)il2cpp_codegen_multiply((float)((-L_1)), (float)L_2)), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Shoot::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Shoot__ctor_m2241958956 (Shoot_t2202938192 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Shoot::Start()
extern "C" IL2CPP_METHOD_ATTR void Shoot_Start_m3249486140 (Shoot_t2202938192 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Shoot::Update()
extern "C" IL2CPP_METHOD_ATTR void Shoot_Update_m316274748 (Shoot_t2202938192 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Stopper::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Stopper__ctor_m2007057324 (Stopper_t1976456672 * __this, const RuntimeMethod* method)
{
	{
		__this->set_live_9((bool)1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stopper::Start()
extern "C" IL2CPP_METHOD_ATTR void Stopper_Start_m3905942180 (Stopper_t1976456672 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stopper_Start_m3905942180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t939494601 * L_0 = Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var);
		__this->set_rb2D_2(L_0);
		CircleCollider2D_t662546754 * L_1 = Component_GetComponent_TisCircleCollider2D_t662546754_m1597288992(__this, /*hidden argument*/Component_GetComponent_TisCircleCollider2D_t662546754_m1597288992_RuntimeMethod_var);
		__this->set_collider_4(L_1);
		SpriteRenderer_t3235626157 * L_2 = Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3235626157_m2253180502_RuntimeMethod_var);
		__this->set_renderer_3(L_2);
		Rigidbody2D_t939494601 * L_3 = __this->get_rb2D_2();
		float L_4 = Rigidbody2D_get_mass_m816391807(L_3, /*hidden argument*/NULL);
		__this->set_mass_5(L_4);
		return;
	}
}
// System.Void Stopper::Update()
extern "C" IL2CPP_METHOD_ATTR void Stopper_Update_m235651721 (Stopper_t1976456672 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stopper_Update_m235651721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpriteRenderer_t3235626157 * G_B5_0 = NULL;
	SpriteRenderer_t3235626157 * G_B4_0 = NULL;
	Color_t2555686324  G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	SpriteRenderer_t3235626157 * G_B6_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)112), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		Transform_t3600365921 * L_1 = __this->get_holder_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Stopper_throwBall_m3126347534(__this, /*hidden argument*/NULL);
	}

IL_0023:
	{
		SpriteRenderer_t3235626157 * L_3 = __this->get_renderer_3();
		bool L_4 = __this->get_live_9();
		G_B4_0 = L_3;
		if (!L_4)
		{
			G_B5_0 = L_3;
			goto IL_003e;
		}
	}
	{
		Color_t2555686324  L_5 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_5;
		G_B6_1 = G_B4_0;
		goto IL_0043;
	}

IL_003e:
	{
		Color_t2555686324  L_6 = Color_get_red_m3227813939(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_6;
		G_B6_1 = G_B5_0;
	}

IL_0043:
	{
		SpriteRenderer_set_color_m3056777566(G_B6_1, G_B6_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stopper::throwBall()
extern "C" IL2CPP_METHOD_ATTR void Stopper_throwBall_m3126347534 (Stopper_t1976456672 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stopper_throwBall_m3126347534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t939494601 * L_0 = __this->get_rb2D_2();
		Rigidbody2D_set_angularVelocity_m2791812150(L_0, (0.0f), /*hidden argument*/NULL);
		Rigidbody2D_t939494601 * L_1 = __this->get_rb2D_2();
		Transform_t3600365921 * L_2 = __this->get_holder_6();
		Vector3_t3722313464  L_3 = Transform_get_up_m3972993886(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_3, (10000.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_5 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Rigidbody2D_AddForce_m1099013366(L_1, L_5, 1, /*hidden argument*/NULL);
		__this->set_live_9((bool)0);
		Stopper_onHolder_m969266357(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stopper::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void Stopper_OnCollisionEnter2D_m1669770713 (Stopper_t1976456672 * __this, Collision2D_t2842956331 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stopper_OnCollisionEnter2D_m1669770713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collision2D_t2842956331 * L_0 = ___collision0;
		GameObject_t1113636619 * L_1 = Collision2D_get_gameObject_m1443495885(L_0, /*hidden argument*/NULL);
		bool L_2 = GameObject_CompareTag_m3144439756(L_1, _stringLiteral37315637, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		Rigidbody2D_t939494601 * L_3 = __this->get_rb2D_2();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_4 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m2898400508(L_3, L_4, /*hidden argument*/NULL);
		Rigidbody2D_t939494601 * L_5 = __this->get_rb2D_2();
		Rigidbody2D_set_angularVelocity_m2791812150(L_5, (0.0f), /*hidden argument*/NULL);
		return;
	}

IL_0036:
	{
		bool L_6 = __this->get_live_9();
		if (L_6)
		{
			goto IL_0042;
		}
	}
	{
		return;
	}

IL_0042:
	{
		Collision2D_t2842956331 * L_7 = ___collision0;
		GameObject_t1113636619 * L_8 = Collision2D_get_gameObject_m1443495885(L_7, /*hidden argument*/NULL);
		bool L_9 = GameObject_CompareTag_m3144439756(L_8, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00b4;
		}
	}
	{
		Transform_t3600365921 * L_10 = __this->get_holder_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_10, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0097;
		}
	}
	{
		CircleCollider2D_t662546754 * L_12 = __this->get_collider_4();
		Transform_t3600365921 * L_13 = Component_get_transform_m3162698980(L_12, /*hidden argument*/NULL);
		__this->set_player_7(L_13);
		Collision2D_t2842956331 * L_14 = ___collision0;
		Transform_t3600365921 * L_15 = Collision2D_get_transform_m2670923831(L_14, /*hidden argument*/NULL);
		Transform_t3600365921 * L_16 = Transform_GetChild_m1092972975(L_15, 0, /*hidden argument*/NULL);
		__this->set_holder_6(L_16);
		Stopper_onHolder_m969266357(__this, (bool)1, /*hidden argument*/NULL);
		goto IL_00b4;
	}

IL_0097:
	{
		Transform_t3600365921 * L_17 = __this->get_player_7();
		Collision2D_t2842956331 * L_18 = ___collision0;
		Transform_t3600365921 * L_19 = Collision2D_get_transform_m2670923831(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b4;
		}
	}
	{
		__this->set_holder_6((Transform_t3600365921 *)NULL);
	}

IL_00b4:
	{
		return;
	}
}
// System.Void Stopper::onHolder(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Stopper_onHolder_m969266357 (Stopper_t1976456672 * __this, bool ___result0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stopper_onHolder_m969266357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3600365921 * G_B2_0 = NULL;
	Transform_t3600365921 * G_B1_0 = NULL;
	Transform_t3600365921 * G_B3_0 = NULL;
	Transform_t3600365921 * G_B3_1 = NULL;
	Transform_t3600365921 * G_B5_0 = NULL;
	Transform_t3600365921 * G_B4_0 = NULL;
	Vector3_t3722313464  G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	Transform_t3600365921 * G_B6_1 = NULL;
	{
		CircleCollider2D_t662546754 * L_0 = __this->get_collider_4();
		bool L_1 = ___result0;
		Collider2D_set_isTrigger_m2923405871(L_0, L_1, /*hidden argument*/NULL);
		Rigidbody2D_t939494601 * L_2 = __this->get_rb2D_2();
		bool L_3 = ___result0;
		Rigidbody2D_set_isKinematic_m1070983191(L_2, L_3, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		bool L_5 = ___result0;
		G_B1_0 = L_4;
		if (!L_5)
		{
			G_B2_0 = L_4;
			goto IL_002f;
		}
	}
	{
		Transform_t3600365921 * L_6 = __this->get_holder_6();
		G_B3_0 = L_6;
		G_B3_1 = G_B1_0;
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = ((Transform_t3600365921 *)(NULL));
		G_B3_1 = G_B2_0;
	}

IL_0030:
	{
		Transform_set_parent_m786917804(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		bool L_8 = ___result0;
		G_B4_0 = L_7;
		if (!L_8)
		{
			G_B5_0 = L_7;
			goto IL_0051;
		}
	}
	{
		Transform_t3600365921 * L_9 = __this->get_holder_6();
		Vector3_t3722313464  L_10 = Transform_get_position_m36019626(L_9, /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		goto IL_005c;
	}

IL_0051:
	{
		Transform_t3600365921 * L_11 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Transform_get_position_m36019626(L_11, /*hidden argument*/NULL);
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
	}

IL_005c:
	{
		Transform_set_position_m3387557959(G_B6_1, G_B6_0, /*hidden argument*/NULL);
		bool L_13 = ___result0;
		if (L_13)
		{
			goto IL_0077;
		}
	}
	{
		MonoBehaviour_Invoke_m4227543964(__this, _stringLiteral2640249375, (3.0f), /*hidden argument*/NULL);
	}

IL_0077:
	{
		return;
	}
}
// System.Void Stopper::setLive()
extern "C" IL2CPP_METHOD_ATTR void Stopper_setLive_m2034170828 (Stopper_t1976456672 * __this, const RuntimeMethod* method)
{
	{
		__this->set_live_9((bool)1);
		__this->set_holder_6((Transform_t3600365921 *)NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TeleportAction::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TeleportAction__ctor_m1645361927 (TeleportAction_t1205856175 * __this, const RuntimeMethod* method)
{
	{
		__this->set_stopMovements_4((bool)1);
		Action__ctor_m2761626621(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean TeleportAction::ExecuteAction(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR bool TeleportAction_ExecuteAction_m2630593480 (TeleportAction_t1205856175 * __this, GameObject_t1113636619 * ___dataObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TeleportAction_ExecuteAction_m2630593480_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody2D_t939494601 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = __this->get_objectToMove_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003d;
		}
	}
	{
		GameObject_t1113636619 * L_2 = __this->get_objectToMove_2();
		Transform_t3600365921 * L_3 = GameObject_get_transform_m1369836730(L_2, /*hidden argument*/NULL);
		Vector2_t2156229523  L_4 = __this->get_newPosition_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_5 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_3, L_5, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_6 = __this->get_objectToMove_2();
		Rigidbody2D_t939494601 * L_7 = GameObject_GetComponent_TisRigidbody2D_t939494601_m2767154013(L_6, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t939494601_m2767154013_RuntimeMethod_var);
		V_0 = L_7;
		goto IL_005f;
	}

IL_003d:
	{
		Transform_t3600365921 * L_8 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_9 = __this->get_newPosition_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_10 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_8, L_10, /*hidden argument*/NULL);
		Transform_t3600365921 * L_11 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Rigidbody2D_t939494601 * L_12 = Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(L_11, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var);
		V_0 = L_12;
	}

IL_005f:
	{
		bool L_13 = __this->get_stopMovements_4();
		if (!L_13)
		{
			goto IL_0091;
		}
	}
	{
		Rigidbody2D_t939494601 * L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_14, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0091;
		}
	}
	{
		Rigidbody2D_t939494601 * L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_17 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_18 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m2898400508(L_16, L_18, /*hidden argument*/NULL);
		Rigidbody2D_t939494601 * L_19 = V_0;
		Rigidbody2D_set_angularVelocity_m2791812150(L_19, (0.0f), /*hidden argument*/NULL);
	}

IL_0091:
	{
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TimedSelfDestruct::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TimedSelfDestruct__ctor_m306186609 (TimedSelfDestruct_t3957973201 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimedSelfDestruct::Start()
extern "C" IL2CPP_METHOD_ATTR void TimedSelfDestruct_Start_m4128724077 (TimedSelfDestruct_t3957973201 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimedSelfDestruct_Start_m4128724077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_timeToDestruction_2();
		MonoBehaviour_Invoke_m4227543964(__this, _stringLiteral2460463356, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimedSelfDestruct::DestroyMe()
extern "C" IL2CPP_METHOD_ATTR void TimedSelfDestruct_DestroyMe_m477135811 (TimedSelfDestruct_t3957973201 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimedSelfDestruct_DestroyMe_m477135811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIItemScript::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UIItemScript__ctor_m446408511 (UIItemScript_t1692532892 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIItemScript::ShowNumber(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIItemScript_ShowNumber_m4287243492 (UIItemScript_t1692532892 * __this, int32_t ___n0, const RuntimeMethod* method)
{
	{
		Text_t1901882714 * L_0 = __this->get_resourceAmount_3();
		String_t* L_1 = Int32_ToString_m141394615((&___n0), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void UIItemScript::DisplayIcon(UnityEngine.Sprite)
extern "C" IL2CPP_METHOD_ATTR void UIItemScript_DisplayIcon_m618755502 (UIItemScript_t1692532892 * __this, Sprite_t280657092 * ___s0, const RuntimeMethod* method)
{
	{
		Image_t2670269651 * L_0 = __this->get_resourceIcon_2();
		Sprite_t280657092 * L_1 = ___s0;
		Image_set_sprite_m2369174689(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIScript::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UIScript__ctor_m1214042822 (UIScript_t1195624422 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIScript__ctor_m1214042822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_scoreToWin_4(5);
		__this->set_numberLabels_5(((TextU5BU5D_t422084607*)SZArrayNew(TextU5BU5D_t422084607_il2cpp_TypeInfo_var, (uint32_t)2)));
		__this->set_scores_14(((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)2)));
		__this->set_playersHealth_15(((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)2)));
		Dictionary_2_t4045795053 * L_0 = (Dictionary_2_t4045795053 *)il2cpp_codegen_object_new(Dictionary_2_t4045795053_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m790289403(L_0, /*hidden argument*/Dictionary_2__ctor_m790289403_RuntimeMethod_var);
		__this->set_resourcesDict_16(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIScript::Start()
extern "C" IL2CPP_METHOD_ATTR void UIScript_Start_m923733378 (UIScript_t1195624422 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIScript_Start_m923733378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_numberOfPlayers_2();
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		goto IL_0092;
	}

IL_0010:
	{
		int32_t L_1 = __this->get_gameType_3();
		if (L_1)
		{
			goto IL_0074;
		}
	}
	{
		Text_t1901882714 * L_2 = __this->get_rightLabel_6();
		V_0 = _stringLiteral1512031223;
		Text_t1901882714 * L_3 = __this->get_leftLabel_7();
		String_t* L_4 = V_0;
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_4);
		String_t* L_5 = V_0;
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_5);
		TextU5BU5D_t422084607* L_6 = __this->get_numberLabels_5();
		int32_t L_7 = 0;
		Text_t1901882714 * L_8 = (L_6)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_7));
		V_0 = _stringLiteral3452614544;
		TextU5BU5D_t422084607* L_9 = __this->get_numberLabels_5();
		int32_t L_10 = 1;
		Text_t1901882714 * L_11 = (L_9)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_10));
		String_t* L_12 = V_0;
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_12);
		String_t* L_13 = V_0;
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_13);
		Int32U5BU5D_t385246372* L_14 = __this->get_scores_14();
		Int32U5BU5D_t385246372* L_15 = __this->get_scores_14();
		int32_t L_16 = 0;
		V_1 = L_16;
		(L_15)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (int32_t)L_16);
		int32_t L_17 = V_1;
		(L_14)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (int32_t)L_17);
		goto IL_0092;
	}

IL_0074:
	{
		Text_t1901882714 * L_18 = __this->get_rightLabel_6();
		V_0 = _stringLiteral3148363280;
		Text_t1901882714 * L_19 = __this->get_leftLabel_7();
		String_t* L_20 = V_0;
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_19, L_20);
		String_t* L_21 = V_0;
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_21);
	}

IL_0092:
	{
		return;
	}
}
// System.Void UIScript::AddOnePoint(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIScript_AddOnePoint_m3793197548 (UIScript_t1195624422 * __this, int32_t ___playerNumber0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___playerNumber0;
		UIScript_AddPoints_m1714476938(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIScript::AddPoints(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIScript_AddPoints_m1714476938 (UIScript_t1195624422 * __this, int32_t ___playerNumber0, int32_t ___amount1, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t385246372* L_0 = __this->get_scores_14();
		int32_t L_1 = ___playerNumber0;
		int32_t* L_2 = ((L_0)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_1)));
		int32_t L_3 = ___amount1;
		*((int32_t*)(L_2)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_2)), (int32_t)L_3));
		int32_t L_4 = __this->get_numberOfPlayers_2();
		if (L_4)
		{
			goto IL_0045;
		}
	}
	{
		TextU5BU5D_t422084607* L_5 = __this->get_numberLabels_5();
		int32_t L_6 = 1;
		Text_t1901882714 * L_7 = (L_5)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_6));
		Int32U5BU5D_t385246372* L_8 = __this->get_scores_14();
		int32_t L_9 = ___playerNumber0;
		String_t* L_10 = Int32_ToString_m141394615(((L_8)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_9))), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_10);
		goto IL_0069;
	}

IL_0045:
	{
		TextU5BU5D_t422084607* L_11 = __this->get_numberLabels_5();
		int32_t L_12 = ___playerNumber0;
		int32_t L_13 = L_12;
		Text_t1901882714 * L_14 = (L_11)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_13));
		Int32U5BU5D_t385246372* L_15 = __this->get_scores_14();
		int32_t L_16 = ___playerNumber0;
		String_t* L_17 = Int32_ToString_m141394615(((L_15)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_16))), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_14, L_17);
	}

IL_0069:
	{
		int32_t L_18 = __this->get_gameType_3();
		if (L_18)
		{
			goto IL_008e;
		}
	}
	{
		Int32U5BU5D_t385246372* L_19 = __this->get_scores_14();
		int32_t L_20 = ___playerNumber0;
		int32_t L_21 = L_20;
		int32_t L_22 = (L_19)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_21));
		int32_t L_23 = __this->get_scoreToWin_4();
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_008e;
		}
	}
	{
		int32_t L_24 = ___playerNumber0;
		UIScript_GameWon_m3372413357(__this, L_24, /*hidden argument*/NULL);
	}

IL_008e:
	{
		return;
	}
}
// System.Void UIScript::RemoveOnePoint(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIScript_RemoveOnePoint_m3624050257 (UIScript_t1195624422 * __this, int32_t ___playerNumber0, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t385246372* L_0 = __this->get_scores_14();
		int32_t L_1 = ___playerNumber0;
		int32_t* L_2 = ((L_0)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_1)));
		*((int32_t*)(L_2)) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(*((int32_t*)L_2)), (int32_t)1));
		int32_t L_3 = __this->get_numberOfPlayers_2();
		if (L_3)
		{
			goto IL_0045;
		}
	}
	{
		TextU5BU5D_t422084607* L_4 = __this->get_numberLabels_5();
		int32_t L_5 = 1;
		Text_t1901882714 * L_6 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_5));
		Int32U5BU5D_t385246372* L_7 = __this->get_scores_14();
		int32_t L_8 = ___playerNumber0;
		String_t* L_9 = Int32_ToString_m141394615(((L_7)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_8))), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_9);
		goto IL_0069;
	}

IL_0045:
	{
		TextU5BU5D_t422084607* L_10 = __this->get_numberLabels_5();
		int32_t L_11 = ___playerNumber0;
		int32_t L_12 = L_11;
		Text_t1901882714 * L_13 = (L_10)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_12));
		Int32U5BU5D_t385246372* L_14 = __this->get_scores_14();
		int32_t L_15 = ___playerNumber0;
		String_t* L_16 = Int32_ToString_m141394615(((L_14)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_15))), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_13, L_16);
	}

IL_0069:
	{
		return;
	}
}
// System.Void UIScript::GameWon(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIScript_GameWon_m3372413357 (UIScript_t1195624422 * __this, int32_t ___playerNumber0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIScript_GameWon_m3372413357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_gameOver_17();
		if (L_0)
		{
			goto IL_004f;
		}
	}
	{
		__this->set_gameOver_17((bool)1);
		Text_t1901882714 * L_1 = __this->get_winLabel_8();
		int32_t L_2 = ___playerNumber0;
		int32_t L_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1));
		___playerNumber0 = L_3;
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral709948148, L_5, _stringLiteral48364148, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_6);
		GameObject_t1113636619 * L_7 = __this->get_statsPanel_9();
		GameObject_SetActive_m796801857(L_7, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_8 = __this->get_winPanel_11();
		GameObject_SetActive_m796801857(L_8, (bool)1, /*hidden argument*/NULL);
	}

IL_004f:
	{
		return;
	}
}
// System.Void UIScript::GameOver(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIScript_GameOver_m2568581351 (UIScript_t1195624422 * __this, int32_t ___playerNumber0, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_gameOver_17();
		if (L_0)
		{
			goto IL_002a;
		}
	}
	{
		__this->set_gameOver_17((bool)1);
		GameObject_t1113636619 * L_1 = __this->get_statsPanel_9();
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_gameOverPanel_10();
		GameObject_SetActive_m796801857(L_2, (bool)1, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void UIScript::SetHealth(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIScript_SetHealth_m2132392576 (UIScript_t1195624422 * __this, int32_t ___amount0, int32_t ___playerNumber1, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t385246372* L_0 = __this->get_playersHealth_15();
		int32_t L_1 = ___playerNumber1;
		int32_t L_2 = ___amount0;
		(L_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_1), (int32_t)L_2);
		TextU5BU5D_t422084607* L_3 = __this->get_numberLabels_5();
		int32_t L_4 = ___playerNumber1;
		int32_t L_5 = L_4;
		Text_t1901882714 * L_6 = (L_3)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_5));
		Int32U5BU5D_t385246372* L_7 = __this->get_playersHealth_15();
		int32_t L_8 = ___playerNumber1;
		String_t* L_9 = Int32_ToString_m141394615(((L_7)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_8))), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_9);
		return;
	}
}
// System.Void UIScript::ChangeHealth(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIScript_ChangeHealth_m2753858397 (UIScript_t1195624422 * __this, int32_t ___change0, int32_t ___playerNumber1, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t385246372* L_0 = __this->get_playersHealth_15();
		int32_t L_1 = ___playerNumber1;
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_2));
		int32_t L_4 = ___change0;
		int32_t L_5 = ___playerNumber1;
		UIScript_SetHealth_m2132392576(__this, ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)L_4)), L_5, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_gameType_3();
		if ((((int32_t)L_6) == ((int32_t)2)))
		{
			goto IL_0032;
		}
	}
	{
		Int32U5BU5D_t385246372* L_7 = __this->get_playersHealth_15();
		int32_t L_8 = ___playerNumber1;
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_9));
		if ((((int32_t)L_10) > ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_11 = ___playerNumber1;
		UIScript_GameOver_m2568581351(__this, L_11, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void UIScript::AddResource(System.Int32,System.Int32,UnityEngine.Sprite)
extern "C" IL2CPP_METHOD_ATTR void UIScript_AddResource_m2508601531 (UIScript_t1195624422 * __this, int32_t ___resourceType0, int32_t ___pickedUpAmount1, Sprite_t280657092 * ___graphics2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIScript_AddResource_m2508601531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	UIItemScript_t1692532892 * V_1 = NULL;
	{
		Dictionary_2_t4045795053 * L_0 = __this->get_resourcesDict_16();
		int32_t L_1 = ___resourceType0;
		bool L_2 = Dictionary_2_ContainsKey_m537408582(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m537408582_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0053;
		}
	}
	{
		Dictionary_2_t4045795053 * L_3 = __this->get_resourcesDict_16();
		int32_t L_4 = ___resourceType0;
		ResourceStruct_t862114426 * L_5 = Dictionary_2_get_Item_m3023042062(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m3023042062_RuntimeMethod_var);
		int32_t L_6 = L_5->get_amount_0();
		int32_t L_7 = ___pickedUpAmount1;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)L_7));
		Dictionary_2_t4045795053 * L_8 = __this->get_resourcesDict_16();
		int32_t L_9 = ___resourceType0;
		ResourceStruct_t862114426 * L_10 = Dictionary_2_get_Item_m3023042062(L_8, L_9, /*hidden argument*/Dictionary_2_get_Item_m3023042062_RuntimeMethod_var);
		UIItemScript_t1692532892 * L_11 = L_10->get_UIItem_1();
		int32_t L_12 = V_0;
		UIItemScript_ShowNumber_m4287243492(L_11, L_12, /*hidden argument*/NULL);
		Dictionary_2_t4045795053 * L_13 = __this->get_resourcesDict_16();
		int32_t L_14 = ___resourceType0;
		ResourceStruct_t862114426 * L_15 = Dictionary_2_get_Item_m3023042062(L_13, L_14, /*hidden argument*/Dictionary_2_get_Item_m3023042062_RuntimeMethod_var);
		int32_t L_16 = V_0;
		L_15->set_amount_0(L_16);
		goto IL_00b7;
	}

IL_0053:
	{
		GameObject_t1113636619 * L_17 = __this->get_resourceItemPrefab_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_18 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_17, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		UIItemScript_t1692532892 * L_19 = GameObject_GetComponent_TisUIItemScript_t1692532892_m3238252138(L_18, /*hidden argument*/GameObject_GetComponent_TisUIItemScript_t1692532892_m3238252138_RuntimeMethod_var);
		V_1 = L_19;
		UIItemScript_t1692532892 * L_20 = V_1;
		Transform_t3600365921 * L_21 = Component_get_transform_m3162698980(L_20, /*hidden argument*/NULL);
		Transform_t3600365921 * L_22 = __this->get_inventory_12();
		Transform_SetParent_m273471670(L_21, L_22, (bool)0, /*hidden argument*/NULL);
		Dictionary_2_t4045795053 * L_23 = __this->get_resourcesDict_16();
		int32_t L_24 = ___resourceType0;
		int32_t L_25 = ___pickedUpAmount1;
		UIItemScript_t1692532892 * L_26 = V_1;
		ResourceStruct_t862114426 * L_27 = (ResourceStruct_t862114426 *)il2cpp_codegen_object_new(ResourceStruct_t862114426_il2cpp_TypeInfo_var);
		ResourceStruct__ctor_m3839822749(L_27, L_25, L_26, /*hidden argument*/NULL);
		Dictionary_2_Add_m1464489386(L_23, L_24, L_27, /*hidden argument*/Dictionary_2_Add_m1464489386_RuntimeMethod_var);
		Dictionary_2_t4045795053 * L_28 = __this->get_resourcesDict_16();
		int32_t L_29 = ___resourceType0;
		ResourceStruct_t862114426 * L_30 = Dictionary_2_get_Item_m3023042062(L_28, L_29, /*hidden argument*/Dictionary_2_get_Item_m3023042062_RuntimeMethod_var);
		UIItemScript_t1692532892 * L_31 = L_30->get_UIItem_1();
		int32_t L_32 = ___pickedUpAmount1;
		UIItemScript_ShowNumber_m4287243492(L_31, L_32, /*hidden argument*/NULL);
		Dictionary_2_t4045795053 * L_33 = __this->get_resourcesDict_16();
		int32_t L_34 = ___resourceType0;
		ResourceStruct_t862114426 * L_35 = Dictionary_2_get_Item_m3023042062(L_33, L_34, /*hidden argument*/Dictionary_2_get_Item_m3023042062_RuntimeMethod_var);
		UIItemScript_t1692532892 * L_36 = L_35->get_UIItem_1();
		Sprite_t280657092 * L_37 = ___graphics2;
		UIItemScript_DisplayIcon_m618755502(L_36, L_37, /*hidden argument*/NULL);
	}

IL_00b7:
	{
		return;
	}
}
// System.Boolean UIScript::CheckIfHasResources(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool UIScript_CheckIfHasResources_m685094672 (UIScript_t1195624422 * __this, int32_t ___resourceType0, int32_t ___amountNeeded1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIScript_CheckIfHasResources_m685094672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t4045795053 * L_0 = __this->get_resourcesDict_16();
		int32_t L_1 = ___resourceType0;
		bool L_2 = Dictionary_2_ContainsKey_m537408582(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m537408582_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Dictionary_2_t4045795053 * L_3 = __this->get_resourcesDict_16();
		int32_t L_4 = ___resourceType0;
		ResourceStruct_t862114426 * L_5 = Dictionary_2_get_Item_m3023042062(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m3023042062_RuntimeMethod_var);
		int32_t L_6 = L_5->get_amount_0();
		int32_t L_7 = ___amountNeeded1;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_002a;
		}
	}
	{
		return (bool)1;
	}

IL_002a:
	{
		return (bool)0;
	}

IL_002c:
	{
		return (bool)0;
	}
}
// System.Void UIScript::ConsumeResource(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIScript_ConsumeResource_m3280961331 (UIScript_t1195624422 * __this, int32_t ___resourceType0, int32_t ___amountNeeded1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIScript_ConsumeResource_m3280961331_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t4045795053 * L_0 = __this->get_resourcesDict_16();
		int32_t L_1 = ___resourceType0;
		ResourceStruct_t862114426 * L_2 = Dictionary_2_get_Item_m3023042062(L_0, L_1, /*hidden argument*/Dictionary_2_get_Item_m3023042062_RuntimeMethod_var);
		ResourceStruct_t862114426 * L_3 = L_2;
		int32_t L_4 = L_3->get_amount_0();
		int32_t L_5 = ___amountNeeded1;
		L_3->set_amount_0(((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)L_5)));
		Dictionary_2_t4045795053 * L_6 = __this->get_resourcesDict_16();
		int32_t L_7 = ___resourceType0;
		ResourceStruct_t862114426 * L_8 = Dictionary_2_get_Item_m3023042062(L_6, L_7, /*hidden argument*/Dictionary_2_get_Item_m3023042062_RuntimeMethod_var);
		UIItemScript_t1692532892 * L_9 = L_8->get_UIItem_1();
		Dictionary_2_t4045795053 * L_10 = __this->get_resourcesDict_16();
		int32_t L_11 = ___resourceType0;
		ResourceStruct_t862114426 * L_12 = Dictionary_2_get_Item_m3023042062(L_10, L_11, /*hidden argument*/Dictionary_2_get_Item_m3023042062_RuntimeMethod_var);
		int32_t L_13 = L_12->get_amount_0();
		UIItemScript_ShowNumber_m4287243492(L_9, L_13, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Utils::.cctor()
extern "C" IL2CPP_METHOD_ATTR void Utils__cctor_m1792585047 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils__cctor_m1792585047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t3648964284 * L_0 = Resources_Load_TisMesh_t3648964284_m3514705491(NULL /*static, unused*/, _stringLiteral1095709682, /*hidden argument*/Resources_Load_TisMesh_t3648964284_m3514705491_RuntimeMethod_var);
		((Utils_t1444179947_StaticFields*)il2cpp_codegen_static_fields_for(Utils_t1444179947_il2cpp_TypeInfo_var))->set_moveArrowMesh_0(L_0);
		Mesh_t3648964284 * L_1 = Resources_Load_TisMesh_t3648964284_m3514705491(NULL /*static, unused*/, _stringLiteral724398280, /*hidden argument*/Resources_Load_TisMesh_t3648964284_m3514705491_RuntimeMethod_var);
		((Utils_t1444179947_StaticFields*)il2cpp_codegen_static_fields_for(Utils_t1444179947_il2cpp_TypeInfo_var))->set_shootArrowMesh_1(L_1);
		Mesh_t3648964284 * L_2 = Resources_Load_TisMesh_t3648964284_m3514705491(NULL /*static, unused*/, _stringLiteral1284005273, /*hidden argument*/Resources_Load_TisMesh_t3648964284_m3514705491_RuntimeMethod_var);
		((Utils_t1444179947_StaticFields*)il2cpp_codegen_static_fields_for(Utils_t1444179947_il2cpp_TypeInfo_var))->set_rotateArrowMesh_2(L_2);
		return;
	}
}
// System.Void Utils::SetAxisTowards(Enums/Directions,UnityEngine.Transform,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void Utils_SetAxisTowards_m1999213583 (RuntimeObject * __this /* static, unused */, int32_t ___axis0, Transform_t3600365921 * ___t1, Vector2_t2156229523  ___direction2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_SetAxisTowards_m1999213583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___axis0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0042;
			}
			case 2:
			{
				goto IL_002c;
			}
			case 3:
			{
				goto IL_0053;
			}
		}
	}
	{
		goto IL_0069;
	}

IL_001b:
	{
		Transform_t3600365921 * L_1 = ___t1;
		Vector2_t2156229523  L_2 = ___direction2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Transform_set_up_m3321958190(L_1, L_3, /*hidden argument*/NULL);
		goto IL_0069;
	}

IL_002c:
	{
		Transform_t3600365921 * L_4 = ___t1;
		Vector2_t2156229523  L_5 = ___direction2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_6 = Vector2_op_UnaryNegation_m2172448356(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Transform_set_up_m3321958190(L_4, L_7, /*hidden argument*/NULL);
		goto IL_0069;
	}

IL_0042:
	{
		Transform_t3600365921 * L_8 = ___t1;
		Vector2_t2156229523  L_9 = ___direction2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_10 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Transform_set_right_m1787339266(L_8, L_10, /*hidden argument*/NULL);
		goto IL_0069;
	}

IL_0053:
	{
		Transform_t3600365921 * L_11 = ___t1;
		Vector2_t2156229523  L_12 = ___direction2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_13 = Vector2_op_UnaryNegation_m2172448356(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		Vector3_t3722313464  L_14 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		Transform_set_right_m1787339266(L_11, L_14, /*hidden argument*/NULL);
		goto IL_0069;
	}

IL_0069:
	{
		return;
	}
}
// UnityEngine.Vector2 Utils::GetVectorFromAxis(Enums/Axes)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Utils_GetVectorFromAxis_m3396922722 (RuntimeObject * __this /* static, unused */, int32_t ___axis0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_GetVectorFromAxis_m3396922722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___axis0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_1 = Vector2_get_right_m1027081661(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_1;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Vector2_get_up_m2647420593(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector2 Utils::GetVector2FromVector3(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Utils_GetVector2FromVector3_m933791627 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___input0, const RuntimeMethod* method)
{
	{
		float L_0 = (&___input0)->get_x_1();
		float L_1 = (&___input0)->get_y_2();
		Vector2_t2156229523  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3970636864((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Utils::DrawMoveArrowGizmo(UnityEngine.Vector3,UnityEngine.Vector2,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Utils_DrawMoveArrowGizmo_m4283051242 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___position0, Vector2_t2156229523  ___direction1, float ___extraAngle2, float ___scale3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_DrawMoveArrowGizmo_m4283051242_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Mesh_t3648964284 * L_0 = ((Utils_t1444179947_StaticFields*)il2cpp_codegen_static_fields_for(Utils_t1444179947_il2cpp_TypeInfo_var))->get_moveArrowMesh_0();
		Vector3_t3722313464  L_1 = ___position0;
		Vector2_t2156229523  L_2 = ___direction1;
		float L_3 = ___extraAngle2;
		float L_4 = ___scale3;
		Utils_DrawGizmo_m3328925645(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Utils::DrawShootArrowGizmo(UnityEngine.Vector3,UnityEngine.Vector2,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Utils_DrawShootArrowGizmo_m2541881908 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___position0, Vector2_t2156229523  ___direction1, float ___extraAngle2, float ___scale3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_DrawShootArrowGizmo_m2541881908_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Mesh_t3648964284 * L_0 = ((Utils_t1444179947_StaticFields*)il2cpp_codegen_static_fields_for(Utils_t1444179947_il2cpp_TypeInfo_var))->get_shootArrowMesh_1();
		Vector3_t3722313464  L_1 = ___position0;
		Vector2_t2156229523  L_2 = ___direction1;
		float L_3 = ___extraAngle2;
		float L_4 = ___scale3;
		Utils_DrawGizmo_m3328925645(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Utils::DrawRotateArrowGizmo(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Utils_DrawRotateArrowGizmo_m3937198024 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___position0, float ___strength1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_DrawRotateArrowGizmo_m3937198024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2555686324  L_0 = Color_get_green_m490390750(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m3399737545(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Mesh_t3648964284 * L_1 = ((Utils_t1444179947_StaticFields*)il2cpp_codegen_static_fields_for(Utils_t1444179947_il2cpp_TypeInfo_var))->get_rotateArrowMesh_2();
		Vector3_t3722313464  L_2 = ___position0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_3 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = ___strength1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_5 = Mathf_Sign_m3457838305(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = ___strength1;
		float L_7 = Mathf_Sign_m3457838305(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m3353183577((&L_8), L_5, (1.0f), L_7, /*hidden argument*/NULL);
		Gizmos_DrawMesh_m113137071(NULL /*static, unused*/, L_1, L_2, L_3, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Utils::DrawGizmo(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Vector2,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Utils_DrawGizmo_m3328925645 (RuntimeObject * __this /* static, unused */, Mesh_t3648964284 * ___meshToDraw0, Vector3_t3722313464  ___position1, Vector2_t2156229523  ___direction2, float ___extraAngle3, float ___scale4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_DrawGizmo_m3328925645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Color_t2555686324  L_0 = Color_get_green_m490390750(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m3399737545(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_1 = ___direction2;
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		float L_2 = Utils_Angle_m3844311520(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = ___scale4;
		if ((!(((float)L_3) == ((float)(0.0f)))))
		{
			goto IL_0026;
		}
	}
	{
		float L_4 = Vector2_get_magnitude_m2752892833((&___direction2), /*hidden argument*/NULL);
		___scale4 = L_4;
	}

IL_0026:
	{
		Mesh_t3648964284 * L_5 = ___meshToDraw0;
		Vector3_t3722313464  L_6 = ___position1;
		float L_7 = V_0;
		float L_8 = ___extraAngle3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_9 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_10 = Quaternion_AngleAxis_m1767165696(NULL /*static, unused*/, ((float)il2cpp_codegen_add((float)L_7, (float)L_8)), L_9, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Vector3_get_one_m1629952498(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_12 = ___scale4;
		Vector3_t3722313464  L_13 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Gizmos_DrawMesh_m113137071(NULL /*static, unused*/, L_5, L_6, L_10, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Utils::Angle(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR float Utils_Angle_m3844311520 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___inputVector0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_Angle_m3844311520_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = (&___inputVector0)->get_x_0();
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0037;
		}
	}
	{
		float L_1 = (&___inputVector0)->get_x_0();
		float L_2 = (&___inputVector0)->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_3 = atan2f(L_1, L_2);
		return ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_3, (float)(57.29578f))), (float)(-1.0f))), (float)(360.0f)));
	}

IL_0037:
	{
		float L_4 = (&___inputVector0)->get_x_0();
		float L_5 = (&___inputVector0)->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_6 = atan2f(L_4, L_5);
		return ((float)il2cpp_codegen_multiply((float)((-L_6)), (float)(57.29578f)));
	}
}
// System.Void Utils::Collider2DDialogWindow(UnityEngine.GameObject,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Utils_Collider2DDialogWindow_m3028026175 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___gameObjectRef0, bool ___makeItTrigger1, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Wander::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Wander__ctor_m4091206792 (Wander_t651605530 * __this, const RuntimeMethod* method)
{
	{
		__this->set_speed_3((1.0f));
		__this->set_directionChangeInterval_4((3.0f));
		__this->set_keepNearStartingPoint_5((bool)1);
		Physics2DObject__ctor_m38296749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Wander::Start()
extern "C" IL2CPP_METHOD_ATTR void Wander_Start_m280845394 (Wander_t651605530 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_directionChangeInterval_4();
		if ((!(((float)L_0) < ((float)(0.1f)))))
		{
			goto IL_001b;
		}
	}
	{
		__this->set_directionChangeInterval_4((0.1f));
	}

IL_001b:
	{
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		__this->set_startingPoint_9(L_2);
		RuntimeObject* L_3 = Wander_ChangeDirection_m1837724728(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Wander::ChangeDirection()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Wander_ChangeDirection_m1837724728 (Wander_t651605530 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Wander_ChangeDirection_m1837724728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CChangeDirectionU3Ec__Iterator0_t1842818970 * V_0 = NULL;
	{
		U3CChangeDirectionU3Ec__Iterator0_t1842818970 * L_0 = (U3CChangeDirectionU3Ec__Iterator0_t1842818970 *)il2cpp_codegen_object_new(U3CChangeDirectionU3Ec__Iterator0_t1842818970_il2cpp_TypeInfo_var);
		U3CChangeDirectionU3Ec__Iterator0__ctor_m557163208(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CChangeDirectionU3Ec__Iterator0_t1842818970 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CChangeDirectionU3Ec__Iterator0_t1842818970 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Wander::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void Wander_FixedUpdate_m2471657602 (Wander_t651605530 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Wander_FixedUpdate_m2471657602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t939494601 * L_0 = ((Physics2DObject_t3026219368 *)__this)->get_rigidbody2D_2();
		Vector2_t2156229523  L_1 = __this->get_direction_8();
		float L_2 = __this->get_speed_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_3 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Rigidbody2D_AddForce_m1113499586(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Wander/<ChangeDirection>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CChangeDirectionU3Ec__Iterator0__ctor_m557163208 (U3CChangeDirectionU3Ec__Iterator0_t1842818970 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Wander/<ChangeDirection>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CChangeDirectionU3Ec__Iterator0_MoveNext_m2145628392 (U3CChangeDirectionU3Ec__Iterator0_t1842818970 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CChangeDirectionU3Ec__Iterator0_MoveNext_m2145628392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	float V_1 = 0.0f;
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0121;
			}
		}
	}
	{
		goto IL_012d;
	}

IL_0021:
	{
		Wander_t651605530 * L_2 = __this->get_U24this_0();
		Vector2_t2156229523  L_3 = Random_get_insideUnitCircle_m1267895911(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_2->set_direction_8(L_3);
		Wander_t651605530 * L_4 = __this->get_U24this_0();
		bool L_5 = L_4->get_keepNearStartingPoint_5();
		if (!L_5)
		{
			goto IL_00c1;
		}
	}
	{
		Wander_t651605530 * L_6 = __this->get_U24this_0();
		Vector3_t3722313464  L_7 = L_6->get_startingPoint_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_8 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Wander_t651605530 * L_9 = __this->get_U24this_0();
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(L_9, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Transform_get_position_m36019626(L_10, /*hidden argument*/NULL);
		Vector2_t2156229523  L_12 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		float L_13 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_8, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		float L_14 = V_1;
		Wander_t651605530 * L_15 = __this->get_U24this_0();
		float L_16 = L_15->get_speed_3();
		if ((!(((float)L_14) > ((float)((float)il2cpp_codegen_add((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)L_16, (float)(0.1f)))))))))
		{
			goto IL_00c1;
		}
	}
	{
		Wander_t651605530 * L_17 = __this->get_U24this_0();
		Wander_t651605530 * L_18 = __this->get_U24this_0();
		Vector3_t3722313464  L_19 = L_18->get_startingPoint_9();
		Wander_t651605530 * L_20 = __this->get_U24this_0();
		Transform_t3600365921 * L_21 = Component_get_transform_m3162698980(L_20, /*hidden argument*/NULL);
		Vector3_t3722313464  L_22 = Transform_get_position_m36019626(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_23 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_19, L_22, /*hidden argument*/NULL);
		V_2 = L_23;
		Vector3_t3722313464  L_24 = Vector3_get_normalized_m2454957984((&V_2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_25 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		L_17->set_direction_8(L_25);
	}

IL_00c1:
	{
		Wander_t651605530 * L_26 = __this->get_U24this_0();
		bool L_27 = L_26->get_orientToDirection_6();
		if (!L_27)
		{
			goto IL_00f7;
		}
	}
	{
		Wander_t651605530 * L_28 = __this->get_U24this_0();
		int32_t L_29 = L_28->get_lookAxis_7();
		Wander_t651605530 * L_30 = __this->get_U24this_0();
		Transform_t3600365921 * L_31 = Component_get_transform_m3162698980(L_30, /*hidden argument*/NULL);
		Wander_t651605530 * L_32 = __this->get_U24this_0();
		Vector2_t2156229523  L_33 = L_32->get_direction_8();
		IL2CPP_RUNTIME_CLASS_INIT(Utils_t1444179947_il2cpp_TypeInfo_var);
		Utils_SetAxisTowards_m1999213583(NULL /*static, unused*/, L_29, L_31, L_33, /*hidden argument*/NULL);
	}

IL_00f7:
	{
		Wander_t651605530 * L_34 = __this->get_U24this_0();
		float L_35 = L_34->get_directionChangeInterval_4();
		WaitForSeconds_t1699091251 * L_36 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_36, L_35, /*hidden argument*/NULL);
		__this->set_U24current_1(L_36);
		bool L_37 = __this->get_U24disposing_2();
		if (L_37)
		{
			goto IL_011c;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_011c:
	{
		goto IL_012f;
	}

IL_0121:
	{
		goto IL_0021;
	}
	// Dead block : IL_0126: ldarg.0

IL_012d:
	{
		return (bool)0;
	}

IL_012f:
	{
		return (bool)1;
	}
}
// System.Object Wander/<ChangeDirection>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CChangeDirectionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3803027382 (U3CChangeDirectionU3Ec__Iterator0_t1842818970 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Wander/<ChangeDirection>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CChangeDirectionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1075338510 (U3CChangeDirectionU3Ec__Iterator0_t1842818970 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Wander/<ChangeDirection>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CChangeDirectionU3Ec__Iterator0_Dispose_m4226444741 (U3CChangeDirectionU3Ec__Iterator0_t1842818970 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Wander/<ChangeDirection>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CChangeDirectionU3Ec__Iterator0_Reset_m401157713 (U3CChangeDirectionU3Ec__Iterator0_t1842818970 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CChangeDirectionU3Ec__Iterator0_Reset_m401157713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0,U3CChangeDirectionU3Ec__Iterator0_Reset_m401157713_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif

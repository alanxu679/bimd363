﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Wander
struct Wander_t651605530;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t662546754;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// System.String
struct String_t;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CCHANGEDIRECTIONU3EC__ITERATOR0_T1842818970_H
#define U3CCHANGEDIRECTIONU3EC__ITERATOR0_T1842818970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wander/<ChangeDirection>c__Iterator0
struct  U3CChangeDirectionU3Ec__Iterator0_t1842818970  : public RuntimeObject
{
public:
	// Wander Wander/<ChangeDirection>c__Iterator0::$this
	Wander_t651605530 * ___U24this_0;
	// System.Object Wander/<ChangeDirection>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Wander/<ChangeDirection>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Wander/<ChangeDirection>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CChangeDirectionU3Ec__Iterator0_t1842818970, ___U24this_0)); }
	inline Wander_t651605530 * get_U24this_0() const { return ___U24this_0; }
	inline Wander_t651605530 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Wander_t651605530 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CChangeDirectionU3Ec__Iterator0_t1842818970, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CChangeDirectionU3Ec__Iterator0_t1842818970, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CChangeDirectionU3Ec__Iterator0_t1842818970, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGEDIRECTIONU3EC__ITERATOR0_T1842818970_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef DIRECTIONS_T3759223838_H
#define DIRECTIONS_T3759223838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enums/Directions
struct  Directions_t3759223838 
{
public:
	// System.Int32 Enums/Directions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Directions_t3759223838, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONS_T3759223838_H
#ifndef MOVEMENTTYPE_T2977684614_H
#define MOVEMENTTYPE_T2977684614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enums/MovementType
struct  MovementType_t2977684614 
{
public:
	// System.Int32 Enums/MovementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MovementType_t2977684614, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTTYPE_T2977684614_H
#ifndef KEYGROUPS_T2888070947_H
#define KEYGROUPS_T2888070947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enums/KeyGroups
struct  KeyGroups_t2888070947 
{
public:
	// System.Int32 Enums/KeyGroups::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyGroups_t2888070947, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYGROUPS_T2888070947_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef AXES_T766252756_H
#define AXES_T766252756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enums/Axes
struct  Axes_t766252756 
{
public:
	// System.Int32 Enums/Axes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axes_t766252756, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXES_T766252756_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef STOPPER_T1976456672_H
#define STOPPER_T1976456672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Stopper
struct  Stopper_t1976456672  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody2D Stopper::rb2D
	Rigidbody2D_t939494601 * ___rb2D_2;
	// UnityEngine.SpriteRenderer Stopper::renderer
	SpriteRenderer_t3235626157 * ___renderer_3;
	// UnityEngine.CircleCollider2D Stopper::collider
	CircleCollider2D_t662546754 * ___collider_4;
	// System.Single Stopper::mass
	float ___mass_5;
	// UnityEngine.Transform Stopper::holder
	Transform_t3600365921 * ___holder_6;
	// UnityEngine.Transform Stopper::player
	Transform_t3600365921 * ___player_7;
	// System.Boolean Stopper::live
	bool ___live_9;

public:
	inline static int32_t get_offset_of_rb2D_2() { return static_cast<int32_t>(offsetof(Stopper_t1976456672, ___rb2D_2)); }
	inline Rigidbody2D_t939494601 * get_rb2D_2() const { return ___rb2D_2; }
	inline Rigidbody2D_t939494601 ** get_address_of_rb2D_2() { return &___rb2D_2; }
	inline void set_rb2D_2(Rigidbody2D_t939494601 * value)
	{
		___rb2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___rb2D_2), value);
	}

	inline static int32_t get_offset_of_renderer_3() { return static_cast<int32_t>(offsetof(Stopper_t1976456672, ___renderer_3)); }
	inline SpriteRenderer_t3235626157 * get_renderer_3() const { return ___renderer_3; }
	inline SpriteRenderer_t3235626157 ** get_address_of_renderer_3() { return &___renderer_3; }
	inline void set_renderer_3(SpriteRenderer_t3235626157 * value)
	{
		___renderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___renderer_3), value);
	}

	inline static int32_t get_offset_of_collider_4() { return static_cast<int32_t>(offsetof(Stopper_t1976456672, ___collider_4)); }
	inline CircleCollider2D_t662546754 * get_collider_4() const { return ___collider_4; }
	inline CircleCollider2D_t662546754 ** get_address_of_collider_4() { return &___collider_4; }
	inline void set_collider_4(CircleCollider2D_t662546754 * value)
	{
		___collider_4 = value;
		Il2CppCodeGenWriteBarrier((&___collider_4), value);
	}

	inline static int32_t get_offset_of_mass_5() { return static_cast<int32_t>(offsetof(Stopper_t1976456672, ___mass_5)); }
	inline float get_mass_5() const { return ___mass_5; }
	inline float* get_address_of_mass_5() { return &___mass_5; }
	inline void set_mass_5(float value)
	{
		___mass_5 = value;
	}

	inline static int32_t get_offset_of_holder_6() { return static_cast<int32_t>(offsetof(Stopper_t1976456672, ___holder_6)); }
	inline Transform_t3600365921 * get_holder_6() const { return ___holder_6; }
	inline Transform_t3600365921 ** get_address_of_holder_6() { return &___holder_6; }
	inline void set_holder_6(Transform_t3600365921 * value)
	{
		___holder_6 = value;
		Il2CppCodeGenWriteBarrier((&___holder_6), value);
	}

	inline static int32_t get_offset_of_player_7() { return static_cast<int32_t>(offsetof(Stopper_t1976456672, ___player_7)); }
	inline Transform_t3600365921 * get_player_7() const { return ___player_7; }
	inline Transform_t3600365921 ** get_address_of_player_7() { return &___player_7; }
	inline void set_player_7(Transform_t3600365921 * value)
	{
		___player_7 = value;
		Il2CppCodeGenWriteBarrier((&___player_7), value);
	}

	inline static int32_t get_offset_of_live_9() { return static_cast<int32_t>(offsetof(Stopper_t1976456672, ___live_9)); }
	inline bool get_live_9() const { return ___live_9; }
	inline bool* get_address_of_live_9() { return &___live_9; }
	inline void set_live_9(bool value)
	{
		___live_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOPPER_T1976456672_H
#ifndef SHOOT_T2202938192_H
#define SHOOT_T2202938192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shoot
struct  Shoot_t2202938192  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOOT_T2202938192_H
#ifndef PHYSICS2DOBJECT_T3026219368_H
#define PHYSICS2DOBJECT_T3026219368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Physics2DObject
struct  Physics2DObject_t3026219368  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody2D Physics2DObject::rigidbody2D
	Rigidbody2D_t939494601 * ___rigidbody2D_2;

public:
	inline static int32_t get_offset_of_rigidbody2D_2() { return static_cast<int32_t>(offsetof(Physics2DObject_t3026219368, ___rigidbody2D_2)); }
	inline Rigidbody2D_t939494601 * get_rigidbody2D_2() const { return ___rigidbody2D_2; }
	inline Rigidbody2D_t939494601 ** get_address_of_rigidbody2D_2() { return &___rigidbody2D_2; }
	inline void set_rigidbody2D_2(Rigidbody2D_t939494601 * value)
	{
		___rigidbody2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___rigidbody2D_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICS2DOBJECT_T3026219368_H
#ifndef CAMERAFOLLOW_T129522575_H
#define CAMERAFOLLOW_T129522575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFollow
struct  CameraFollow_t129522575  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform CameraFollow::target
	Transform_t3600365921 * ___target_2;
	// System.Boolean CameraFollow::limitBounds
	bool ___limitBounds_3;
	// System.Single CameraFollow::left
	float ___left_4;
	// System.Single CameraFollow::right
	float ___right_5;
	// System.Single CameraFollow::bottom
	float ___bottom_6;
	// System.Single CameraFollow::top
	float ___top_7;
	// UnityEngine.Vector3 CameraFollow::lerpedPosition
	Vector3_t3722313464  ___lerpedPosition_8;
	// UnityEngine.Camera CameraFollow::_camera
	Camera_t4157153871 * ____camera_9;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_limitBounds_3() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___limitBounds_3)); }
	inline bool get_limitBounds_3() const { return ___limitBounds_3; }
	inline bool* get_address_of_limitBounds_3() { return &___limitBounds_3; }
	inline void set_limitBounds_3(bool value)
	{
		___limitBounds_3 = value;
	}

	inline static int32_t get_offset_of_left_4() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___left_4)); }
	inline float get_left_4() const { return ___left_4; }
	inline float* get_address_of_left_4() { return &___left_4; }
	inline void set_left_4(float value)
	{
		___left_4 = value;
	}

	inline static int32_t get_offset_of_right_5() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___right_5)); }
	inline float get_right_5() const { return ___right_5; }
	inline float* get_address_of_right_5() { return &___right_5; }
	inline void set_right_5(float value)
	{
		___right_5 = value;
	}

	inline static int32_t get_offset_of_bottom_6() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___bottom_6)); }
	inline float get_bottom_6() const { return ___bottom_6; }
	inline float* get_address_of_bottom_6() { return &___bottom_6; }
	inline void set_bottom_6(float value)
	{
		___bottom_6 = value;
	}

	inline static int32_t get_offset_of_top_7() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___top_7)); }
	inline float get_top_7() const { return ___top_7; }
	inline float* get_address_of_top_7() { return &___top_7; }
	inline void set_top_7(float value)
	{
		___top_7 = value;
	}

	inline static int32_t get_offset_of_lerpedPosition_8() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___lerpedPosition_8)); }
	inline Vector3_t3722313464  get_lerpedPosition_8() const { return ___lerpedPosition_8; }
	inline Vector3_t3722313464 * get_address_of_lerpedPosition_8() { return &___lerpedPosition_8; }
	inline void set_lerpedPosition_8(Vector3_t3722313464  value)
	{
		___lerpedPosition_8 = value;
	}

	inline static int32_t get_offset_of__camera_9() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ____camera_9)); }
	inline Camera_t4157153871 * get__camera_9() const { return ____camera_9; }
	inline Camera_t4157153871 ** get_address_of__camera_9() { return &____camera_9; }
	inline void set__camera_9(Camera_t4157153871 * value)
	{
		____camera_9 = value;
		Il2CppCodeGenWriteBarrier((&____camera_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFOLLOW_T129522575_H
#ifndef FOLLOWTARGET_T1456261924_H
#define FOLLOWTARGET_T1456261924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FollowTarget
struct  FollowTarget_t1456261924  : public Physics2DObject_t3026219368
{
public:
	// UnityEngine.Transform FollowTarget::target
	Transform_t3600365921 * ___target_3;
	// System.Single FollowTarget::speed
	float ___speed_4;
	// System.Boolean FollowTarget::lookAtTarget
	bool ___lookAtTarget_5;
	// Enums/Directions FollowTarget::useSide
	int32_t ___useSide_6;

public:
	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(FollowTarget_t1456261924, ___target_3)); }
	inline Transform_t3600365921 * get_target_3() const { return ___target_3; }
	inline Transform_t3600365921 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Transform_t3600365921 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(FollowTarget_t1456261924, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_lookAtTarget_5() { return static_cast<int32_t>(offsetof(FollowTarget_t1456261924, ___lookAtTarget_5)); }
	inline bool get_lookAtTarget_5() const { return ___lookAtTarget_5; }
	inline bool* get_address_of_lookAtTarget_5() { return &___lookAtTarget_5; }
	inline void set_lookAtTarget_5(bool value)
	{
		___lookAtTarget_5 = value;
	}

	inline static int32_t get_offset_of_useSide_6() { return static_cast<int32_t>(offsetof(FollowTarget_t1456261924, ___useSide_6)); }
	inline int32_t get_useSide_6() const { return ___useSide_6; }
	inline int32_t* get_address_of_useSide_6() { return &___useSide_6; }
	inline void set_useSide_6(int32_t value)
	{
		___useSide_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTARGET_T1456261924_H
#ifndef AUTOROTATE_T913508323_H
#define AUTOROTATE_T913508323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AutoRotate
struct  AutoRotate_t913508323  : public Physics2DObject_t3026219368
{
public:
	// System.Single AutoRotate::rotationSpeed
	float ___rotationSpeed_3;
	// System.Single AutoRotate::currentRotation
	float ___currentRotation_4;

public:
	inline static int32_t get_offset_of_rotationSpeed_3() { return static_cast<int32_t>(offsetof(AutoRotate_t913508323, ___rotationSpeed_3)); }
	inline float get_rotationSpeed_3() const { return ___rotationSpeed_3; }
	inline float* get_address_of_rotationSpeed_3() { return &___rotationSpeed_3; }
	inline void set_rotationSpeed_3(float value)
	{
		___rotationSpeed_3 = value;
	}

	inline static int32_t get_offset_of_currentRotation_4() { return static_cast<int32_t>(offsetof(AutoRotate_t913508323, ___currentRotation_4)); }
	inline float get_currentRotation_4() const { return ___currentRotation_4; }
	inline float* get_address_of_currentRotation_4() { return &___currentRotation_4; }
	inline void set_currentRotation_4(float value)
	{
		___currentRotation_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOROTATE_T913508323_H
#ifndef ROTATE_T1850091912_H
#define ROTATE_T1850091912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rotate
struct  Rotate_t1850091912  : public Physics2DObject_t3026219368
{
public:
	// Enums/KeyGroups Rotate::typeOfControl
	int32_t ___typeOfControl_3;
	// System.Single Rotate::speed
	float ___speed_4;
	// System.Single Rotate::spin
	float ___spin_5;

public:
	inline static int32_t get_offset_of_typeOfControl_3() { return static_cast<int32_t>(offsetof(Rotate_t1850091912, ___typeOfControl_3)); }
	inline int32_t get_typeOfControl_3() const { return ___typeOfControl_3; }
	inline int32_t* get_address_of_typeOfControl_3() { return &___typeOfControl_3; }
	inline void set_typeOfControl_3(int32_t value)
	{
		___typeOfControl_3 = value;
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(Rotate_t1850091912, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_spin_5() { return static_cast<int32_t>(offsetof(Rotate_t1850091912, ___spin_5)); }
	inline float get_spin_5() const { return ___spin_5; }
	inline float* get_address_of_spin_5() { return &___spin_5; }
	inline void set_spin_5(float value)
	{
		___spin_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATE_T1850091912_H
#ifndef WANDER_T651605530_H
#define WANDER_T651605530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wander
struct  Wander_t651605530  : public Physics2DObject_t3026219368
{
public:
	// System.Single Wander::speed
	float ___speed_3;
	// System.Single Wander::directionChangeInterval
	float ___directionChangeInterval_4;
	// System.Boolean Wander::keepNearStartingPoint
	bool ___keepNearStartingPoint_5;
	// System.Boolean Wander::orientToDirection
	bool ___orientToDirection_6;
	// Enums/Directions Wander::lookAxis
	int32_t ___lookAxis_7;
	// UnityEngine.Vector2 Wander::direction
	Vector2_t2156229523  ___direction_8;
	// UnityEngine.Vector3 Wander::startingPoint
	Vector3_t3722313464  ___startingPoint_9;

public:
	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(Wander_t651605530, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_directionChangeInterval_4() { return static_cast<int32_t>(offsetof(Wander_t651605530, ___directionChangeInterval_4)); }
	inline float get_directionChangeInterval_4() const { return ___directionChangeInterval_4; }
	inline float* get_address_of_directionChangeInterval_4() { return &___directionChangeInterval_4; }
	inline void set_directionChangeInterval_4(float value)
	{
		___directionChangeInterval_4 = value;
	}

	inline static int32_t get_offset_of_keepNearStartingPoint_5() { return static_cast<int32_t>(offsetof(Wander_t651605530, ___keepNearStartingPoint_5)); }
	inline bool get_keepNearStartingPoint_5() const { return ___keepNearStartingPoint_5; }
	inline bool* get_address_of_keepNearStartingPoint_5() { return &___keepNearStartingPoint_5; }
	inline void set_keepNearStartingPoint_5(bool value)
	{
		___keepNearStartingPoint_5 = value;
	}

	inline static int32_t get_offset_of_orientToDirection_6() { return static_cast<int32_t>(offsetof(Wander_t651605530, ___orientToDirection_6)); }
	inline bool get_orientToDirection_6() const { return ___orientToDirection_6; }
	inline bool* get_address_of_orientToDirection_6() { return &___orientToDirection_6; }
	inline void set_orientToDirection_6(bool value)
	{
		___orientToDirection_6 = value;
	}

	inline static int32_t get_offset_of_lookAxis_7() { return static_cast<int32_t>(offsetof(Wander_t651605530, ___lookAxis_7)); }
	inline int32_t get_lookAxis_7() const { return ___lookAxis_7; }
	inline int32_t* get_address_of_lookAxis_7() { return &___lookAxis_7; }
	inline void set_lookAxis_7(int32_t value)
	{
		___lookAxis_7 = value;
	}

	inline static int32_t get_offset_of_direction_8() { return static_cast<int32_t>(offsetof(Wander_t651605530, ___direction_8)); }
	inline Vector2_t2156229523  get_direction_8() const { return ___direction_8; }
	inline Vector2_t2156229523 * get_address_of_direction_8() { return &___direction_8; }
	inline void set_direction_8(Vector2_t2156229523  value)
	{
		___direction_8 = value;
	}

	inline static int32_t get_offset_of_startingPoint_9() { return static_cast<int32_t>(offsetof(Wander_t651605530, ___startingPoint_9)); }
	inline Vector3_t3722313464  get_startingPoint_9() const { return ___startingPoint_9; }
	inline Vector3_t3722313464 * get_address_of_startingPoint_9() { return &___startingPoint_9; }
	inline void set_startingPoint_9(Vector3_t3722313464  value)
	{
		___startingPoint_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WANDER_T651605530_H
#ifndef PATROL_T2573740331_H
#define PATROL_T2573740331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Patrol
struct  Patrol_t2573740331  : public Physics2DObject_t3026219368
{
public:
	// System.Single Patrol::speed
	float ___speed_3;
	// System.Single Patrol::directionChangeInterval
	float ___directionChangeInterval_4;
	// System.Boolean Patrol::orientToDirection
	bool ___orientToDirection_5;
	// Enums/Directions Patrol::lookAxis
	int32_t ___lookAxis_6;
	// UnityEngine.Vector2[] Patrol::waypoints
	Vector2U5BU5D_t1457185986* ___waypoints_7;
	// UnityEngine.Vector2[] Patrol::newWaypoints
	Vector2U5BU5D_t1457185986* ___newWaypoints_8;
	// System.Int32 Patrol::currentTargetIndex
	int32_t ___currentTargetIndex_9;

public:
	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(Patrol_t2573740331, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_directionChangeInterval_4() { return static_cast<int32_t>(offsetof(Patrol_t2573740331, ___directionChangeInterval_4)); }
	inline float get_directionChangeInterval_4() const { return ___directionChangeInterval_4; }
	inline float* get_address_of_directionChangeInterval_4() { return &___directionChangeInterval_4; }
	inline void set_directionChangeInterval_4(float value)
	{
		___directionChangeInterval_4 = value;
	}

	inline static int32_t get_offset_of_orientToDirection_5() { return static_cast<int32_t>(offsetof(Patrol_t2573740331, ___orientToDirection_5)); }
	inline bool get_orientToDirection_5() const { return ___orientToDirection_5; }
	inline bool* get_address_of_orientToDirection_5() { return &___orientToDirection_5; }
	inline void set_orientToDirection_5(bool value)
	{
		___orientToDirection_5 = value;
	}

	inline static int32_t get_offset_of_lookAxis_6() { return static_cast<int32_t>(offsetof(Patrol_t2573740331, ___lookAxis_6)); }
	inline int32_t get_lookAxis_6() const { return ___lookAxis_6; }
	inline int32_t* get_address_of_lookAxis_6() { return &___lookAxis_6; }
	inline void set_lookAxis_6(int32_t value)
	{
		___lookAxis_6 = value;
	}

	inline static int32_t get_offset_of_waypoints_7() { return static_cast<int32_t>(offsetof(Patrol_t2573740331, ___waypoints_7)); }
	inline Vector2U5BU5D_t1457185986* get_waypoints_7() const { return ___waypoints_7; }
	inline Vector2U5BU5D_t1457185986** get_address_of_waypoints_7() { return &___waypoints_7; }
	inline void set_waypoints_7(Vector2U5BU5D_t1457185986* value)
	{
		___waypoints_7 = value;
		Il2CppCodeGenWriteBarrier((&___waypoints_7), value);
	}

	inline static int32_t get_offset_of_newWaypoints_8() { return static_cast<int32_t>(offsetof(Patrol_t2573740331, ___newWaypoints_8)); }
	inline Vector2U5BU5D_t1457185986* get_newWaypoints_8() const { return ___newWaypoints_8; }
	inline Vector2U5BU5D_t1457185986** get_address_of_newWaypoints_8() { return &___newWaypoints_8; }
	inline void set_newWaypoints_8(Vector2U5BU5D_t1457185986* value)
	{
		___newWaypoints_8 = value;
		Il2CppCodeGenWriteBarrier((&___newWaypoints_8), value);
	}

	inline static int32_t get_offset_of_currentTargetIndex_9() { return static_cast<int32_t>(offsetof(Patrol_t2573740331, ___currentTargetIndex_9)); }
	inline int32_t get_currentTargetIndex_9() const { return ___currentTargetIndex_9; }
	inline int32_t* get_address_of_currentTargetIndex_9() { return &___currentTargetIndex_9; }
	inline void set_currentTargetIndex_9(int32_t value)
	{
		___currentTargetIndex_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATROL_T2573740331_H
#ifndef MOVE_T3440333737_H
#define MOVE_T3440333737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Move
struct  Move_t3440333737  : public Physics2DObject_t3026219368
{
public:
	// Enums/KeyGroups Move::typeOfControl
	int32_t ___typeOfControl_3;
	// System.Single Move::speed
	float ___speed_4;
	// Enums/MovementType Move::movementType
	int32_t ___movementType_5;
	// System.Boolean Move::orientToDirection
	bool ___orientToDirection_6;
	// Enums/Directions Move::lookAxis
	int32_t ___lookAxis_7;
	// UnityEngine.Vector2 Move::movement
	Vector2_t2156229523  ___movement_8;
	// UnityEngine.Vector2 Move::cachedDirection
	Vector2_t2156229523  ___cachedDirection_9;
	// System.Single Move::moveHorizontal
	float ___moveHorizontal_10;
	// System.Single Move::moveVertical
	float ___moveVertical_11;
	// System.Boolean Move::holdingBall
	bool ___holdingBall_12;

public:
	inline static int32_t get_offset_of_typeOfControl_3() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___typeOfControl_3)); }
	inline int32_t get_typeOfControl_3() const { return ___typeOfControl_3; }
	inline int32_t* get_address_of_typeOfControl_3() { return &___typeOfControl_3; }
	inline void set_typeOfControl_3(int32_t value)
	{
		___typeOfControl_3 = value;
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_movementType_5() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___movementType_5)); }
	inline int32_t get_movementType_5() const { return ___movementType_5; }
	inline int32_t* get_address_of_movementType_5() { return &___movementType_5; }
	inline void set_movementType_5(int32_t value)
	{
		___movementType_5 = value;
	}

	inline static int32_t get_offset_of_orientToDirection_6() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___orientToDirection_6)); }
	inline bool get_orientToDirection_6() const { return ___orientToDirection_6; }
	inline bool* get_address_of_orientToDirection_6() { return &___orientToDirection_6; }
	inline void set_orientToDirection_6(bool value)
	{
		___orientToDirection_6 = value;
	}

	inline static int32_t get_offset_of_lookAxis_7() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___lookAxis_7)); }
	inline int32_t get_lookAxis_7() const { return ___lookAxis_7; }
	inline int32_t* get_address_of_lookAxis_7() { return &___lookAxis_7; }
	inline void set_lookAxis_7(int32_t value)
	{
		___lookAxis_7 = value;
	}

	inline static int32_t get_offset_of_movement_8() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___movement_8)); }
	inline Vector2_t2156229523  get_movement_8() const { return ___movement_8; }
	inline Vector2_t2156229523 * get_address_of_movement_8() { return &___movement_8; }
	inline void set_movement_8(Vector2_t2156229523  value)
	{
		___movement_8 = value;
	}

	inline static int32_t get_offset_of_cachedDirection_9() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___cachedDirection_9)); }
	inline Vector2_t2156229523  get_cachedDirection_9() const { return ___cachedDirection_9; }
	inline Vector2_t2156229523 * get_address_of_cachedDirection_9() { return &___cachedDirection_9; }
	inline void set_cachedDirection_9(Vector2_t2156229523  value)
	{
		___cachedDirection_9 = value;
	}

	inline static int32_t get_offset_of_moveHorizontal_10() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___moveHorizontal_10)); }
	inline float get_moveHorizontal_10() const { return ___moveHorizontal_10; }
	inline float* get_address_of_moveHorizontal_10() { return &___moveHorizontal_10; }
	inline void set_moveHorizontal_10(float value)
	{
		___moveHorizontal_10 = value;
	}

	inline static int32_t get_offset_of_moveVertical_11() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___moveVertical_11)); }
	inline float get_moveVertical_11() const { return ___moveVertical_11; }
	inline float* get_address_of_moveVertical_11() { return &___moveVertical_11; }
	inline void set_moveVertical_11(float value)
	{
		___moveVertical_11 = value;
	}

	inline static int32_t get_offset_of_holdingBall_12() { return static_cast<int32_t>(offsetof(Move_t3440333737, ___holdingBall_12)); }
	inline bool get_holdingBall_12() const { return ___holdingBall_12; }
	inline bool* get_address_of_holdingBall_12() { return &___holdingBall_12; }
	inline void set_holdingBall_12(bool value)
	{
		___holdingBall_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVE_T3440333737_H
#ifndef JUMP_T2935391209_H
#define JUMP_T2935391209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jump
struct  Jump_t2935391209  : public Physics2DObject_t3026219368
{
public:
	// UnityEngine.KeyCode Jump::key
	int32_t ___key_3;
	// System.Single Jump::jumpStrength
	float ___jumpStrength_4;
	// System.String Jump::groundTag
	String_t* ___groundTag_5;
	// System.Boolean Jump::checkGround
	bool ___checkGround_6;
	// System.Boolean Jump::canJump
	bool ___canJump_7;

public:
	inline static int32_t get_offset_of_key_3() { return static_cast<int32_t>(offsetof(Jump_t2935391209, ___key_3)); }
	inline int32_t get_key_3() const { return ___key_3; }
	inline int32_t* get_address_of_key_3() { return &___key_3; }
	inline void set_key_3(int32_t value)
	{
		___key_3 = value;
	}

	inline static int32_t get_offset_of_jumpStrength_4() { return static_cast<int32_t>(offsetof(Jump_t2935391209, ___jumpStrength_4)); }
	inline float get_jumpStrength_4() const { return ___jumpStrength_4; }
	inline float* get_address_of_jumpStrength_4() { return &___jumpStrength_4; }
	inline void set_jumpStrength_4(float value)
	{
		___jumpStrength_4 = value;
	}

	inline static int32_t get_offset_of_groundTag_5() { return static_cast<int32_t>(offsetof(Jump_t2935391209, ___groundTag_5)); }
	inline String_t* get_groundTag_5() const { return ___groundTag_5; }
	inline String_t** get_address_of_groundTag_5() { return &___groundTag_5; }
	inline void set_groundTag_5(String_t* value)
	{
		___groundTag_5 = value;
		Il2CppCodeGenWriteBarrier((&___groundTag_5), value);
	}

	inline static int32_t get_offset_of_checkGround_6() { return static_cast<int32_t>(offsetof(Jump_t2935391209, ___checkGround_6)); }
	inline bool get_checkGround_6() const { return ___checkGround_6; }
	inline bool* get_address_of_checkGround_6() { return &___checkGround_6; }
	inline void set_checkGround_6(bool value)
	{
		___checkGround_6 = value;
	}

	inline static int32_t get_offset_of_canJump_7() { return static_cast<int32_t>(offsetof(Jump_t2935391209, ___canJump_7)); }
	inline bool get_canJump_7() const { return ___canJump_7; }
	inline bool* get_address_of_canJump_7() { return &___canJump_7; }
	inline void set_canJump_7(bool value)
	{
		___canJump_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JUMP_T2935391209_H
#ifndef PUSH_T4181980645_H
#define PUSH_T4181980645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Push
struct  Push_t4181980645  : public Physics2DObject_t3026219368
{
public:
	// UnityEngine.KeyCode Push::key
	int32_t ___key_3;
	// System.Single Push::pushStrength
	float ___pushStrength_4;
	// Enums/Axes Push::axis
	int32_t ___axis_5;
	// System.Boolean Push::relativeAxis
	bool ___relativeAxis_6;
	// System.Boolean Push::keyPressed
	bool ___keyPressed_7;
	// UnityEngine.Vector2 Push::pushVector
	Vector2_t2156229523  ___pushVector_8;

public:
	inline static int32_t get_offset_of_key_3() { return static_cast<int32_t>(offsetof(Push_t4181980645, ___key_3)); }
	inline int32_t get_key_3() const { return ___key_3; }
	inline int32_t* get_address_of_key_3() { return &___key_3; }
	inline void set_key_3(int32_t value)
	{
		___key_3 = value;
	}

	inline static int32_t get_offset_of_pushStrength_4() { return static_cast<int32_t>(offsetof(Push_t4181980645, ___pushStrength_4)); }
	inline float get_pushStrength_4() const { return ___pushStrength_4; }
	inline float* get_address_of_pushStrength_4() { return &___pushStrength_4; }
	inline void set_pushStrength_4(float value)
	{
		___pushStrength_4 = value;
	}

	inline static int32_t get_offset_of_axis_5() { return static_cast<int32_t>(offsetof(Push_t4181980645, ___axis_5)); }
	inline int32_t get_axis_5() const { return ___axis_5; }
	inline int32_t* get_address_of_axis_5() { return &___axis_5; }
	inline void set_axis_5(int32_t value)
	{
		___axis_5 = value;
	}

	inline static int32_t get_offset_of_relativeAxis_6() { return static_cast<int32_t>(offsetof(Push_t4181980645, ___relativeAxis_6)); }
	inline bool get_relativeAxis_6() const { return ___relativeAxis_6; }
	inline bool* get_address_of_relativeAxis_6() { return &___relativeAxis_6; }
	inline void set_relativeAxis_6(bool value)
	{
		___relativeAxis_6 = value;
	}

	inline static int32_t get_offset_of_keyPressed_7() { return static_cast<int32_t>(offsetof(Push_t4181980645, ___keyPressed_7)); }
	inline bool get_keyPressed_7() const { return ___keyPressed_7; }
	inline bool* get_address_of_keyPressed_7() { return &___keyPressed_7; }
	inline void set_keyPressed_7(bool value)
	{
		___keyPressed_7 = value;
	}

	inline static int32_t get_offset_of_pushVector_8() { return static_cast<int32_t>(offsetof(Push_t4181980645, ___pushVector_8)); }
	inline Vector2_t2156229523  get_pushVector_8() const { return ___pushVector_8; }
	inline Vector2_t2156229523 * get_address_of_pushVector_8() { return &___pushVector_8; }
	inline void set_pushVector_8(Vector2_t2156229523  value)
	{
		___pushVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUSH_T4181980645_H
#ifndef AUTOMOVE_T3127166102_H
#define AUTOMOVE_T3127166102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AutoMove
struct  AutoMove_t3127166102  : public Physics2DObject_t3026219368
{
public:
	// UnityEngine.Vector2 AutoMove::direction
	Vector2_t2156229523  ___direction_3;
	// System.Boolean AutoMove::relativeToRotation
	bool ___relativeToRotation_4;

public:
	inline static int32_t get_offset_of_direction_3() { return static_cast<int32_t>(offsetof(AutoMove_t3127166102, ___direction_3)); }
	inline Vector2_t2156229523  get_direction_3() const { return ___direction_3; }
	inline Vector2_t2156229523 * get_address_of_direction_3() { return &___direction_3; }
	inline void set_direction_3(Vector2_t2156229523  value)
	{
		___direction_3 = value;
	}

	inline static int32_t get_offset_of_relativeToRotation_4() { return static_cast<int32_t>(offsetof(AutoMove_t3127166102, ___relativeToRotation_4)); }
	inline bool get_relativeToRotation_4() const { return ___relativeToRotation_4; }
	inline bool* get_address_of_relativeToRotation_4() { return &___relativeToRotation_4; }
	inline void set_relativeToRotation_4(bool value)
	{
		___relativeToRotation_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOVE_T3127166102_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (AutoMove_t3127166102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[2] = 
{
	AutoMove_t3127166102::get_offset_of_direction_3(),
	AutoMove_t3127166102::get_offset_of_relativeToRotation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (AutoRotate_t913508323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[2] = 
{
	AutoRotate_t913508323::get_offset_of_rotationSpeed_3(),
	AutoRotate_t913508323::get_offset_of_currentRotation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (CameraFollow_t129522575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[8] = 
{
	CameraFollow_t129522575::get_offset_of_target_2(),
	CameraFollow_t129522575::get_offset_of_limitBounds_3(),
	CameraFollow_t129522575::get_offset_of_left_4(),
	CameraFollow_t129522575::get_offset_of_right_5(),
	CameraFollow_t129522575::get_offset_of_bottom_6(),
	CameraFollow_t129522575::get_offset_of_top_7(),
	CameraFollow_t129522575::get_offset_of_lerpedPosition_8(),
	CameraFollow_t129522575::get_offset_of__camera_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (FollowTarget_t1456261924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[4] = 
{
	FollowTarget_t1456261924::get_offset_of_target_3(),
	FollowTarget_t1456261924::get_offset_of_speed_4(),
	FollowTarget_t1456261924::get_offset_of_lookAtTarget_5(),
	FollowTarget_t1456261924::get_offset_of_useSide_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (Jump_t2935391209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[5] = 
{
	Jump_t2935391209::get_offset_of_key_3(),
	Jump_t2935391209::get_offset_of_jumpStrength_4(),
	Jump_t2935391209::get_offset_of_groundTag_5(),
	Jump_t2935391209::get_offset_of_checkGround_6(),
	Jump_t2935391209::get_offset_of_canJump_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (Move_t3440333737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[10] = 
{
	Move_t3440333737::get_offset_of_typeOfControl_3(),
	Move_t3440333737::get_offset_of_speed_4(),
	Move_t3440333737::get_offset_of_movementType_5(),
	Move_t3440333737::get_offset_of_orientToDirection_6(),
	Move_t3440333737::get_offset_of_lookAxis_7(),
	Move_t3440333737::get_offset_of_movement_8(),
	Move_t3440333737::get_offset_of_cachedDirection_9(),
	Move_t3440333737::get_offset_of_moveHorizontal_10(),
	Move_t3440333737::get_offset_of_moveVertical_11(),
	Move_t3440333737::get_offset_of_holdingBall_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (Patrol_t2573740331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[7] = 
{
	Patrol_t2573740331::get_offset_of_speed_3(),
	Patrol_t2573740331::get_offset_of_directionChangeInterval_4(),
	Patrol_t2573740331::get_offset_of_orientToDirection_5(),
	Patrol_t2573740331::get_offset_of_lookAxis_6(),
	Patrol_t2573740331::get_offset_of_waypoints_7(),
	Patrol_t2573740331::get_offset_of_newWaypoints_8(),
	Patrol_t2573740331::get_offset_of_currentTargetIndex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (Push_t4181980645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[6] = 
{
	Push_t4181980645::get_offset_of_key_3(),
	Push_t4181980645::get_offset_of_pushStrength_4(),
	Push_t4181980645::get_offset_of_axis_5(),
	Push_t4181980645::get_offset_of_relativeAxis_6(),
	Push_t4181980645::get_offset_of_keyPressed_7(),
	Push_t4181980645::get_offset_of_pushVector_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (Rotate_t1850091912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[3] = 
{
	Rotate_t1850091912::get_offset_of_typeOfControl_3(),
	Rotate_t1850091912::get_offset_of_speed_4(),
	Rotate_t1850091912::get_offset_of_spin_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (Wander_t651605530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[7] = 
{
	Wander_t651605530::get_offset_of_speed_3(),
	Wander_t651605530::get_offset_of_directionChangeInterval_4(),
	Wander_t651605530::get_offset_of_keepNearStartingPoint_5(),
	Wander_t651605530::get_offset_of_orientToDirection_6(),
	Wander_t651605530::get_offset_of_lookAxis_7(),
	Wander_t651605530::get_offset_of_direction_8(),
	Wander_t651605530::get_offset_of_startingPoint_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (U3CChangeDirectionU3Ec__Iterator0_t1842818970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[4] = 
{
	U3CChangeDirectionU3Ec__Iterator0_t1842818970::get_offset_of_U24this_0(),
	U3CChangeDirectionU3Ec__Iterator0_t1842818970::get_offset_of_U24current_1(),
	U3CChangeDirectionU3Ec__Iterator0_t1842818970::get_offset_of_U24disposing_2(),
	U3CChangeDirectionU3Ec__Iterator0_t1842818970::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (Shoot_t2202938192), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (Stopper_t1976456672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1812[8] = 
{
	Stopper_t1976456672::get_offset_of_rb2D_2(),
	Stopper_t1976456672::get_offset_of_renderer_3(),
	Stopper_t1976456672::get_offset_of_collider_4(),
	Stopper_t1976456672::get_offset_of_mass_5(),
	Stopper_t1976456672::get_offset_of_holder_6(),
	Stopper_t1976456672::get_offset_of_player_7(),
	0,
	Stopper_t1976456672::get_offset_of_live_9(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif

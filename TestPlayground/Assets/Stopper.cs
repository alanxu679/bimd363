﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Stopper : MonoBehaviour {

	private Rigidbody2D rb2D;
    private SpriteRenderer renderer;
    private CircleCollider2D collider;
    private float mass;
    private Transform holder;
    private Transform player;
    private const float force = 10000;
    private bool live = true;
	// Use this for initialization
	void Start () {
        rb2D = GetComponent<Rigidbody2D>();
        collider = GetComponent<CircleCollider2D>();
        renderer = GetComponent<SpriteRenderer>();
        mass = rb2D.mass;
	}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) && holder != null)
            throwBall();

        renderer.color = (live) ? Color.white : Color.red;
    }

    private void throwBall()
    {
        rb2D.angularVelocity = 0;
        rb2D.AddForce(holder.up * force, ForceMode2D.Impulse);
        live = false;
        onHolder(false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.CompareTag("Bullet"))
        {
            rb2D.velocity = Vector2.zero;
            rb2D.angularVelocity = 0;
            return;
        }

        if (!live)
            return;

        if (collision.gameObject.CompareTag("Player"))
        {
            if(holder == null)
            {
                player = collider.transform;
                holder = collision.transform.GetChild(0);
                onHolder(true);
            }
            else
            {
                if(player != collision.transform)
                {
                    //scoring here
                    holder = null;
                }
                    
            }
        }
    }

    private void onHolder(bool result)
    {
        collider.isTrigger = result;
        rb2D.isKinematic = result;
        transform.parent = (result)?holder: null;
        transform.position = (result)? holder.position: transform.position;

        if (!result)
        {
            Invoke("setLive", 3);
        }
    }

    //3 second wait till ball is active again
    private void setLive()
    {
        live = true;
        holder = null;
    }
}
